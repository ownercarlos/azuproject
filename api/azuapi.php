<?php



if (isset($_POST)){
    /*
    DB Connection
    */
    
    $servername = "localhost";
    $database = "u931312158_dev_shahidaali";
    $username = "u931312158_shahidaali";
    $password = "dev_fiverrShahidaali1";
    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);
    // Check connection
    if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
    }
    
    
    
    /*
        Get table
    */
    if(isset($_POST["action"])){
     
    $action = json_decode($_POST["action"]);
    $table = "";
    foreach($action as $key => $row)
    {
        $table = $key;
   
    
    }
    
   
    
    /*
        Switch table to perform an operation.
    */

    switch($table){
        case "categoria":
                        /*
                            Categorias
                
                         */
                         switch($action->categoria->operation){
                             case "insert":
                                 include 'categorias/insert.php';
                                 $categoria_insert = new insert_categoria;
                                 $categoria_insert->insert($action->categoria->tipo, $action->categoria->sexo, $action->categoria->estatus, $conn); 
                                 break;
                            case "update":
                                include 'categorias/update.php';
                                $categoria_update = new update_categoria;
                                $categoria_update->update($action->categoria->tipo, $action->categoria->sexo, $action->categoria->estatus, $conn, $action->categoria->id);
                                break;
                            case "select":
                                include 'categorias/select.php';
                                $categoria_select = new select_categoria;
                                $categoria_select->select($action->categoria->tipo, $action->categoria->sexo, $action->categoria->estatus, $conn);
                                break;
                            case "delete":
                                include 'categorias/delete.php';
                                $categoria_delete = new delete_categoria;
                                $categoria_delete->delete($action->categoria->id, $conn);
                                break;
                            default:
                                echo "ERROR AZU0002";
                                break;
                         }
            break;
        case "tallas":
                         /*
                             Tallas
                         */
                         switch($action->tallas->operation){
                             case "insert":
                                 include 'tallas/insert.php';
                                 $talla_insert = new insert_tallas;
                                 $talla_insert->insert($action->tallas->s, $action->tallas->m, $action->tallas->l, $action->tallas->xs, $action->tallas->xl, $action->tallas->estatus, $conn);
                                 break;
                            case "update":
                                include 'tallas/update.php';
                                $talla_update = new update_tallas;
                                $talla_update->update($action->tallas->id, $action->tallas->estatus ,$conn);
                                break;
                            case "select":
                                include 'tallas/select.php';
                                $talla_select = new select_tallas;
                                $talla_select->select($action->tallas->sactive, $action->tallas->mactive, $action->tallas->lactive, $action->tallas->xsactive, $action->tallas->xlactive, $action->tallas->s, $action->tallas->m, $action->tallas->l, $action->tallas->xs, $action->tallas->xl,$action->tallas->id, $conn);
                                break;
                            case "delete":
                                include 'tallas/delete.php';
                                $talla_delete = new delete_tallas;
                                $talla_delete->delete();
                                break;
                            default:
                                echo "ERROR AZU0002";
                                break;
                         }
            
            break;
        case "imagenes":
              /*
                             imagenes
                         */
                         switch($action->imagenes->operation){
                             case "insert":
                                 break;
                            case "update":
                                include 'imagenes/update.php';
                                $imagen_update = new image_update;
                                $imagen_update->update($action->imagenes->id, $action->imagenes->color, $action->imagenes->activo, $conn);
                                break;
                            case "select":
                                include 'imagenes/select.php';
                                $imagen_select = new image_select;
                                $imagen_select->select($action->imagenes->id, $action->imagenes->color, $action->imagenes->activo, $conn);
                                break;
                            case "delete":
                                include 'imagenes/delete.php';
                                $imagen_delete = new image_delete;
                                $imagen_delete->delete($action->imagenes->id, $conn);
                                break;
                            default:
                                echo "ERROR AZU0002";
                                break;
                         }
            break;
        case "producto":
              /*
                             Producto
                         */
                         switch($action->producto->operation){
                             case "insert":
                                 include 'producto/insert.php';
                                 $producto_insert = new producto_insert;
                                $producto_insert->insert($action->producto->precio, $action->producto->descuento, $action->producto->peso, $action->producto->oferta, $action->producto->stock, $action->producto->colores, $action->producto->idimagen, $action->producto->activo, $action->producto->idcategoria, $action->producto->marca, $action->producto->titulo, $action->producto->descripcion, $action->producto->features, $action->producto->idtalla, $conn);
                                 break;
                            case "update":
                                include 'producto/update.php';
                                $producto_update = new producto_update;
                                $producto_update->update($action->producto->precio, $action->producto->descuento, $action->producto->peso, $action->producto->oferta, $action->producto->stock, $action->producto->colores, $action->producto->activo, $action->producto->idProducto, $conn);
                                break;
                            case "select":
                                include 'producto/select.php';
                                $producto_select = new producto_select;
                                $producto_select->select($action->producto->id, $action->producto->sku, $action->producto->precio, $action->producto->descuento, $action->producto->peso, $action->producto->oferta, $action->producto->stock, $action->producto->colores, $action->producto->idimagen, $action->producto->activo, $action->producto->marca, $action->producto->titulo, $action->producto->descripcion, $action->producto->features, $conn);
                                break;
                            case "delete":
                                include 'producto/delete.php';
                                $producto_delete = new producto_delete;
                                $producto_delete->delete($action->producto->id, $conn);
                                break;
                         }
            break;
        case "pagos":
              /*
                             Pagos
                         */
                         switch($action->pagos->operation){
                             case "insert":
                                 break;
                            case "update":
                                break;
                            case "select":
                                break;
                            case "delete":
                                break;
                         }
            break;
        case "ordenes":
              /*
                             Ordenes
                         */
                         switch($action->ordenes->operation){
                             case "insert":
                                 break;
                            case "update":
                                break;
                            case "select":
                                break;
                            case "delete":
                                break;
                         }
            break;
        case "usuarios":
              /*
                             usuarios
                         */
                         switch($action->usuario->operation){
                             case "insert":
                                 break;
                            case "update":
                                break;
                            case "select":
                                break;
                            case "delete":
                                break;
                         }
            break;
        case "visitas":
              /*
                             visitas
                         */
                         switch($action->visitas->operation){
                             case "insert":
                                
                                include 'visitas/insert.php';
                                $visitas_insert = new visitas_insert;
                                $visitas_insert->insert($action->visitas->timestamp, $action->visitas->status, $action->visitas->color, $action->visitas->sku, $action->visitas->email, $conn);
                                 break;
                            case "update":
                                break;
                            case "select":
                                break;
                            case "delete":
                                break;
                         }
            break;
        default:
            echo "Error AZU0001";
            break;
        
    }
    
    
   
    
    /*
    Imagenes handler for submitting image
    */
}
 }
if (count($_FILES) > 0) {
    if (is_uploaded_file($_FILES['userImage']['tmp_name'])) {
        $imgData = addslashes(file_get_contents($_FILES['userImage']['tmp_name']));
        $imageProperties = getimageSize($_FILES['userImage']['tmp_name']);
        $sql = "INSERT INTO Imagenes(idImagen, productoSKU, imagen, tipoimagen, color, activo)
	VALUES(NULL,'0220', '{$imgData}', '{$imageProperties['mime']}', 'rojo', '1' )";
        $current_id = mysqli_query($conn, $sql) or die("<b>Error:</b> Problem on Image Insert<br/>" . mysqli_error($conn));
        if (isset($current_id)) {
          
        }
    }
}





?>