<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
<meta http-equciv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Azú</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="template-collection belle">
<div class="pageWrapper">
    <!--Top Header-->
     <div id="insertheader">
        
    
    <!--End Top Header-->
    <!--Header-->
    
    <!--End Header-->
    </div>
    <!--Mobile Menu-->

	<!--End Mobile Menu-->
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Collection Banner-->
    	<div class="collection-header">
			<div class="collection-hero">
        		<div class="collection-hero__image"></div>
        		<div class="collection-hero__title-wrapper"><h1 class="collection-hero__title page-width">Búsqueda en Azú</h1></div>
      		</div>
		</div>
        <!--End Collection Banner-->
        
        <div class="container">
        	<div class="row">
            	<!--Sidebar-->
            	<div class="col-12 col-sm-12 col-md-3 col-lg-3 sidebar filterbar">
                	<div class="closeFilter d-block d-md-none d-lg-none"><i class="icon icon anm anm-times-l"></i></div>
                	<div class="sidebar_tags">
                    	<!--Categories-->
                    	<div id="search_category">
                            
                        </div>
                        <!--Categories-->
                        <!--Price Filter-->

                        <!--End Price Filter-->
                        <!--Size Swatches-->
                        <div class="sidebar_widget filterBox filter-widget size-swacthes">
                            <div class="widget-title"><h2>Tallas</h2></div>
                            <div class="filter-color swacth-list">
                            	<ul>
                                    <li><span id="span_size_xs" class="swacth-btn" onclick="handleSize('xs');">XS</span></li>
                                    <li><span id="span_size_s" class="swacth-btn" onclick="handleSize('s');">S</span></li>
                                    <li><span id="span_size_m" class="swacth-btn" onclick="handleSize('m');">M</span></li>
                                    <li><span id="span_size_l" class="swacth-btn" onclick="handleSize('l');">L</span></li>
                                    <li><span id="span_size_xl" class="swacth-btn" onclick="handleSize('xl');">XL</span></li>
                                </ul>
                            </div>
                        </div>
                        <!--End Size Swatches-->
                        <!--Color Swatches-->
                        <div class="sidebar_widget filterBox filter-widget">
                            <div class="widget-title"><h2>Color</h2></div>
                            <div class="filter-color swacth-list clearfix">
                                <span class="swacth-btn black" onclick="handleColors('black');" id="span_colors_black"></span>
                                <span class="swacth-btn white checked" onclick="handleColors('white');" id="span_colors_white"></span>
                                <span class="swacth-btn red" onclick="handleColors('red');" id="span_colors_red"></span>
                                <span class="swacth-btn blue" onclick="handleColors('blue');" id="span_colors_blue"></span>
                                <span class="swacth-btn pink" onclick="handleColors('pink');" id="span_colors_pink"></span>
                                <span class="swacth-btn gray" onclick="handleColors('gray');" id="span_colors_gray"></span>
                                <span class="swacth-btn green" onclick="handleColors('green');" id="span_colors_green"></span>
                                <span class="swacth-btn orange" onclick="handleColors('orange');" id="span_colors_orange"></span>
                                <span class="swacth-btn yellow" onclick="handleColors('yellow');" id="span_colors_yellow"></span>
                                <span class="swacth-btn brown" onclick="handleColors('brown');" id="span_colors_brown"></span>
                                <span class="swacth-btn khaki" onclick="handleColors('khaki');" id="span_colors_khaki"></span> 
                            </div>
                        </div>
                        <!--End Color Swatches-->
                        <!--Brand-->
                        <!--<div class="sidebar_widget filterBox filter-widget">-->
                        <!--    <div class="widget-title"><h2>Brands</h2></div>-->
                        <!--    <ul>-->
                        <!--        <li>-->
                        <!--          <input type="checkbox" value="allen-vela" id="check1">-->
                        <!--          <label for="check1"><span><span></span></span>Allen Vela</label>-->
                        <!--        </li>-->
                        <!--        <li>-->
                        <!--          <input type="checkbox" value="oxymat" id="check3">-->
                        <!--          <label for="check3"><span><span></span></span>Oxymat</label>-->
                        <!--        </li>-->
                        <!--        <li>-->
                        <!--          <input type="checkbox" value="vanelas" id="check4">-->
                        <!--          <label for="check4"><span><span></span></span>Vanelas</label>-->
                        <!--        </li>-->
                        <!--        <li>-->
                        <!--          <input type="checkbox" value="pagini" id="check5">-->
                        <!--          <label for="check5"><span><span></span></span>Pagini</label>-->
                        <!--        </li>-->
                        <!--        <li>-->
                        <!--          <input type="checkbox" value="monark" id="check6">-->
                        <!--          <label for="check6"><span><span></span></span>Monark</label>-->
                        <!--        </li>-->
                        <!--    </ul>-->
                        <!--</div>-->
                        <!--End Brand-->
                        <!--Popular Products-->
						<div class="sidebar_widget">
                        	<div class="widget-title"><h2>Productos Populares</h2></div>
							<div class="widget-content">
                                <div class="list list-sidebar-products">
                                  <div class="grid" id="grid_popular">
                                    <div class="grid__item">
                                      <div class="mini-list-item">
                                        <div class="mini-view_image">
                                            <a class="grid-view-item__link" href="#">
                                                <img class="grid-view-item__image" src="assets/images/product-images/mini-product-img.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="details"> <a class="grid-view-item__title" href="#">Cena Skirt</a>
                                          <div class="grid-view-item__meta"><span class="product-price__price"><span class="money">$173.60</span></span></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="grid__item">
                                      <div class="mini-list-item">
                                        <div class="mini-view_image"> <a class="grid-view-item__link" href="#"><img class="grid-view-item__image" src="assets/images/product-images/mini-product-img1.jpg" alt="" /></a> </div>
                                        <div class="details"> <a class="grid-view-item__title" href="#">Block Button Up</a>
                                          <div class="grid-view-item__meta"><span class="product-price__price"><span class="money">$378.00</span></span></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="grid__item">
                                      <div class="mini-list-item">
                                        <div class="mini-view_image"> <a class="grid-view-item__link" href="#"><img class="grid-view-item__image" src="assets/images/product-images/mini-product-img2.jpg" alt="" /></a> </div>
                                        <div class="details"> <a class="grid-view-item__title" href="#">Balda Button Pant</a>
                                          <div class="grid-view-item__meta"><span class="product-price__price"><span class="money">$278.60</span></span></div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="grid__item">
                                      <div class="mini-list-item">
                                        <div class="mini-view_image"> <a class="grid-view-item__link" href="#"><img class="grid-view-item__image" src="assets/images/product-images/mini-product-img3.jpg" alt="" /></a> </div>
                                        <div class="details"> <a class="grid-view-item__title" href="#">Border Dress in Black/Silver</a>
                                          <div class="grid-view-item__meta"><span class="product-price__price"><span class="money">$228.00</span></span></div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                          	</div>
						</div>
                        <!--End Popular Products-->
                        <!--Banner-->
                        <!--Banner-->
                        <!--Information-->
                        <div class="sidebar_widget">
                            <div class="widget-title"><h2>¿Qué es Azú?</h2></div>
                            <div class="widget-content"><p>Nos preocupamos directamente con tus necesidades, queremos darte lo mejor que ofrecemos, hecho con mucho amor especialmente para tí.</p></div>
                        </div>
                        <!--end Information-->
                        <!--Product Tags-->
                        <div class="sidebar_widget">
                          <div class="widget-title">
                            <h2>Product Tags</h2>
                          </div>
                          <div class="widget-content">
                            <ul class="product-tags">
                              <li><a href="#" title="Marca">Azú</a></li>
                            </ul>
                            <span class="btn btn--small btnview">View all</span> </div>
                        </div>
                        <!--end Product Tags-->
                    </div>
                </div>
                <!--End Sidebar-->
                <!--Main Content-->
                <div class="col-12 col-sm-12 col-md-9 col-lg-9 main-col">
                	<div class="category-description" style="margin-top: 10%;">
                    <div class="search__form">
                    	<form class="search-bar__form" action="#">
               <button class="search-trigger" style="height:100%; width:auto; border: 1px solid transparent; font-size: 20px;" type="submit"><i class="icon anm anm-search-l" style="color: #f7931eff;"></i></button>
                <input class="search__input" style="width: 80%;" type="search" name="input_search" value="" placeholder="Buscar en Azú" aria-label="Search" autocomplete="off"> 
            </form>
            </div>
         
              
                    <hr>
                	<div class="productList">
                    	<!--Toolbar-->
                        <button type="button" class="btn btn-filter d-block d-md-none d-lg-none"> Product Filters</button>
                    	<div class="toolbar">
                        	<div class="filters-toolbar-wrapper">
                            	<div class="row">
                                	<div class="col-4 col-md-4 col-lg-4 filters-toolbar__item collection-view-as d-flex justify-content-start align-items-center">
                                    	<a href="shop-left-sidebar.html" title="Grid View" class="change-view change-view--active">
                                        	<!--<img src="assets/images/grid.jpg" alt="Grid" />-->
                                        </a>
                                        <a href="shop-listview.html" title="List View" class="change-view">
                                        	<!--<img src="assets/images/list.jpg" alt="List" />-->
                                        </a>
                                    </div>
                                    <div class="col-4 col-md-4 col-lg-4 text-center filters-toolbar__item filters-toolbar__item--count d-flex justify-content-center align-items-center">
                                    	<span class="filters-toolbar__product-count" style="font-size: large;" id="showing"></span>
                                    </div>
                                    <div class="col-4 col-md-4 col-lg-4 text-right">
                                    	<div class="filters-toolbar__item">
                                      		<label for="SortBy" class="hidden">Sort</label>
                                      		<select name="SortBy" id="SortBy" class="filters-toolbar__input filters-toolbar__input--sort">
                                                <option value="title-ascending" selected="selected">Sort</option>
                                                <option>Best Selling</option>
                                                <option>Alphabetically, A-Z</option>
                                                <option>Alphabetically, Z-A</option>
                                                <option>Price, low to high</option>
                                                <option>Price, high to low</option>
                                                <option>Date, new to old</option>
                                                <option>Date, old to new</option>
                                      		</select>
                                      		<input class="collection-header__default-sort" type="hidden" value="manual">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!--End Toolbar-->
                        <div class="grid-products grid--view-items">
                            <div class="row" id="catalog">
                                
                                
                            </div>
                        </div>
                    </div>
                    <hr class="clear">
                    <div class="pagination">
                      <ul>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li class="next"><a href="#"><i class="fa fa-caret-right" aria-hidden="true"></i></a></li>
                      </ul>
                    </div>
                </div>
                <!--End Main Content-->
            </div>
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <footer id="footer">
    </footer>
    <!--End Footer-->
    
    <!--Scoll Top-->
    <span id="site-scroll" style=" background-color: darkred"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
    <!--Quick View popup-->
    <div class="modal fade quick-view-popup" id="content_quickview">
    	<div class="modal-dialog">
        	<div class="modal-content">
            	<div class="modal-body">
                    <div id="ProductSection-product-template" class="product-template__container prstyle1">
                <div class="product-single">
                <!-- Start model close -->
                <a href="javascript:void()" data-dismiss="modal" class="model-close-btn pull-right" title="close"><span class="icon icon anm anm-times-l"></span></a>
                <!-- End model close -->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="product-details-img">
                            <div class="pl-20">
                                <img src="assets/images/product-detail-page/camelia-reversible-big1.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="product-single__meta">
                                <h2 class="product-single__title">Product Quick View Popup</h2>
                                <div class="prInfoRow">
                                    <div class="product-stock"> <span class="instock ">In Stock</span> <span class="outstock hide">Unavailable</span> </div>
                                    <div class="product-sku">SKU: <span class="variant-sku">19115-rdxs</span></div>
                                </div>
                                <p class="product-single__price product-single__price-product-template">
                                    <span class="visually-hidden">Regular price</span>
                                    <s id="ComparePrice-product-template"><span class="money">$600.00</span></s>
                                    <span class="product-price__price product-price__price-product-template product-price__sale product-price__sale--single">
                                        <span id="ProductPrice-product-template"><span class="money">$500.00</span></span>
                                    </span>
                                </p>
                                <div class="product-single__description rte">
                                    Belle Multipurpose Bootstrap 4 Html Template that will give you and your customers a smooth shopping experience which can be used for various kinds of stores such as fashion,...
                                </div>
                                
                            <form method="post" action="http://annimexweb.com/cart/add" id="product_form_10508262282" accept-charset="UTF-8" class="product-form product-form-product-template hidedropdown" enctype="multipart/form-data">
                                <div class="swatch clearfix swatch-0 option1" data-option-index="0">
                                    <div class="product-form__item">
                                      <label class="header">Color: <span class="slVariant">Red</span></label>
                                      <div data-value="Red" class="swatch-element color red available">
                                        <input class="swatchInput" id="swatch-0-red" type="radio" name="option-0" value="Red">
                                        <label class="swatchLbl color medium rectangle" for="swatch-0-red" style="background-image:url(assets/images/product-detail-page/variant1-1.jpg);" title="Red"></label>
                                      </div>
                                      <div data-value="Blue" class="swatch-element color blue available">
                                        <input class="swatchInput" id="swatch-0-blue" type="radio" name="option-0" value="Blue">
                                        <label class="swatchLbl color medium rectangle" for="swatch-0-blue" style="background-image:url(assets/images/product-detail-page/variant1-2.jpg);" title="Blue"></label>
                                      </div>
                                      <div data-value="Green" class="swatch-element color green available">
                                        <input class="swatchInput" id="swatch-0-green" type="radio" name="option-0" value="Green">
                                        <label class="swatchLbl color medium rectangle" for="swatch-0-green" style="background-image:url(assets/images/product-detail-page/variant1-3.jpg);" title="Green"></label>
                                      </div>
                                      <div data-value="Gray" class="swatch-element color gray available">
                                        <input class="swatchInput" id="swatch-0-gray" type="radio" name="option-0" value="Gray">
                                        <label class="swatchLbl color medium rectangle" for="swatch-0-gray" style="background-image:url(assets/images/product-detail-page/variant1-4.jpg);" title="Gray"></label>
                                      </div>
                                    </div>
                                </div>
                                <div class="swatch clearfix swatch-1 option2" data-option-index="1">
                                    <div class="product-form__item">
                                      <label class="header">Size: <span class="slVariant">XS</span></label>
                                      <div data-value="XS" class="swatch-element xs available">
                                        <input class="swatchInput" id="swatch-1-xs" type="radio" name="option-1" value="XS">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-xs" title="XS">XS</label>
                                      </div>
                                      <div data-value="S" class="swatch-element s available">
                                        <input class="swatchInput" id="swatch-1-s" type="radio" name="option-1" value="S">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-s" title="S">S</label>
                                      </div>
                                      <div data-value="M" class="swatch-element m available">
                                        <input class="swatchInput" id="swatch-1-m" type="radio" name="option-1" value="M">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-m" title="M">M</label>
                                      </div>
                                      <div data-value="L" class="swatch-element l available">
                                        <input class="swatchInput" id="swatch-1-l" type="radio" name="option-1" value="L">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-l" title="L">L</label>
                                      </div>
                                    </div>
                                </div>
                                <!-- Product Action -->
                                <div class="product-action clearfix">
                                    <div class="product-form__item--quantity">
                                        <div class="wrapQtyBtn">
                                            <div class="qtyField">
                                                <a class="qtyBtn minus" href="javascript:void(0);"><i class="fa anm anm-minus-r" aria-hidden="true"></i></a>
                                                <input type="text" id="Quantity" name="quantity" value="1" class="product-form__input qty">
                                                <a class="qtyBtn plus" href="javascript:void(0);"><i class="fa anm anm-plus-r" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="product-form__item--submit">
                                        <button type="button" name="add" class="btn product-form__cart-submit">
                                            <span>Add to cart</span>
                                        </button>
                                    </div>
                                </div>
                                <!-- End Product Action -->
                            </form>
                            <div class="display-table shareRow">
                                    <div class="display-table-cell">
                                        <div class="wishlist-btn">
                                            <a class="wishlist add-to-wishlist" href="#" title="Add to Wishlist"><i class="icon anm anm-heart-l" aria-hidden="true"></i> <span>Add to Wishlist</span></a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
                <!--End-product-single-->
                </div>
            </div>
        		</div>
        	</div>
        </div>
    </div>
    <!--End Quick View popup-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>
<script>
    $("#insertheader").load("header_component.php", function(){
                         console.log("header component loaded succesfully");
                     });
    $("#footer").load("footer_component.php", function(){
                         console.log("footer component loaded succesfully");
                     });
</script>
<script>
var category_mapping = [];
var action = JSON.stringify({"categoria": {"operation": "select", "tipo":"", "sexo":"", "estatus":"on"}} );

        var html = '<div class="sidebar_widget categories filter-widget"><div class="widget-title"><h2>Categories</h2></div><div class="widget-content"><ul class="sidebar_categories"><li class="level1 sub-level"><a href="javascript:void(0);" id="category_container" onclick="categoryClick(this)" class="site-nav">Estilos de Prenda</a><ul id="category_list" class="sidebar_categories" style="display:inline-block;" >';
         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/azuapi.php' );
         xhr.send(fd);
         xhr.onreadystatechange = function ( response ) {
              if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
             var respObject = JSON.parse(this.response);
             //EMPIEZA LLAMADA A API PARA IMAGEN
             for(var i = 0; i < respObject.length; i++){
                 category_mapping.push({id: respObject[i].id, name: respObject[i].name})
                 html = html + '<li class="level2"><a href="search.php?search='+respObject[i].name+'" class="site-nav">'+respObject[i].name+'</a></li>';
             }
             html = html + '<li class="level2"><a href="search.php?search='+'all'+'" class="site-nav">'+'Ver todos los productos'+'</a></li>';
             html = html + '</ul></li></ul></div></div>';
              document.getElementById('search_category').innerHTML = html;
              var ul = document.getElementById('category_list');
              
              }
             
              
         }
         
    /*
                                        
    */

    function categoryClick(e){
        if(e.id = "category_container"){
            var ul = document.getElementById("category_list");
            if(ul.style.display == "inline-block"){
                ul.setAttribute('style', "display:none;");
            }
            else{
                ul.setAttribute('style', "display:inline-block;")
            }
            
        }
        
    }


    var selected_sizes = {
        xs:false,
        s: false,
        m: false,
        l: false,
        xl: false
        
    };
    function handleSize(doc){
        search_object.size = true;
        var selection = document.getElementById("span_size_"+doc);
        selected_sizes[doc] = !selected_sizes[doc];
        
        if(selection.style.border == "1px solid black"){
            
          selection.setAttribute("style", "border: 0px solid black;");
            
        }
        else{
             
             selection.setAttribute("style", "border: 1px solid black;");
           
        }
        search_by_size();
        

    }

    var colors = {
        black: false,
        white: false,
        red: false,
        blue: false,
        pink: false,
        gray: false,
        green: false,
        orange: false,
        yellow: false,
        brown: false,
        khaki: false
        
    };
    function handleColors(color){
        search_object.color = true;
         var selection = document.getElementById("span_colors_"+color);
        colors[color] = !colors[color];
         if(selection.style.border == "1px solid black"){
            
          selection.setAttribute("style", "border: 0px solid black;");
            
        }
        else{
             
             selection.setAttribute("style", "border: 1px solid black;");
           
        }
        filter_by_color();
    }

        $.get('https://api.azu-studio.com/showFeatures.php?search=features',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
document.getElementById("grid_popular").innerHTML = "";
for(i = 0; i < (myObj.information).length; i++){
    document.getElementById("grid_popular").innerHTML += myObj.information[i];
}

});
    

    window.onload = function() {
        init();
        search();
        
      
    };
     var search_object = {
            bar: false,
            category: false,
            color: false,
            size: false,
            features: false
            
        };
        
        var search_input_parameter = {};
 var search_bar_input, category, color, size, search_bar_feature;
    function init(){
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        if(urlParams.has('input_search')){
            search_bar_input = urlParams.get('input_search');
            search_object.bar = true;
        }
       
        if(urlParams.has('feature')){
            search_bar_feature = urlParams.get('feature');
            search_object.features = true;
        }
        if(urlParams.has('search')){
            category = urlParams.get('search');
            search_input_parameter.name = category;
            search_object.category = true;
        }
        if(urlParams.has('size')){
            size = urlParams.get('search');
            search_input_parameter.name = size;
            switch(size){
                          case "xs":
                              this.selected_sizes.xs = true;
                               break;
                            case "s":
                              this.selected_sizes.s = true;
                               break;
                            case "m":
                              this.selected_sizes.m = true;
                               break;
                            case "l":
                              this.selected_sizes.l = true;
                               break;
                            case "xl":
                              this.selected_sizes.xl = true;
                               break;
                               
                      }
                     
            search_object.size = true;
        }
    }
    
 var products, locals;   
       function search(){
        var action = JSON.stringify({"producto":{"operation":"select"}} );
         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/getAllProducts.php' );
         xhr.send(fd);
         xhr.onreadystatechange = function ( response ) {
              if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {

             var respObject = JSON.parse(xhr.response);
             products = respObject;
             document.getElementById("showing").innerHTML = (respObject.searchinformation).length +" Resultados";
             for(i = 0; i<(respObject.searchinformation).length; i++){
             var div_container = document.createElement("DIV");
             div_container.setAttribute("class", 'col-6 col-sm-6 col-md-4 col-lg-4 item');
    
                var div_product_image = document.createElement("DIV");
                div_product_image.setAttribute("class", "product-image");
                
                    var a_image = document.createElement("A");
                    a_image.setAttribute("href", "#");
                    
                        var image = document.createElement("IMG");
                        image.setAttribute("class", 'primary blur-up ls-is-cached lazyloaded');
                        image.src = respObject.searchinformation[i].images;
                        image.setAttribute("data-src", respObject.searchinformation[i].images);
                        
                            var div_tags = document.createElement("DIV");
                            div_tags.setAttribute("class", 'product-labels rectangular');
                            if(respObject.searchinformation[i].offer == "1"){
                                var span_offer = document.createElement("SPAN");
                                span_offer.setAttribute("class", 'lbl on-sale');
                                span_offer.innerHTML = respObject.searchinformation[i].sale + "%";
                                div_tags.appendChild(span_offer);
                            }
                        a_image.appendChild(image);
                        a_image.appendChild(div_tags);
                div_product_image.appendChild(a_image);
                
                    var form_button = document.createElement("FORM");
                    form_button.setAttribute("class","variants add");
                    form_button.setAttribute("action","#");
                    form_button.setAttribute("onclick","window.location.href='product-accordion.php'");
                        var submit_button = document.createElement("BUTTON");
                        submit_button.setAttribute("class", "btn btn-addto-cart");
                        submit_button.setAttribute("type", "button");
                        submit_button.innerHTML = "VER AHORA";
                    form_button.appendChild(submit_button);
                div_product_image.appendChild(form_button);
                
                
                    var div_button_collections = document.createElement("DIV");
                    div_button_collections.setAttribute("class", 'button-set');
                    
                        var a_modal = document.createElement("A");
                        a_modal.setAttribute("href", "javascript:void");
                        a_modal.setAttribute("title", "ver modal");
                        a_modal.setAttribute("class", "quick-view-popup quick-view");
                        a_modal.setAttribute("data-toggle", "modal");
                        a_modal.setAttribute("data-target", "#content_quickview");
                        
                            var icon_modal = document.createElement("I");
                            icon_modal.setAttribute("class", "icon anm anm-search-plus-r");
                        a_modal.appendChild(icon_modal);
                    div_button_collections.appendChild(a_modal);
                    
                    
                        var div_wishlist = document.createElement("DIV");
                        div_wishlist.setAttribute("class", "wishlist-btn");
                        
                            var a_wishlist = document.createElement("A");
                            a_wishlist.setAttribute("href", "#");
                            a_wishlist.setAttribute("title", "Agregar a bolsa");
                            a_wishlist.setAttribute("class", "wishlist add-to-wishlist");
                        
                                var icon_modal = document.createElement("I");
                                icon_modal.setAttribute("class", "icon anm anm-heart-l");
                            
                            a_wishlist.appendChild(icon_modal);
                        div_wishlist.appendChild(a_wishlist);
                    div_button_collections.appendChild(div_wishlist);
                div_product_image.appendChild(div_button_collections);
            div_container.appendChild(div_product_image);
            
            //next div
            
                var div_product_details = document.createElement("DIV");
                div_product_details.setAttribute("class", "product-details text-center");
                
                    var div_name = document.createElement("DIV");
                    div_name.setAttribute("class", "product-name");
                    
                        var a_name = document.createElement("A");
                        a_name.setAttribute("hred", "#");
                        a_name.innerHTML = respObject.searchinformation[i].title;
                        
                    div_name.appendChild(a_name);
                div_product_details.appendChild(div_name);
                
                var div_product_prices = document.createElement("DIV");
                div_product_prices.setAttribute("class", "product-price");
                
                    if(respObject.searchinformation[i].offer == "1"){
                        var span_old_price = document.createElement("SPAN");
                        span_old_price.setAttribute("class", "old-price");
                        span_old_price.innerHTML = respObject.searchinformation[i].oldprice;
                        
                        
                        var span_price = document.createElement("SPAN");
                        span_price.setAttribute("class", "price");
                        span_price.innerHTML = respObject.searchinformation[i].price;
                div_product_prices.appendChild(span_old_price);
                div_product_prices.appendChild(span_price);
                    }
                    else{
                         var span_price = document.createElement("SPAN");
                         span_price.setAttribute("class", "price");
                        span_price.innerHTML = respObject.searchinformation[i].price;
                div_product_prices.appendChild(span_price);        
                    }
                    
                div_product_details.appendChild(div_product_prices);
                
                
                var div_colors = document.createElement("DIV");
                div_colors.setAttribute("class", "filter-color swacth-list clearfix");
                
                var objColors = JSON.parse(respObject.searchinformation[i].colors);
                jQuery.each(objColors, function(i, val) {
                 
                 var span_color = document.createElement("SPAN");
                 span_color.setAttribute("class", "swacth-btn "+i);
                 div_colors.appendChild(span_color);
                });
                
                div_product_details.appendChild(div_colors);
            div_container.appendChild(div_product_details);
            
                
        document.getElementById("catalog").appendChild(div_container);
                
                    
             }
              filter_by_url();
            
              }
    }
   
       }
       function filter_by_category(obj, id){
           var filtered_obj = {};
           filtered_obj.searchinformation = [];
           for(i = 0; i<(obj.searchinformation).length; i++){
               if(obj.searchinformation[i].product_category_id == id){
                   filtered_obj.searchinformation.push(obj.searchinformation[i]);
               }
           }
           return filtered_obj;
       }
       
       function filter_by_url(){
           var products_filtered, category_to_filter;
           var cat_name_all;
           var semfore = false;
           if(search_object.category){
               products_filtered = this.products;
               for(var j = 0; j<category_mapping.length; j++){
                    if(search_input_parameter.name != "all"){
                        if(category_mapping[j].name==search_input_parameter.name){
                        category_to_filter = category_mapping[j].id;
                        semfore = true;
                        }
                        else{
                        }
                    }
                    else{
                        cat_name_all = true;
                        continue;
                    }
                    
               }
               if(cat_name_all!=true && semfore==true){
                   products_filtered = filter_by_category(products_filtered, category_to_filter);
                   
                   this.locals = products_filtered;
                   display_Products(products_filtered);
               }
               else{
                   display_Products(this.products);
                   this.locals = this.products;
                   
               }
               
               
           }
           else if(search_object.features){
               
                var products_filtered_by_input = this.products;
                var features_products_filtered = {};
                 features_products_filtered.searchinformation = [];
                for(var j = 0; j<products_filtered_by_input.searchinformation.length; j++){
                    var features_object = JSON.parse((products_filtered_by_input.searchinformation[j]).features);
                    for(var features in features_object){
                    
                        if((this.search_bar_feature.toLowerCase()).includes(features) || features=="any"){
                            features_products_filtered.searchinformation.push(products_filtered_by_input.searchinformation[j]);
                        }
                    }
                    
                    display_Products(features_products_filtered);
                    
                
               }
           }
           else if(search_object.bar){
               
                var products_filtered_by_input = this.products;
                var features_products_filtered = {};
                 features_products_filtered.searchinformation = [];
                for(var j = 0; j<products_filtered_by_input.searchinformation.length; j++){
                    var features_object = JSON.parse((products_filtered_by_input.searchinformation[j]).features);
                    for(var features in features_object){
                    
                        if((this.search_bar_input.toLowerCase()).includes(features) || features=="any"){
                            console.log("feature"+j);
                            features_products_filtered.searchinformation.push(products_filtered_by_input.searchinformation[j]);
                        }
                    }
                    
                     var colors_object = JSON.parse((products_filtered_by_input.searchinformation[j]).colors);
                    for(var colors in colors_object){
                    
                        if((this.search_bar_input.toLowerCase()).includes(colors) || colors=="any"){
                            console.log("color" +j);
                            features_products_filtered.searchinformation.push(products_filtered_by_input.searchinformation[j]);
                        }
                    }
                    
                    
                }
              debugger;
                display_Products(features_products_filtered);
                
           }
           else{
               
           }
       }
      
       function search_by_size(){
            var size_product_list = [];
         var products_by_size = {searchinformation:[]};  
           var control_variable = 0;
           if(search_object.size){
             for(var i =0; i<this.locals.searchinformation.length;i++){
               for (var prop in this.selected_sizes) {

                 
                  if(this.selected_sizes[prop]==true){
                      switch(prop){
                          case "xs":
                              if(this.locals.searchinformation[i].stock_xs >0){
                                  if(size_product_list.length>0){
                                      if(size_product_list.includes(this.locals.searchinformation[i].id)){
                                          
                                      }
                                      else{
                                           size_product_list.push(this.locals.searchinformation[i].id);
                                        products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                      }
                                     
                                  }
                                  else{
                                      size_product_list.push(this.locals.searchinformation[i].id);
                                     products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                  }
                                  

                                  control_variable++;
                              }
                               break;
                            case "s":
                              if(this.locals.searchinformation[i].stock_s >0){
                                   if(size_product_list.length>0){
                                      if(size_product_list.includes(this.locals.searchinformation[i].id)){
                                          
                                      }
                                      else{
                                           size_product_list.push(this.locals.searchinformation[i].id);
                                        products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                      }
                                     
                                  }
                                  else{
                                      size_product_list.push(this.locals.searchinformation[i].id);
                                      products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                  }
                                  control_variable++;
                              }
                               break;
                            case "m":
                              if(this.locals.searchinformation[i].stock_m >0){
                                  if(size_product_list.length>0){
                                      if(size_product_list.includes(this.locals.searchinformation[i].id)){
                                          
                                      }
                                      else{
                                           size_product_list.push(this.locals.searchinformation[i].id);
                                        products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                      }
                                     
                                  }
                                  else{
                                      size_product_list.push(this.locals.searchinformation[i].id);
                                      products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                  }
                                  control_variable++;
                              }
                               break;
                            case "l":
                              if(this.locals.searchinformation[i].stock_l >0){
                                  if(size_product_list.length>0){
                                      if(size_product_list.includes(this.locals.searchinformation[i].id)){
                                          
                                      }
                                      else{
                                           size_product_list.push(this.locals.searchinformation[i].id);
                                        products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                      }
                                     
                                  }
                                  else{
                                      size_product_list.push(this.locals.searchinformation[i].id);
                                      products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                  }
                                  control_variable++;
                              }
                               break;
                            case "xl":
                              if(this.locals.searchinformation[i].stock_xl >0){
                                   if(size_product_list.length>0){
                                      if(size_product_list.includes(this.locals.searchinformation[i].id)){
                                          
                                      }
                                      else{
                                           size_product_list.push(this.locals.searchinformation[i].id);
                                        products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                      }
                                     
                                  }
                                  else{
                                      size_product_list.push(this.locals.searchinformation[i].id);
                                      products_by_size.searchinformation.push(this.locals.searchinformation[i]);
                                  }
                                  control_variable++;
                              }
                               break;
                               
                      }
                      
                      
                  }
                }
            }  
            if(control_variable == 0){
                display_Products(this.locals);
                return this.locals;
            }
            else{
                display_Products(products_by_size);
                return products_by_size;
            }
            
           }
           
          
       }
       /*
         var colors = {
        black: false,
        white: false,
        red: false,
        blue: false,
        pink: false,
        gray: false,
        green: false,
        orange: false,
        yellow: false,
        brown: false,
        khaki: false
        
    };
       */
       function filter_by_color(){
        var color_product_list = [];
         var products_by_color = {searchinformation:[]};
           var control_variable = 0;
           if(search_object.color){
             for(var i =0; i<this.locals.searchinformation.length;i++){
               for (var prop in this.colors) {

                 
                  if(this.colors[prop]==true){
                      switch(prop){
                          case "black":
                                product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "black"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                  control_variable++;
                              
                               break;
                            case "white":
                                     product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "white"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                  control_variable++;
                              
                               break;
                            case "red":
                             product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "red"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                  control_variable++;
                              
                               break;
                            case "blue":
                              product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "blue"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                  control_variable++;
                               break;
                            case "pink":
                               product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "pink"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                  control_variable++;
                              
                               break;
                               
                            case "gray":
                                 product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "gray"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                control_variable++;
                                break;
                            case "green":
                                 product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "green"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                control_variable++;
                                break;
                            
                            case "orange":
                                 product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "orange"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                control_variable++;
                                break;
                            case "yellow":
                                 product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "yellow"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                control_variable++;
                                break;
                            case "khaki":
                                 product_colors = JSON.parse(this.locals.searchinformation[i].colors)
                                for(var colores in product_colors){
                                    if(colores == "khaki"){
                                        products_by_color.searchinformation.push(this.locals.searchinformation[i]);
                                    }
                                }
                                control_variable++;
                                break;
                               
                      }
                      
                      
                  }
                }
            }  
            if(control_variable == 0){
                display_Products(this.locals);
                
            }
            else{
                display_Products(products_by_color);
            }
            
           }
           return products_by_color;
       }
       function display_Products(proto){
           document.getElementById("catalog").innerHTML = "";
           debugger;
           document.getElementById("showing").innerHTML = (proto.searchinformation).length +" Resultados";
             for(i = 0; i<(proto.searchinformation).length; i++){
             var div_container = document.createElement("DIV");
             div_container.setAttribute("class", 'col-6 col-sm-6 col-md-4 col-lg-4 item');
    
                var div_product_image = document.createElement("DIV");
                div_product_image.setAttribute("class", "product-image");
                
                    var a_image = document.createElement("A");
                    a_image.setAttribute("href", "#");
                    
                        var image = document.createElement("IMG");
                        image.setAttribute("class", 'primary blur-up ls-is-cached lazyloaded');
                        image.src = proto.searchinformation[i].images;
                        image.setAttribute("data-src", proto.searchinformation[i].images);
                        
                        
                            var div_tags = document.createElement("DIV");
                            div_tags.setAttribute("class", 'product-labels rectangular');
                            if(proto.searchinformation[i].offer == "1"){
                                var span_offer = document.createElement("SPAN");
                                span_offer.setAttribute("class", 'lbl on-sale');
                                span_offer.innerHTML = proto.searchinformation[i].sale + "%";
                                div_tags.appendChild(span_offer);
                            }
                        a_image.appendChild(image);
                        a_image.appendChild(div_tags);
                div_product_image.appendChild(a_image);
                
                    var form_button = document.createElement("FORM");
                    form_button.setAttribute("class","variants add");
                    form_button.setAttribute("action","#");
                    form_button.setAttribute("onclick","window.location.href='product-accordion.php'");
                        var submit_button = document.createElement("BUTTON");
                        submit_button.setAttribute("class", "btn btn-addto-cart");
                        submit_button.setAttribute("type", "button");
                        submit_button.innerHTML = "VER AHORA";
                    form_button.appendChild(submit_button);
                div_product_image.appendChild(form_button);
                
                
                    var div_button_collections = document.createElement("DIV");
                    div_button_collections.setAttribute("class", 'button-set');
                    
                        var a_modal = document.createElement("A");
                        a_modal.setAttribute("href", "javascript:void");
                        a_modal.setAttribute("title", "ver modal");
                        a_modal.setAttribute("class", "quick-view-popup quick-view");
                        a_modal.setAttribute("data-toggle", "modal");
                        a_modal.setAttribute("data-target", "#content_quickview");
                        
                            var icon_modal = document.createElement("I");
                            icon_modal.setAttribute("class", "icon anm anm-search-plus-r");
                        a_modal.appendChild(icon_modal);
                    div_button_collections.appendChild(a_modal);
                    
                    
                        var div_wishlist = document.createElement("DIV");
                        div_wishlist.setAttribute("class", "wishlist-btn");
                        
                            var a_wishlist = document.createElement("A");
                            a_wishlist.setAttribute("href", "#");
                            a_wishlist.setAttribute("title", "Agregar a bolsa");
                            a_wishlist.setAttribute("class", "wishlist add-to-wishlist");
                        
                                var icon_modal = document.createElement("I");
                                icon_modal.setAttribute("class", "icon anm anm-heart-l");
                            
                            a_wishlist.appendChild(icon_modal);
                        div_wishlist.appendChild(a_wishlist);
                    div_button_collections.appendChild(div_wishlist);
                div_product_image.appendChild(div_button_collections);
            div_container.appendChild(div_product_image);
            
            //next div
            
                var div_product_details = document.createElement("DIV");
                div_product_details.setAttribute("class", "product-details text-center");
                
                    var div_name = document.createElement("DIV");
                    div_name.setAttribute("class", "product-name");
                    
                        var a_name = document.createElement("A");
                        a_name.setAttribute("hred", "#");
                        a_name.innerHTML = proto.searchinformation[i].title;
                        
                    div_name.appendChild(a_name);
                div_product_details.appendChild(div_name);
                
                var div_product_prices = document.createElement("DIV");
                div_product_prices.setAttribute("class", "product-price");
                
                    if(proto.searchinformation[i].offer == "1"){
                        var span_old_price = document.createElement("SPAN");
                        span_old_price.setAttribute("class", "old-price");
                        span_old_price.innerHTML = proto.searchinformation[i].oldprice;
                        
                        
                        var span_price = document.createElement("SPAN");
                        span_price.setAttribute("class", "price");
                        span_price.innerHTML = proto.searchinformation[i].price;
                div_product_prices.appendChild(span_old_price);
                div_product_prices.appendChild(span_price);
                    }
                    else{
                         var span_price = document.createElement("SPAN");
                         span_price.setAttribute("class", "price");
                        span_price.innerHTML = proto.searchinformation[i].price;
                div_product_prices.appendChild(span_price);        
                    }
                    
                div_product_details.appendChild(div_product_prices);
                
                
                var div_colors = document.createElement("DIV");
                div_colors.setAttribute("class", "filter-color swacth-list clearfix");
                
                var objColors = JSON.parse(proto.searchinformation[i].colors);
                jQuery.each(objColors, function(i, val) {
                 
                 var span_color = document.createElement("SPAN");
                 span_color.setAttribute("class", "swacth-btn "+i);
                 div_colors.appendChild(span_color);
                });
                
                div_product_details.appendChild(div_colors);
            div_container.appendChild(div_product_details);
            
                
        document.getElementById("catalog").appendChild(div_container);
                
                    
             }
       }
    
    
    
</script>

</html>