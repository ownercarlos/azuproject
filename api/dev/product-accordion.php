<?php
$semaforo = false;
$sku = "";
$images = null;
if(isset($_POST["action"])){
    
    $semaforo = true;
    $action = json_decode($_POST["action"]);
    $sku = $action->sku;
    $servername = "localhost";
    $database = "u931312158_dev_shahidaali";
    $username = "u931312158_shahidaali";
    $password = "dev_fiverrShahidaali1";
    $conn = mysqli_connect($servername, $username, $password, $database);
    $sql = "SELECT * FROM products WHERE products.sku LIKE '$sku'";
    $result = mysqli_query($conn, $sql) or die("<b>Error:</b> Problem getting products<br/>" . mysqli_error($conn));
    $row = $result->fetch_assoc();
    $images = json_decode($row['images']);
}
else{
    $semaforo = false;
}

?>
<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Azú - Vista Producto </title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.png" />
<link rel="apple-touch-icon" href="assets/images/apple-touch-icon-57x57.png" />
<link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png" />
<link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="template-product belle">
	<div class="pageWrapper">
<div id="insertheader">
        
    </div>
    <br><br><br><br>
        <!--Body Content-->
        <div id="page-content">
            <!--MainContent-->
            <div id="MainContent" class="main-content" role="main">
                <!--Breadcrumb-->
                <div class="bredcrumbWrap">
                    <div class="container breadcrumbs">
                        <a href="index.php" title="Back to the home page">Inicio</a><span aria-hidden="true">›</span><span>Producto</span>
                    </div>
                </div>
                <!--End Breadcrumb-->
                
                <div id="ProductSection-product-template" class="product-template__container prstyle2 container">
                    <!--#ProductSection-product-template-->
                    <div class="product-single product-single-1">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="product-details-img product-single__photos bottom">
                                    <div class="zoompro-wrap product-zoom-right pl-20">
                                        <div class="zoompro-span" id="zoompro_span">
                                            <?php if(count($images)>0) { ?>
                                                <img class="blur-up lazyload zoompro" data-zoom-image="/dev/shahidaali/azu-studio/storage/<?=$images[0]?>" alt="" src="/dev/shahidaali/azu-studio/storage/<?=$images[0]?>" />
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="product-thumb product-thumb-1">
                                        <div id="gallery" class="product-dec-slider-1 product-tab-left">
                                            <?php if(count($images) > 0) { ?>
                                            <a data-image="/dev/shahidaali/azu-studio/storage/<?=$images[0]?>" data-zoom-image="/dev/shahidaali/azu-studio/storage/<?=$images[0]?>" class="slick-slide slick-cloned" data-slick-index="0" aria-hidden="true" tabindex="-1">
                                                <img class="blur-up lazyload" src="/dev/shahidaali/azu-studio/storage/<?=$images[0]?>" alt="" />
                                            </a>
                                            <?php
                                                for($x = 1; $x < count($images); $x++){
                                            ?>
                                                <a data-image="/dev/shahidaali/azu-studio/storage/<?=$images[$x]?>" data-zoom-image="/dev/shahidaali/azu-studio/storage/<?=$images[$x]?>" class="slick-slide slick-cloned" data-slick-index="<?=$x?>" aria-hidden="true" tabindex="-1">
                                                    <img class="blur-up lazyload" src="/dev/shahidaali/azu-studio/storage/<?=$images[$x]?>" alt="" />
                                                </a>
                                            <?php
                                                }
                                             } ?>
                                        </div>
                                    </div>
                                    <div class="lightboximages">
                                        <a href="assets/images/product-detail-page/camelia-reversible-big1.jpg" data-size="1462x2048"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big2.jpg" data-size="1462x2048"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big3.jpg" data-size="1462x2048"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible7-big.jpg" data-size="1462x2048"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big4.jpg" data-size="1462x2048"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big5.jpg" data-size="1462x2048"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big6.jpg" data-size="731x1024"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big7.jpg" data-size="731x1024"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big8.jpg" data-size="731x1024"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big9.jpg" data-size="731x1024"></a>
                                        <a href="assets/images/product-detail-page/camelia-reversible-big10.jpg" data-size="731x1024"></a>
                                    </div>
        
                                </div>
                                <!--Product Feature-->
                                <div class="prFeatures">
                                    <div class="row">
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 feature">
                                            <img src="assets/images/credit-card.png" alt="Safe Payment" title="Safe Payment" />
                                            <div class="details"><h3>Pago Seguro</h3>Haz tu pago de manera segura.</div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 feature">
                                            <img src="assets/images/shield.png" alt="Confidence" title="Confidence" />
                                            <div class="details"><h3>Confianza</h3>Proteccion que protege tu informacion.</div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 feature">
                                            <img src="assets/images/worldwide.png" alt="Worldwide Delivery" title="Worldwide Delivery" />
                                            <div class="details"><h3>Envio</h3>Gratis &amp; rapida entrega de tu producto.</div>
                                        </div>
                                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 feature">
                                            <img src="assets/images/phone-call.png" alt="Hotline" title="Hotline" />
                                            <div class="details"><h3>Telefonos</h3>(+)502 59307820</div>
                                        </div>
                                    </div>
                                </div>
                                <!--End Product Feature-->
                                
                                <!--Imagen 3D-->
                            <div id="myCanvas"  ></div>

 

                               
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="product-single__meta">
                                    <h1 class="product-single__title"></h1>
                                    <div class="product-nav clearfix">					
                                        <a href="#" class="next" title="Next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="prInfoRow">
                                        <div class="product-stock">En Inventario: <span class="instock"></span></div>
                                        <div class="product-sku">SKU: <span class="variant-sku"> </span></div>
                                        <div class="product-review"><a class="reviewLink" href="#tab2"><span id="full_stars" class="spr-badge-caption"></span></a></div>
                                    </div>
                                    <p class="product-single__price product-single__price-product-template">
                                        <span class="visually-hidden">Precio</span>
                                        <s id="ComparePrice-product-template"><span id="regular_price" class="money"></span></s>
                                        <span class="product-price__price product-price__price-product-template product-price__sale product-price__sale--single">
                                            <span id="ProductPrice-product-template"><span id="discount_price" class="money"></span></span>
                                        </span>
                                    </p>
                                <div class="product-single__description rte">
                                </div>
                                <form method="post" action="http://annimexweb.com/cart/add" id="product_form_10508262282" accept-charset="UTF-8" class="product-form product-form-product-template hidedropdown" enctype="multipart/form-data">
                                    
                                    <div class="swatch clearfix swatch-1 option2" data-option-index="1">
                                        <div class="product-form__item">
                                          <label class="header" Style: "margin: 24px; padding-left: 32px;">Tamaño: &nbsp &nbsp  
                                          <div data-value="XS" class="swatch-element xs available">
                                            <input class="swatchInput" id="swatch-1-xs" type="radio" name="option-1" value="XS">
                                            <label class="swatchLbl small" for="swatch-1-xs" title="XS">XS</label>
                                          </div>
                                          <div data-value="S" class="swatch-element s available">
                                            <input class="swatchInput" id="swatch-1-s" type="radio" name="option-1" value="S">
                                            <label class="swatchLbl small" for="swatch-1-s" title="S">S</label>
                                          </div>
                                          <div data-value="M" class="swatch-element m available">
                                            <input class="swatchInput" id="swatch-1-m" type="radio" name="option-1" value="M">
                                            <label class="swatchLbl small" for="swatch-1-m" title="M">M</label>
                                          </div>
                                          <div data-value="L" class="swatch-element l available">
                                            <input class="swatchInput" id="swatch-1-l" type="radio" name="option-1" value="L">
                                            <label class="swatchLbl small" for="swatch-1-l" title="L">L</label>
                                          </div>
                                          <div data-value="XL" class="swatch-element xl available">
                                            <input class="swatchInput" id="swatch-1-xl" type="radio" name="option-1" value="XL">
                                            <label class="swatchLbl small" for="swatch-1-xl" title="XL">XL</label>
                                          </div>
                                        </div>
                                        </label>
                                    </div>
                                    <p class="infolinks"><u><b></b><a href="https://api.azu-studio.com/dev/contact.php" > Preguntar del Producto</a></p></b></u>
                                    <!-- Product Action -->
                                    <div class="product-action clearfix">
                                        <div class="product-form__item--quantity">
                                            <div class="wrapQtyBtn">
                                                <div class="qtyField">
                                                    <a class="qtyBtn minus" href="javascript:void(0);"><i class="fa anm anm-minus-r" aria-hidden="true"></i></a>
                                                    <input type="text" id="Quantity" name="quantity" value="1" class="product-form__input qty">
                                                    <a class="qtyBtn plus" href="javascript:void(0);"><i class="fa anm anm-plus-r" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                        </div>                                
                                        <div class="product-form__item--submit">
                                            <button type="button" name="add" class="btn product-form__cart-submit" style=" background-color: #c11718">
                                                <span>Añadir al Carrito</span>
                                            </button>
                                        </div>
                                    </div>
                                    <!-- End Product Action -->
                                </form>
                                <div class="display-table shareRow">
                                        <div class="display-table-cell medium-up--one-third">
                                            <div class="wishlist-btn">
                                                <a class="wishlist add-to-wishlist" id="a_like" title="Add to Wishlist"><i class="icon anm anm-heart-l" aria-hidden="true"></i> <span>Añadir a Favoritos</span></a>
                                            </div>
                                        </div>
                                        <div class="display-table-cell text-right">
                                            <div class="social-sharing">
                                                <a target="_blank" href="#" class="btn btn--small btn--secondary btn--share share-facebook" title="Share on Facebook">
                                                    <i class="fa fa-facebook-square" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Compartir</span>
                                                </a>
                                                <a target="_blank" href="#" class="btn btn--small btn--secondary btn--share share-twitter" title="Tweet on Twitter">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Tweet</span>
                                                </a>
                                                <a href="#" title="Share on google+" class="btn btn--small btn--secondary btn--share" >
                                                    <i class="fa fa-google-plus" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Google+</span>
                                                </a>
                                                <a target="_blank" href="#" class="btn btn--small btn--secondary btn--share share-pinterest" title="Pin on Pinterest">
                                                    <i class="fa fa-pinterest" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Pin it</span>
                                                </a>
                                                <a href="#" class="btn btn--small btn--secondary btn--share share-pinterest" title="Share by Email" target="_blank">
                                                    <i class="fa fa-envelope" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Email</span>
                                                </a>
                                             </div>
                                        </div>
                                    </div>
                            </div>
                            	<!--Product Tabs-->
                                <div class="tabs-listing">
                                    <div class="tab-container">
                                    	<h3 class="acor-ttl active" rel="tab1">Te Aseguramos Que</h3>
                                        <div id="tab1" class="tab-content">
                                            <div class="product-description rte">
                                                <p>Los productos son de calidad y te encantaran debido a que mantenemos con nosotros la mejor forma de mejora en productos para mujeres y su calidad en telas. Ademas te podemos asegurar que todas las compras que realices no te desepcionaran porque somos los mejores en la calidad de nuestros servicios.</p>
                                            </div>
                                        </div>
                                        <h3 class="acor-ttl" rel="tab2">Reseñas del Producto</h3>
                                        <div id="tab2" class="tab-content">
                                            <div id="shopify-product-reviews">
                                                <div class="spr-container">
                                                    <div class="spr-header clearfix">
                                                        <div class="spr-summary" id="spr_summary">
                                                            
                                                            <span class="spr-summary-actions">
                                                                <a href="#" class="spr-summary-actions-newreview btn">Escribe una reseña</a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="spr-content">
                                                        <div class="spr-form clearfix">
                                                            <h3 class="spr-form-title">Escribe una reseña</h3>
                                                            <fieldset class="spr-form-review">
                                                                <input type="hidden" name="rating" id="rating-input" min="1" max="5" value="1"/>
                                                                <div class="rating" role="optgroup">
                                                                  <!-- in Rails just use 1.upto(5) -->  
                                                                  <i class="fa fa-star-o fa-2x rating-star" id="rating-1" data-rating="1" tabindex="0" aria-label="Califica con una de 5 estrellas" role="radio"></i>
                                                                  <i class="fa fa-star-o fa-2x rating-star" id="rating-2" data-rating="2" tabindex="0" aria-label="Califica con dos de 5 estrellas" role="radio"></i>
                                                                  <i class="fa fa-star-o fa-2x rating-star" id="rating-3" data-rating="3" tabindex="0" aria-label="Califica con tres de 5 estrellas" role="radio"></i>
                                                                  <i class="fa fa-star-o fa-2x rating-star" id="rating-4" data-rating="4" tabindex="0" aria-label="Califica con cuatro de 5 estrellas" role="radio"></i>
                                                                  <i class="fa fa-star-o fa-2x rating-star" id="rating-5" data-rating="5" tabindex="0" aria-label="Califica con cinco de 5 estrellas" role="radio"></i>
                                                                </div>
                                                            <fieldset class="spr-form-contact">
                                                                <div class="spr-form-contact-name">
                                                                  <label class="spr-form-label" for="review_author_10508262282">Edad</label>
                                                                  <input class="spr-form-input spr-form-input-text " id="review_age" type="number" name="age" placeholder="Ingresa tu edad" min="0">
                                                                </div>
                                                            </fieldset>
                                                              <div class="spr-form-review-title">
                                                                <label class="spr-form-label" for="review_title_10508262282">Título</label>
                                                                <input class="spr-form-input spr-form-input-text " id="review_title" type="text" name="title" placeholder="Dale un título a tu reseña">
                                                              </div>
                                                        
                                                              <div class="spr-form-review-body">
                                                                <label class="spr-form-label" for="review_body_10508262282">Cuerpo de la reseña:<span class="spr-form-review-body-charactersremaining">(1500)</span></label>
                                                                <div class="spr-form-input">
                                                                  <textarea class="spr-form-input spr-form-input-textarea " id="review_text"  name="reviewText" rows="10" placeholder="Escribe tus comentarios aquí"></textarea>
                                                                </div>
                                                              </div>
                                                            </fieldset>
                                                            <input type="hidden" id="marca">
                                                            <input type="hidden" id="categoryName">
                                                            <fieldset class="spr-form-actions">
                                                                <input type="submit" class="spr-button spr-button-primary button button-primary btn btn-primary" id="reivew_sender" value="Enviar reseña">
                                                            </fieldset>
                                                        </div>
                                                        <div class="spr-reviews" id="spr_reviews">
                                                            
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <h3 class="acor-ttl" rel="tab3">Tabla de Tallas</h3>
                                        <div id="tab3" class="tab-content">
                                            <h3>Tallas Segun Tu Cuerpo</h3>
                                            
                                            <div class="text-center" id="body_image">
                                                <img src="assets/images/size.jpg" alt="" />
                                            </div>
                                      </div>
                                        <h3 class="acor-ttl" rel="tab4">Envio &amp; Devoluciones</h3>
                                        <div id="tab4" class="tab-content">
                                            <h4>Politica de Retorno</h4>
                                            <p>Puedes hacer devolucion o cambio del producto dentro de los primeros 60 dias luego de tu compra.</p>
                                            <h4>Envios</h4>
                                            <p>Para los envios se realizaran por medio de la plataforma en donde se hara la entrega del producto 24 horas luego de tu compra y verificacion de tus credenciales.</p>
                                        </div>
                                    </div>
                                </div>
                                <!--End Product Tabs-->
                        	</div>
                    	</div>
                    <!--End-product-single-->
                    
                    <!--Related Product Slider-->
                    <div class="related-product grid-products">
                        <header class="section-header">
                            <h2 class="section-header__title text-center h2"><span>Productos Relacionados</span></h2>
                        </header>
                        <div class="productPageSlider">
                            <div class="col-12 item">
                                <!-- start product image -->
                                <div class="product-image">
                                    <!-- start product image -->
                                    <a href="#">
                                        <!-- image -->
                                        <img class="primary blur-up lazyload" data-src="assets/images/product-images/product-image1.jpg" src="assets/images/product-images/product-image1.jpg" alt="image" title="product">
                                        <!-- End image -->
                                        <!-- Hover image -->
                                        <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image1-1.jpg" src="assets/images/product-images/product-image1-1.jpg" alt="image" title="product">
                                        <!-- End hover image -->
                                        <!-- product label -->
                                        <div class="product-labels rectangular"><span class="lbl on-sale">Sale</span> <span class="lbl pr-label1">new</span></div>
                                        <!-- End product label -->
                                    </a>
                                    <!-- end product image -->
        
                                    <!-- Start product button -->
                                    <form class="variants add" action="#" onclick="window.location.href='cart.html'"method="post">
                                        <button class="btn btn-addto-cart" type="button" tabindex="0">Ver Producto</button>
                                    </form>
                                    <div class="button-set">
                                        <a href="#" title="Quick View" class="quick-view" tabindex="0">
                                            <i class="icon anm anm-search-plus-r"></i>
                                        </a>
                                        <div class="wishlist-btn">
                                            <a class="wishlist add-to-wishlist" href="wishlist.html">
                                                <i class="icon anm anm-heart-l"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- end product button -->
                                </div>
                                <!-- end product image -->
                                <!--start product details -->
                                <div class="product-details text-center">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="#">Edna Dress</a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <span class="old-price">$500.00</span>
                                        <span class="price">$600.00</span>
                                    </div>
                                    <!-- End product price -->
                                    
                                    <div class="product-review">
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star-o"></i>
                                        <i class="font-13 fa fa-star-o"></i>
                                    </div>
                                    <!-- Variant -->
                                    <ul class="swatches">
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant1.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant2.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant4.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant5.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant6.jpg" alt="image" /></li>
                                    </ul>
                                    <!-- End Variant -->
                                </div>
                                <!-- End product details -->
                            </div>
                            <div class="col-12 item">
                                <!-- start product image -->
                                <div class="product-image">
                                    <!-- start product image -->
                                    <a href="#">
                                        <!-- image -->
                                        <img class="primary blur-up lazyload" data-src="assets/images/product-images/product-image2.jpg" src="assets/images/product-images/product-image2.jpg" alt="image" title="product">
                                        <!-- End image -->
                                        <!-- Hover image -->
                                        <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image2-1.jpg" src="assets/images/product-images/product-image2-1.jpg" alt="image" title="product">
                                        <!-- End hover image -->
                                    </a>
                                    <!-- end product image -->
        
                                    <!-- Start product button -->
                                    <form class="variants add" action="#" onclick="window.location.href='cart.html'"method="post">
                                        <button class="btn btn-addto-cart" type="button" tabindex="0">Ver Producto</button>
                                    </form>
                                    <div class="button-set">
                                        <a href="#" title="Quick View" class="quick-view" tabindex="0">
                                            <i class="icon anm anm-search-plus-r"></i>
                                        </a>
                                        <div class="wishlist-btn">
                                            <a class="wishlist add-to-wishlist" href="wishlist.html">
                                                <i class="icon anm anm-heart-l"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- end product button -->
                                </div>
                                <!-- end product image -->
        
                                <!--start product details -->
                                <div class="product-details text-center">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="#">Elastic Waist Dress</a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <span class="price">$748.00</span>
                                    </div>
                                    <!-- End product price -->
                                    <div class="product-review">
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                    </div>
                                    <!-- Variant -->
                                    <ul class="swatches">
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant2-1.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant2-2.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant2-3.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant2-4.jpg" alt="image" /></li>
                                    </ul>
                                    <!-- End Variant -->
                                </div>
                                <!-- End product details -->
                            </div>
                            <div class="col-12 item">
                                <!-- start product image -->
                                <div class="product-image">
                                    <!-- start product image -->
                                    <a href="#">
                                        <!-- image -->
                                        <img class="primary blur-up lazyload" data-src="assets/images/product-images/product-image3.jpg" src="assets/images/product-images/product-image3.jpg" alt="image" title="product">
                                        <!-- End image -->
                                        <!-- Hover image -->
                                        <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image3-1.jpg" src="assets/images/product-images/product-image3-1.jpg" alt="image" title="product">
                                        <!-- End hover image -->
                                        <!-- product label -->
                                        <div class="product-labels rectangular"><span class="lbl pr-label2">Hot</span></div>
                                        <!-- End product label -->
                                    </a>
                                    <!-- end product image -->
        
                                    <!-- Start product button -->
                                    <form class="variants add" action="#" onclick="window.location.href='cart.html'"method="post">
                                        <button class="btn btn-addto-cart" type="button" tabindex="0">Ver Producto</button>
                                    </form>
                                    <div class="button-set">
                                        <a href="#" title="Quick View" class="quick-view" tabindex="0">
                                            <i class="icon anm anm-search-plus-r"></i>
                                        </a>
                                        <div class="wishlist-btn">
                                            <a class="wishlist add-to-wishlist" href="wishlist.html">
                                                <i class="icon anm anm-heart-l"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- end product button -->
                                </div>
                                <!-- end product image -->
        
                                <!--start product details -->
                                <div class="product-details text-center">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="#">3/4 Sleeve Kimono Dress</a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <span class="price">$550.00</span>
                                    </div>
                                    <!-- End product price -->
                                    
                                    <div class="product-review">
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star-o"></i>
                                    </div>
                                    <!-- Variant -->
                                    <ul class="swatches">
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-1.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-2.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-3.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-4.jpg" alt="image" /></li>
                                    </ul>
                                    <!-- End Variant -->
                                </div>
                                <!-- End product details -->
                            </div>
                            <div class="col-12 item">
                                <!-- start product image -->
                                <div class="product-image">
                                    <!-- start product image -->
                                    <a href="#">
                                        <!-- image -->
                                        <img class="primary blur-up lazyload" data-src="assets/images/product-images/product-image4.jpg" src="assets/images/product-images/product-image4.jpg" alt="image" title="product" />
                                        <!-- End image -->
                                        <!-- Hover image -->
                                        <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image4-1.jpg" src="assets/images/product-images/product-image4-1.jpg" alt="image" title="product" />
                                        <!-- End hover image -->
                                        <!-- product label -->
                                        <div class="product-labels"><span class="lbl on-sale">Sale</span></div>
                                        <!-- End product label -->
                                    </a>
                                    <!-- end product image -->
        
                                    <!-- Start product button -->
                                    <form class="variants add" action="#" onclick="window.location.href='cart.html'"method="post">
                                        <button class="btn btn-addto-cart" type="button" tabindex="0">Ver Producto</button>
                                    </form>
                                    <div class="button-set">
                                        <a href="#" title="Quick View" class="quick-view" tabindex="0">
                                            <i class="icon anm anm-search-plus-r"></i>
                                        </a>
                                        <div class="wishlist-btn">
                                            <a class="wishlist add-to-wishlist" href="wishlist.html">
                                                <i class="icon anm anm-heart-l"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- end product button -->
                                </div>
                                <!-- end product image -->
        
                                <!--start product details -->
                                <div class="product-details text-center">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="#">Cape Dress</a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <span class="old-price">$900.00</span>
                                        <span class="price">$788.00</span>
                                    </div>
                                    <!-- End product price -->
                                    
                                    <div class="product-review">
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star-o"></i>
                                        <i class="font-13 fa fa-star-o"></i>
                                    </div>
                                    <!-- Variant -->
                                    <ul class="swatches">
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant4-1.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant4-2.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant4-3.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant4-4.jpg" alt="image" /></li>
                                    </ul>
                                    <!-- End Variant -->
                                </div>
                                <!-- End product details -->
                            </div>
                            <div class="col-12 item">
                                <!-- start product image -->
                                <div class="product-image">
                                    <!-- start product image -->
                                    <a href="#">
                                        <!-- image -->
                                        <img class="primary blur-up lazyload" data-src="assets/images/product-images/product-image5.jpg" src="assets/images/product-images/product-image5.jpg" alt="image" title="product" />
                                        <!-- End image -->
                                        <!-- Hover image -->
                                        <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image5-1.jpg" src="assets/images/product-images/product-image5-1.jpg" alt="image" title="product" />
                                        <!-- End hover image -->
                                        <!-- product label -->
                                        <div class="product-labels"><span class="lbl on-sale">Sale</span></div>
                                        <!-- End product label -->
                                    </a>
                                    <!-- end product image -->
        
                                    <!-- Start product button -->
                                    <form class="variants add" action="#" onclick="window.location.href='cart.html'"method="post">
                                        <button class="btn btn-addto-cart" type="button" tabindex="0">Ver Producto</button>
                                    </form>
                                    <div class="button-set">
                                        <a href="#" title="Quick View" class="quick-view" tabindex="0">
                                            <i class="icon anm anm-search-plus-r"></i>
                                        </a>
                                        <div class="wishlist-btn">
                                            <a class="wishlist add-to-wishlist" href="wishlist.html">
                                                <i class="icon anm anm-heart-l"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <!-- end product button -->
                                </div>
                                <!-- end product image -->
        
                                <!--start product details -->
                                <div class="product-details text-center">
                                    <!-- product name -->
                                    <div class="product-name">
                                        <a href="#">Paper Dress</a>
                                    </div>
                                    <!-- End product name -->
                                    <!-- product price -->
                                    <div class="product-price">
                                        <span class="price">$550.00</span>
                                    </div>
                                    <!-- End product price -->
                                    
                                    <div class="product-review">
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                        <i class="font-13 fa fa-star"></i>
                                    </div>
                                    <!-- Variant -->
                                    <ul class="swatches">
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-1.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-2.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-3.jpg" alt="image" /></li>
                                        <li class="swatch medium rounded"><img src="assets/images/product-images/variant3-4.jpg" alt="image" /></li>
                                    </ul>
                                    <!-- End Variant -->
                                </div>
                                <!-- End product details -->
                            </div>
                        </div>
                        </div>
                    <!--End Related Product Slider-->
                    </div>
                	<!--#ProductSection-product-template-->
            	</div>
            <!--MainContent-->
        </div>
    	<!--End Body Content-->
    	</div>
    <!--Footer-->
    <footer id="footer">
       
    </footer>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll" style=" background-color: #c11718"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
    <div class="hide">
    	<div id="productInquiry">
        	<div class="contact-form form-vertical">
          <div class="page-title">
            <h3>Azu</h3>
          </div>
          <form method="post" action="#" id="contact_form" class="contact-form">
            <input type="hidden" name="form_type" value="contact" />
            <input type="hidden" name="utf8" value="✓" />
            <div class="formFeilds">
              <input type="hidden"  name="contact[product name]" value="Camelia Reversible Jacket">
              <input type="hidden"  name="contact[product link]" value="/products/camelia-reversible-jacket-black-red">
              <div class="row">
                  <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                  	<input type="text" id="ContactFormName" name="contact[name]" placeholder="Nombre"  value="" required>
                  </div>
              </div>
              <div class="row">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                  <input type="email" id="ContactFormEmail" name="contact[email]" placeholder="Email"  autocapitalize="off" value="" required>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <input required type="tel" id="ContactFormPhone" name="contact[phone]" pattern="[0-9\-]*" placeholder="Telefono"  value="">
                </div>
              </div>
              <div class="row">
              	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
              		<textarea required rows="10" id="ContactFormMessage" name="contact[body]" placeholder="Mensaje" ></textarea>
              	</div>  
              </div>
              <div class="row">
              	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
              		<input type="submit" class="btn" value="Enviar mensaje">
                </div>
             </div>
            </div>
          </form>
        </div>
      	</div>
    </div>
    
        
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
     <!-- Photoswipe Gallery -->
     <script src="assets/js/vendor/photoswipe.min.js"></script>
     <script src="assets/js/vendor/photoswipe-ui-default.min.js"></script>
     <script src="assets/js/rating_stars.js"></script>
     <script>
        var product_sku = "<?=$sku?>";
        debugger;
         document.getElementById("a_like").setAttribute("href" ,"javascript:addLikes('"+product_sku+"')");
        $(function(){
            $('#reivew_sender').on('click', function(){
                $.post('https://api.azu-studio.com/review/insertReview.php',
                {
                    sku: product_sku,
                    age: $('#review_age').val(),
                    title: $('#review_title').val(),
                    rating: $('#rating-input').val(),
                    reviewText: $('#review_text').val(),
                    marca: $('#marca').val(),
                    categoryName: $('#categoryName').val()
                },
                function(data){
                   console.log(data); 
                });
                
                
                var reviews_html = '<div class="spr-review">';
                    reviews_html += '   <div class="spr-review-header">';
                    reviews_html += '       <span class="product-review spr-starratings spr-review-header-starratings">';
                    reviews_html += '           <span class="reviewLink">';
                    var lrate = $('#rating-input').val();
                    for(var x = 1; x <= lrate; x++){
                        reviews_html += '<i class="font-13 fa fa-star"></i>';
                    }
                
                    for(var x = 5; x > lrate; x--){
                        reviews_html += '<i class="font-13 fa fa-star-o"></i>';
                    }
                    reviews_html += '           </span>';
                    reviews_html += '<h3 class="spr-review-header-title">'+$('#review_title').val()+'</h3>';
                    reviews_html += '<div class="spr-review-content">';
                    reviews_html += '    <p class="spr-review-content-body">'+$('#review_text').val()+'</p>';
                    reviews_html += '</div>';
                    reviews_html += '</div>';
                $('#spr_reviews').append(reviews_html);
                
            });
            
            var $pswp = $('.pswp')[0],
                image = [],
                getItems = function() {
                    var items = [];
                    $('.lightboximages a').each(function() {
                        var $href   = $(this).attr('href'),
                            $size   = $(this).data('size').split('x'),
                            item = {
                                src : $href,
                                w: $size[0],
                                h: $size[1]
                            }
                            items.push(item);
                    });
                    return items;
                }
            var items = getItems();
        
            $.each(items, function(index, value) {
                image[index]     = new Image();
                image[index].src = value['src'];
            });
            
            $.get("https://api.azu-studio.com/producto/getProduct.php?sku="+product_sku,
            function(data){
                var obj_data = JSON.parse(data);
                console.log(obj_data);
                $('.product-single__title').text(obj_data.title);
                $('.variant-sku').text(obj_data.sku);
                $('.instock').text(obj_data.stock);
                $('#marca').val(obj_data.brand);
                $('#categoryName').val(obj_data.category.name);
                    
                if(obj_data.discount > 0){
                    var new_price = obj_data.price - obj_data.price*obj_data.discount;
                    $('#discount_price').text("Q " + new_price);
                    $('#regular_price').text("Q " + obj_data.price);
                } else {
                    $('#discount_price').text("Q " + obj_data.price);
                    $('#regular_price').css({
                        'display': 'none'
                    });
                }
                $('.product-single__description').append('<p>'+obj_data.description+'</p>');
                $('#full_stars').text(obj_data.reviews.length + ' reviews');
                var rate = 0;
                obj_data.reviews.forEach(function(review){
                    rate += parseInt(review.rating);
                });
                
                if(obj_data.reviews.length > 0){
                    rate = rate / obj_data.reviews.length;
                }
                
                rate = parseInt(rate);
                
                var html = "";
                for(var x = 1; x <= rate; x++){
                    html += '<i class="font-13 fa fa-star"></i>';
                }
                
                for(var x = 5; x > rate; x--){
                    html += '<i class="font-13 fa fa-star-o"></i>';
                }
                $('#full_stars').before(html);
                $('#spr_summary').append(html + '&nbsp;<span class="spr-summary-actions-togglereviews">Basado en '+obj_data.reviews.length+' reseñas</span>');
                
                
                var reviews_html = '<div class="spr-review">';
                    reviews_html += '   <div class="spr-review-header">';
                    reviews_html += '       <span class="product-review spr-starratings spr-review-header-starratings">';
                    reviews_html += '           <span class="reviewLink">';
                obj_data.reviews.forEach(function(review){
                    var lrate = review.rating;
                    for(var x = 1; x <= lrate; x++){
                    reviews_html += '<i class="font-13 fa fa-star"></i>';
                }
                
                for(var x = 5; x > lrate; x--){
                    reviews_html += '<i class="font-13 fa fa-star-o"></i>';
                }
                    reviews_html += '           </span>';
                    reviews_html += '<h3 class="spr-review-header-title">'+review.title+'</h3>';
                    reviews_html += '<div class="spr-review-content">';
                    reviews_html += '    <p class="spr-review-content-body">'+review.reviewText+'</p>';
                    reviews_html += '</div>';
                });
                    reviews_html += '</div>';
                $('#spr_reviews').append(reviews_html);
                
                var xsmall = JSON.parse(obj_data.size_description.extrasmall);
                var small = JSON.parse(obj_data.size_description.small);
                var medium = JSON.parse(obj_data.size_description.medium);
                var large = JSON.parse(obj_data.size_description.large);
                var xlarge = JSON.parse(obj_data.size_description.extralarge);
                var size_html = '<table>';
                    size_html += '  <tbody>';
                    size_html += '    <tr>';
                    size_html += '      <th>Tamaño</th>';
                    size_html += '      <th>XS</th>';
                    size_html += '      <th>S</th>';
                    size_html += '      <th>M</th>';
                    size_html += '      <th>L</th>';
                    size_html += '      <th>XL</th>';
                    size_html += '    </tr>';
                var busto_html = '      <tr>';
                    busto_html += '      <td>Busto</td>';
                    busto_html += '      <td>'+xsmall.busto+'</td>';
                    busto_html += '      <td>'+small.busto+'</td>';
                    busto_html += '      <td>'+medium.busto+'</td>';
                    busto_html += '      <td>'+large.busto+'</td>';
                    busto_html += '      <td>'+xlarge.busto+'</td>';
                    busto_html += '    </tr>';
                var bra_html = '      <tr>';
                    bra_html += '      <td>Bra</td>';
                    bra_html += '      <td>'+xsmall.bra+'</td>';
                    bra_html += '      <td>'+small.bra+'</td>';
                    bra_html += '      <td>'+medium.bra+'</td>';
                    bra_html += '      <td>'+large.bra+'</td>';
                    bra_html += '      <td>'+xlarge.bra+'</td>';
                    bra_html += '    </tr>';
                var cintura_html = '      <tr>';
                    cintura_html += '      <td>Cintura</td>';
                    cintura_html += '      <td>'+xsmall.cintura+'</td>';
                    cintura_html += '      <td>'+small.cintura+'</td>';
                    cintura_html += '      <td>'+medium.cintura+'</td>';
                    cintura_html += '      <td>'+large.cintura+'</td>';
                    cintura_html += '      <td>'+xlarge.cintura+'</td>';
                    cintura_html += '    </tr>';
                var cadera_html = '      <tr>';
                    cadera_html += '      <td>Cadera</td>';
                    cadera_html += '      <td>'+xsmall.cadera+'</td>';
                    cadera_html += '      <td>'+small.cadera+'</td>';
                    cadera_html += '      <td>'+medium.cadera+'</td>';
                    cadera_html += '      <td>'+large.cadera+'</td>';
                    cadera_html += '      <td>'+xlarge.cadera+'</td>';
                    cadera_html += '    </tr>';
                var piernas_html = '      <tr>';
                    piernas_html += '      <td>Piernas</td>';
                    piernas_html += '      <td>'+xsmall.piernas+'</td>';
                    piernas_html += '      <td>'+small.piernas+'</td>';
                    piernas_html += '      <td>'+medium.piernas+'</td>';
                    piernas_html += '      <td>'+large.piernas+'</td>';
                    piernas_html += '      <td>'+xlarge.piernas+'</td>';
                    piernas_html += '    </tr>';
                    size_html += busto_html;
                    size_html += bra_html;
                    size_html += cintura_html;
                    size_html += cadera_html;
                    size_html += piernas_html;
                    size_html += '  </tbody>';
                    size_html += '</table>';
                $('#body_image').before(size_html);
            });
            
            $('.prlightbox').on('click', function (event) {
                event.preventDefault();
              
                var $index = $(".active-thumb").parent().attr('data-slick-index');
                $index++;
                $index = $index-1;
        
                var options = {
                    index: $index,
                    bgOpacity: 0.9,
                    showHideOpacity: true
                }
                var lightBox = new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options);
                lightBox.init();
            });
        });
        </script>
    </div>

	<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
        	<div class="pswp__bg"></div>
            <div class="pswp__scroll-wrap"><div class="pswp__container"><div class="pswp__item"></div><div class="pswp__item"></div><div class="pswp__item"></div></div><div class="pswp__ui pswp__ui--hidden"><div class="pswp__top-bar"><div class="pswp__counter"></div><button class="pswp__button pswp__button--close" title="Close (Esc)"></button><button class="pswp__button pswp__button--share" title="Share"></button><button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button><button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button><div class="pswp__preloader"><div class="pswp__preloader__icn"><div class="pswp__preloader__cut"><div class="pswp__preloader__donut"></div></div></div></div></div><div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap"><div class="pswp__share-tooltip"></div></div><button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button><button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button><div class="pswp__caption"><div class="pswp__caption__center"></div></div></div></div></div>

<script>
    $("#insertheader").load("header_component.php", function(){
                         console.log("header component loaded succesfully");
                     });
    $("#footer").load("footer_component.php", function(){
                         console.log("footer component loaded succesfully");
                     });
</script>

<!-- Load 3D model -->
<script>
    $("#myCanvas").load("threejs_example.php", function(){
                         console.log("3D component loaded succesfully");
                     });
</script>
<!-- finish 3D -->

<!-- LIKES -->

<script >
    
    function addLikes(sku){
    if(Cookies.get('likes') != undefined){
        //logic for likes
        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
         const found = jar.find(element => element == sku.id);
         if(found != undefined){
             console.log("Process found.");
         }
         else{
            
            jar.push(sku.id);
         }
        let monster = JSON.stringify(jar);
        jQuery.cookie('likes', monster, { expires: 30 });
        
        //logic for mapping

      
        updateLikes();
    }
    else{
        var arr = new Array();
        arr.push(sku.id)
        arr = JSON.stringify(arr);
        jQuery.cookie('likes', arr, { expires: 30 });
        jQuery.cookie('likes_map', JSON.stringify({}), {expires: 30});
        updateLikes();

    }

    
}



function getLikes(arrStringed){
    
    var obj = JSON.parse(arrStringed);
    var iterator = 0;
    var product_sku ="";
    var finalprice ="0";

             var action = JSON.stringify({"producto":{"operation":"select","id":"", "sku":obj, "precio":"", "descuento":"", "peso":"", "oferta":"", "stock":"","colores":"", "activo":"1", "marca":"", "titulo":"", "descripcion":"", "features":""}});

        product_sku = obj;
         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/likes/getLikes.php', true );
         xhr.send(fd);
         xhr.onreadystatechange = function ( response ) {
             if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
             var respObject = JSON.parse(xhr.response);
             //EMPIEZA LLAMADA A API PARA IMAGEN
             for(var i = 0; i < product_sku.length; i++){
             
             
             var action = JSON.stringify({"producto": {"operation": "select", "sku":respObject[i].sku}});
                 var fd = new FormData();
                 fd.append("action", action);
                //document.getElementById('action').value = JSON.stringify(obj);
                var formData = document.getElementById('formhidden');
                 let xhr2 = new XMLHttpRequest();
                 xhr2.open( 'POST', 'https://api.azu-studio.com/azuapi.php', true );
                 xhr2.send(fd);
                 xhr2.lastobject = respObject[i];
                 xhr2.onreadystatechange = function ( response ) {
                     var respObject2 = JSON.parse(xhr.response);
                     if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    let lastobject = xhr2.lastobject;
                    let sku = lastobject.sku;
                     //var imgArray = JSON.parse(xhr2.response);
                     var source = lastobject.images;
                     var type = "";
                     var color = "";
                     Object.keys(JSON.parse(lastobject.colors)).forEach(function(key) {
                            color = lastobject.colors[key];
                            // ...
                        });
                     
                     var li = document.createElement("LI");
                     li.setAttribute('class', "item");
                     li.setAttribute('id', "li"+sku);
                     var a = document.createElement("A");
                     a.setAttribute('class', "product-image");
                     a.setAttribute('href', "#");
                     var image = document.createElement("IMG");
                     image.alt = "Image";
                     image.src = source;
                     a.appendChild(image);
                     li.appendChild(a);
                     
                     var div = document.createElement("DIV");
                     div.setAttribute('class', "product-details");
                     var a2 = document.createElement("A");
                     a2.setAttribute('class', "remove");
                     a2.setAttribute('href', "javascript:unLike('"+sku+"','"+lastobject.id+"');");
                     var i = document.createElement("I");
                     i.setAttribute('class', "anm anm-times-l");
                     i.setAttribute('aria-hidden', "true");
                     a2.appendChild(i);
                     div.appendChild(a2);
                     var a3 = document.createElement("A");
                     a3.setAttribute('class', "edit-i remove");
                     a3.setAttribute('href', "#");
                     
                     var i2 = document.createElement("I");
                     i2.setAttribute('class', "anm anm-edit");
                     i2.setAttribute('aria-hidden', "true");
                     a3.appendChild(i2);
                     //div.appendChild(a3);
                     var a4 = document.createElement("A");
                     a4.setAttribute('class', "pName");
                     a4.setAttribute('href', "cart.html");
                     a4.innerHTML = lastobject.title;
                     
                     div.appendChild(a4);
                     
                     var div2 = document.createElement("DIV");
                     div2.setAttribute('class', "variant-cart");
                     var color;
                     for (var col in JSON.parse(lastobject.colors)) {
                         color = col
                     }
                     div2.innerHTML = color;
                     
                     div.appendChild(div2);
                     
                     var div3 = document.createElement("DIV");
                     div3.setAttribute('class', "wrapQtyBtn");
                     
                     var div4 = document.createElement("DIV");
                     div4.setAttribute('class', "qtyField");
                     
                     var span = document.createElement("SPAN");
                     span.setAttribute("class", 'label');
                     span.setAttribute('id', "span_q"+"input"+lastobject.id);
                     span.innerHTML = "Cantidad";
                     
                     div4.appendChild(span);
                     var input_product = "input"+lastobject.id;
                     var a5 = document.createElement("A");
                     a5.setAttribute('class', "qtyBtn minus");
                     a5.setAttribute('href', "javascript:substractOne('"+input_product+"');");
                     var i3 = document.createElement("I");
                     i3.setAttribute('class', "fa anm anm-minus-r");
                     i3.setAttribute('aria-hidden', "true");
                     a5.appendChild(i3);
                     div4.appendChild(a5);
                     
                     var input = document.createElement("INPUT");
                     input.setAttribute('type', "text");
                     input.setAttribute('min', "1");
                     input.setAttribute('id', "input"+lastobject.id);
                        if(jQuery.cookie('likes_map') !== undefined){
                        var jar = JSON.parse(Cookies.get('likes_map'));
                       if(!jQuery.isEmptyObject(jar)){
                            for(var inputs in jar){
                            
                                 if("input"+lastobject.id == inputs){
                                input.setAttribute('value', jar[inputs]);
                            
                            }
                            else{
                                input.setAttribute('value', "1"); 
                            }
                         
                           
                           
                       
                        }
                        }
                        else{
                            input.setAttribute('value', "1"); 
                        }
                        
                    }
                    else{
                       input.setAttribute('value', "1"); 
                    }
                     input.setAttribute('name', "quantity");
                     
                     input.setAttribute('class', "product-form__input qty");
                     var a6 = document.createElement("A");
                     a6.setAttribute('class', "qtyBtn plus");
                    
                     a6.setAttribute('href', "javascript:addOne('"+input_product+"');");
                     var i4 = document.createElement("I");
                     i4.setAttribute('class', "fa anm anm-plus-r");
                     i4.setAttribute('aria-hidden', "true");
                     a6.appendChild(i4);
                     div4.appendChild(input);
                     div4.appendChild(a6);
                     div3.appendChild(div4);
                     div.appendChild(div3);
                     
                     var div5 = document.createElement("DIV");
                     div5.setAttribute('class', "priceRow");
                     var div6 = document.createElement("DIV");
                     div6.setAttribute('class', "product-price");
                     var span2 = document.createElement("SPAN");
                     span2.setAttribute('class', "money");
                     span2.setAttribute('id', "span_"+"input"+lastobject.id);
                     var precio = "";
                     
                     if(lastobject.active == "1"){
                         precio = lastobject.price - (lastobject.price*lastobject.discount);
                     }
                     else{
                         precio = lastobject.price;
                     }
                     finalprice = parseFloat(finalprice, 10) + parseFloat(precio, 10);
                     span2.innerHTML = "Q"+precio;
                     div6.appendChild(span2);
                     
                     div5.appendChild(div6);
                     
                     div.appendChild(div5);
                     
                     li.appendChild(div);
                    
                     document.getElementById('cartlist').appendChild(li);
                      if(jQuery.cookie('likes_map')!== undefined){
                        updatePriceLikes("noelement", "noelement");
            
                      }
                     document.getElementById('spanmoney').innerHTML = "Q"+finalprice;
                    }
                     
                     
                 };
             }
                 
             
             //TERMINA LLAMADA A API PARA IMAGEN
             }
         };
         

         
    

         
}

    if(Cookies.get('likes') != undefined){
         var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let monster = JSON.stringify(jar);
        getLikes(monster);
    }
    else{
        
    }

    function updateLikes(){
        document.getElementById("cartlist").innerHTML = "";
         if(Cookies.get('likes') != undefined){
         document.getElementById('cartlist').innerHTML = "";
         var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let monster = JSON.stringify(jar);
        getLikes(monster);
         var ul =document.getElementById("cartlist").childNodes
        if(ul.length==0){
            document.getElementById("spanmoney").innerHTML = "Q00.00";
        }

        }
    }
    function updatePriceLikes(inputid, operation){
        if(inputid !== "noelement" && operation !== "noelement"){
            
       
        var li = document.getElementById('cartlist');
        var costo = document.getElementById("span_"+inputid).innerHTML;
        costo = costo.substr(1, costo.length);
        var total = document.getElementById("spanmoney").innerHTML;
        total = total.substr(1, total.length);
        var qty = document.getElementById(inputid).value;
        if(JSON.parse(Cookies.get('likes')).length > 1){
            if(operation == "+"){
                total = parseFloat(total, 10) + (1* parseFloat(costo, 10));
            }
            else{
                total = parseFloat(total, 10) - (1* parseFloat(costo, 10));
            }
        }
        else{
            total = (parseInt(qty,10)* parseFloat(costo, 10));
        }
           document.getElementById("spanmoney").innerHTML = "Q"+total; 

        }
        else{
            var liked_elements = JSON.parse(jQuery.cookie('likes_map'));
            for(var el in liked_elements){
                    var li = document.getElementById('cartlist');
                    var nodes = li.childNodes[1].childNodes[1].childNodes[4].childNodes[0].childNodes[0].childNodes[0].data;
                    var s = "span_"+String(el);
                    var costo = nodes;
                    costo = costo.substr(1, costo.length);
                    var total = document.getElementById("spanmoney").innerHTML;
                    total = total.substr(1, total.length);
                    var qty = document.getElementById(el).value;
                    if(JSON.parse(Cookies.get('likes')).length > 1){
                            total = parseFloat(total, 10) + (1* parseFloat(costo, 10));
                        
                    }
                    else{
                        total = (parseInt(qty,10)* parseFloat(costo, 10));
                    }
                       document.getElementById("spanmoney").innerHTML = "Q"+total; 
            
                        }
        }
        
    
        

    }
    function addOne(id){
        var input_element = document.getElementById(id);
        if(!isNaN(parseInt(input_element.value, 10)) &&  parseInt(input_element.value, 10) != 0){
            input_element.value = parseInt(input_element.value, 10) +1;
             var jar = JSON.parse(Cookies.get('likes_map'));
             jar[id] = input_element.value;

            jQuery.cookie('likes_map', JSON.stringify(jar), { expires: 30 });
            updatePriceLikes(id, "+");
        }
        else{
            
        }
        
    }
    function substractOne(id){
        var input_element = document.getElementById(id);
        if(!isNaN(parseInt(input_element.value, 10)) &&  parseInt(input_element.value, 10) > 1){
            input_element.value =  parseInt(input_element.value, 10) - 1;
            //guardar cookie que contenga el id del input con value = input_element.value
             var jar = JSON.parse(Cookies.get('likes_map'));
             jar[id] = input_element.value;
            jQuery.cookie('likes_map', JSON.stringify(jar), { expires: 30 });
            updatePriceLikes(id, "-");
        }
        else{
            
        }
        
        
        
    }
    function unLike(sku, id){
        let element = document.getElementById('li'+sku);
        element.parentNode.removeChild(element);
        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let resultado = jar.filter(skus => skus != sku);
        let monster = JSON.stringify(resultado);
        jQuery.cookie('likes', monster, { expires: 30 });
        var id = "input"+id;
        var liked_elements = JSON.parse(jQuery.cookie('likes_map'));
        for(var el in liked_elements){
            if(el == id){
                delete liked_elements[el];
            }
        }
        jQuery.cookie('likes_map', JSON.stringify(liked_elements), { expires: 30 });
        
        updateLikes();
       
        
    }

</script>

<!-- END LIKES -->
</body>

<!-- belle/product-accordion.html   11 Nov 2019 12:44:02 GMT -->
</html>