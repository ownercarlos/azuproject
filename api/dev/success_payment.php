<html class="no-js" lang="en">

<!-- belle/about-us.html   11 Nov 2019 12:44:33 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>About Us &ndash; Belle Multipurpose Bootstrap 4 Template</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="page-template belle">
<div class="pageWrapper">
 <div id="insertheader">
        
    </div>
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
        <!--End Page Title-->
        <div class="map-section map">
        </div>
          <div class="bredcrumbWrap">
            <div class="container breadcrumbs">
              
            </div>
        </div>
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col">
                	<div class="text-center mb-4">
                    </div>
               	</div>
            </div>  
            
            <div class="text-center mb-4">
                        <h2 class="h1" id="payment_title">Su pago ha sido efectuado
                        exitosamente</h2>
            

            <div class="row" align="center">
            	<div class="container" ><img class="blur-up lazyload" data-src="assets/images/success-payment/success_watermark.png" src="assets/images/success-payment/success_watermark.png" alt="About Us" style="opacity:0.5;" /></div>
            
            
            
        </div>
        
    </div>
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <footer id="footer"></footer>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
<script> $("#insertheader").load("header_component.php", function(){
                         console.log("header component loaded succesfully");
                     });
    $("#footer").load("footer_component.php", function(){
                         console.log("footer component loaded succesfully");
                     });
                     
                     
                     
                     if(urlParams.has('name')){
                         const queryString = window.location.search;
                        const urlParams = new URLSearchParams(queryString);
                        var search_bar_input, category, color, size;
                        name = urlParams.get('name');
                        document.getElementById("payment_title").innerHTML = "Su pago ha sido efectuado exitosamente"+name;
            
        }
                     </script>
                     
</body>
<!-- belle/about-us.html   11 Nov 2019 12:44:39 GMT -->
</html>