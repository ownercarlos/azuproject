	<!--Search Form Drawer-->
	<div class="search">
        <div class="search__form">
            <form class="search-bar__form" action="search.php">
                <button class="go-btn search__button" type="submit"><i class="icon anm anm-search-l"></i></button>
                <input class="search__input" type="search" name="input_search" value="" placeholder="Search entire store..." aria-label="Search" autocomplete="off">
            </form>
            <button type="button" class="search-trigger close-btn"><i class="anm anm-times-l"></i></button>
        </div>
    </div>
    <!--End Search Form Drawer-->
<!--Top Header-->
    <div class="top-header">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-10 col-sm-8 col-md-5 col-lg-4">
                    <!-- <div class="currency-picker">
                        <span class="selected-currency">GTQ</span>
                        <ul id="currencies">
                        </ul>
                    </div>
                    <div class="language-dropdown">
                        <span class="language-dd">Español</span>
                        <ul id="language">
                        </ul>
                    </div> -->
                    <p class="phone-no" id="phone"></p>
                </div>
                <div class="col-sm-4 col-md-4 col-lg-4 d-none d-lg-none d-md-block d-lg-block">
                	<div class="text-center"><p class="top-header_middle-text"><div id="mensajedeldia"></div></p></div>
                </div>
                <div class="col-2 col-sm-4 col-md-3 col-lg-4 text-right">
                	<span class="user-menu d-block d-lg-none"><i class="anm anm-user-al" aria-hidden="true"></i></span>
                    <ul class="customer-links list-inline">
                        <li><a href="login.html">Ingresar</a></li>
                        <li><a href="register.html">Crear Cuenta</a></li>
                        <li><a href="wishlist.html">Likes!</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--End Top Header-->
    <!--Header-->
    <div class="header-wrap classicHeader animated d-flex">
    	<div class="container-fluid">        
            <div class="row align-items-center">
            	<!--Desktop Logo-->
                <div class="logo col-md-2 col-lg-2 d-none d-lg-block">
                    <a href="index.php">
                    	<img src="assets/azulogos/Logo_180x92px/Logo_p180x92.png" alt="Logo Azu" title="Azu logo positivo" style="width:30%; height:30%;"/>
                    </a>
                </div>
                <!--End Desktop Logo-->
                <div class="col-2 col-sm-3 col-md-3 col-lg-8">
                	<div class="d-block d-lg-none">
                        <button type="button" class="btn--link site-header__menu js-mobile-nav-toggle mobile-nav--open">
                        	<i class="icon anm anm-times-l"></i>
                            <i class="anm anm-bars-r"></i>
                        </button>
                    </div>
                	<!--Desktop Menu-->
                	<nav class="grid__item" id="AccessibleNav"><!-- for mobile -->
                        <ul id="siteNav" class="site-nav medium center hidearrow">
                            <li class="lvl1 parent megamenu"><a href="https://www.azu-studio.com">Novedades <i class="anm anm-angle-down-l"></i></a>
                                <div class="megamenu style1">
                                    <ul class="grid mmWrapper">
                                        <li class="grid__item large-up--one-whole">
                                            <ul class="grid">
                                                <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Nuevo</a>
                                                    <ul class="subLinks">
                                                      <li class="lvl-2"><a href="search.php?search=Top" class="site-nav lvl-2">Tops <span class="lbl nm_label1">Nuevo</span></a></li>
                                                      <li class="lvl-2"><a href="search.php?search=Vestido" class="site-nav lvl-2">Vestidos <span class="lbl nm_label1">Nuevo</span></a></li>
                                                      <li class="lvl-2"><a href="search.php?search=Falda" class="site-nav lvl-2">Faldas <span class="lbl nm_label1">Nuevo</span></a></li>
                                                    </ul>
                                                  </li>
                                                <!-- <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Home Group 2</a>
                                                    <ul class="subLinks">
                                                        <li class="lvl-2"><a href="home8-jewellery.html" class="site-nav lvl-2">Home 8 - Jewellery</a></li>
                                                        <li class="lvl-2"><a href="home9-parallax.html" class="site-nav lvl-2">Home 9 - Parallax</a></li>
                                                        <li class="lvl-2"><a href="home10-minimal.html" class="site-nav lvl-2">Home 10 - Minimal</a></li>
                                                        <li class="lvl-2"><a href="home11-grid.html" class="site-nav lvl-2">Home 11 - Grid</a></li>
                                                        <li class="lvl-2"><a href="home12-category.html" class="site-nav lvl-2">Home 12 - Category</a></li>
                                                        <li class="lvl-2"><a href="home13-auto-parts.html" class="site-nav lvl-2">Home 13 - Auto Parts</a></li>
                                                        <li class="lvl-2"><a href="home14-bags.html" class="site-nav lvl-2">Home 14 - Bags <span class="lbl nm_label1">New</span></a></li>
                                                    </ul>
                                                </li>
                                                <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">New Sections</a>
                                                    <ul class="subLinks">
                                                        <li class="lvl-2"><a href="home11-grid.html" class="site-nav lvl-2">Image Gallery</a></li>
                                                        <li class="lvl-2"><a href="home5-cosmetic.html" class="site-nav lvl-2">Featured Product</a></li>
                                                        <li class="lvl-2"><a href="home7-shoes.html" class="site-nav lvl-2">Columns with Items</a></li>
                                                        <li class="lvl-2"><a href="home6-modern.html" class="site-nav lvl-2">Text columns with images</a></li>
                                                        <li class="lvl-2"><a href="home2-default.html" class="site-nav lvl-2">Products Carousel</a></li>
                                                        <li class="lvl-2"><a href="home9-parallax.html" class="site-nav lvl-2">Parallax Banner</a></li>
                                                    </ul>
                                                </li>
                                                <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">New Features</a>
                                                    <ul class="subLinks">
                                                        <li class="lvl-2"><a href="home13-auto-parts.html" class="site-nav lvl-2">Top Information Bar <span class="lbl nm_label1">New</span></a></li>
                                                        <li class="lvl-2"><a href="#" class="site-nav lvl-2">Age Varification <span class="lbl nm_label1">New</span></a></li>
                                                        <li class="lvl-2"><a href="#" class="site-nav lvl-2">Footer Blocks</a></li>
                                                        <li class="lvl-2"><a href="#" class="site-nav lvl-2">2 New Megamenu style</a></li>
                                                        <li class="lvl-2"><a href="#" class="site-nav lvl-2">Show Total Savings <span class="lbl nm_label3">Hot</span></a></li>
                                                        <li class="lvl-2"><a href="#" class="site-nav lvl-2">New Custom Icons</a></li>
                                                        <li class="lvl-2"><a href="#" class="site-nav lvl-2">Auto Currency</a></li>
                                                    </ul>
                                                </li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="lvl1 parent megamenu"><a href="#">Comprar <i class="anm anm-angle-down-l"></i></a>
                            	<div class="megamenu style4">
                                    <ul class="grid grid--uniform mmWrapper">
                                    	<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Lo más vendido</a>
                                            <ul class="subLinks">
                                                <li class="lvl-2"><a href="search.php?search=Top" class="site-nav lvl-2">Tops</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Vestidos" class="site-nav lvl-2">Vestidos</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Faldas" class="site-nav lvl-2">Faldas</a></li>
                                            </ul>
                                      	</li>
                                      	<li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Relevantes</a>
                                            <ul class="subLinks">
                                                <li class="lvl-2"><a href="search.php?feature=sol" class="site-nav lvl-2">Sol </a></li>
                                                <li class="lvl-2"><a href="search.php?feature=fiesta" class="site-nav lvl-2">Fiesta</a></li>
                                                <li class="lvl-2"><a href="search.php?feature=noche" class="site-nav lvl-2">De noche</a></li>
                                                <li class="lvl-2"><a href="search.php?feature=amigos" class="site-nav lvl-2">Amigas</a></li>
                                                <li class="lvl-2"><a href="search.php?feature=coctel" class="site-nav lvl-2">Coctel</a></li>
                                                <li class="lvl-2"><a href="search.php?feature=fresco" class="site-nav lvl-2">Fresco </a></li>
                                            </ul>
                                      	</li>
                                        <li class="grid__item lvl-1 col-md-6 col-lg-6">
                                        	<a href="#"><img src="assets/imagesazu/Temporada/azu_capa.jpg" alt="" title="" /></a>
                                        </li>
                                    </ul>
                              	</div>
                            </li>
                        <li class="lvl1 parent megamenu"><a href="#">Tops <i class="anm anm-angle-down-l"></i></a>
                        	<div class="megamenu style2">
                                <ul class="grid mmWrapper">
                                    <li class="grid__item one-whole">
                                        <ul class="grid">
                                            <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Populares</a>
                                                <ul class="subLinks">
                                                    <li class="lvl-2"><a href="search.php?input_search=blanco+elegante&search=Top" class="site-nav lvl-2" >Blanco Elegante</a></li>
                                                    <li class="lvl-2"><a href="search.php?input_search=negro+fiesta&search=Top" class="site-nav lvl-2">Negro de fiesta</a></li>
                                                    <li class="lvl-2"><a href="search.php?input_search=amigas&search=Top" class="site-nav lvl-2">Con tus amigas</a></li>
                                                    <li class="lvl-2"><a href="search.php?input_search=dia+con+tu+pareja&search=Top" class="site-nav lvl-2">Dia con tu pareja</a></li>
                                                    <li class="lvl-2"><a href="search.php?input_search=una+manga&search=Top" class="site-nav lvl-2">Una Manga</a></li>
                                                </ul>
                                            </li>
                                            <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Colores</a>
                                                <ul class="subLinks">
                                                <li class="lvl-2"><a href="search.php?search=Top&color=black" class="site-nav lvl-2">Negro</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Top&color=white" class="site-nav lvl-2">Blanco</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Top&color=blue" class="site-nav lvl-2">Azul</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Top&color=pink" class="site-nav lvl-2">Rosa</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Top&color=red" class="site-nav lvl-2">Rojo</a></li>
                                                <li class="lvl-2"><a href="search.php?search=Top&color=yellow" class="site-nav lvl-2">Amarillo </a></li>
                                                </ul>
                                              </li>
                                            <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Tallas</a>
                                                    <ul class="subLinks">
                                                    <li class="lvl-2"><a href="search.php?search=Top&size=xs" class="site-nav lvl-2">XS</a></li>
                                                    <li class="lvl-2"><a href="search.php?search=Top&size=s" class="site-nav lvl-2">S</a></li>
                                                    <li class="lvl-2"><a href="search.php?search=Top&size=m" class="site-nav lvl-2">M</a></li>
                                                    <li class="lvl-2"><a href="search.php?search=Top&size=l" class="site-nav lvl-2">L</a></li>
                                                    <li class="lvl-2"><a href="search.php?search=Top&size=xl" class="site-nav lvl-2">XL</a></li>
                                                    </ul>
                                                </li>
                                                <li class="grid__item lvl-1 col-md-3 col-lg-3"><a href="#" class="site-nav lvl-1">Ofertas</a>
                                                    <ul class="subLinks">
                                                    <li class="lvl-2"><a href="search.php?search=Top&sku=A123456789" class="site-nav lvl-2">Amarillo Elegante</a></li>
                                                    <li class="lvl-2"><a href="search.php?search=Top&sku=A123456789" class="site-nav lvl-2">De una manga </a></li>
                                                    </ul>
                                                </li>
                                        </ul>
                                    </li>
                                  	<li class="grid__item large-up--one-whole imageCol"><a href="#"><img src="assets/images/megamenu-bg2.jpg" alt=""></a></li>
                                </ul>
                          	</div>
                        </li>
                        <li class="lvl1 parent dropdown"><a href="#">Navegación <i class="anm anm-angle-down-l"></i></a>
                          <ul class="dropdown">
                            <li><a href="#" class="site-nav">Bolsa de Compras <i class="anm anm-angle-right-l"></i></a>
                                <ul class="dropdown">
                                    <li><a href="bag.php" class="site-nav">Ver Bolsa</a></li>
                                    <li><a href="buy.php" class="site-nav">Pagar Ahora</a></li>
                                 </ul>
                            </li>
							<li><a href="buy.php" class="site-nav">Pagar</a></li>
                            <li><a href="who.php" class="site-nav">¿Quienes Somos? </a></li>
                            <li><a href="contact.php"  class="site-nav">Contáctanos</a></li>

                          </ul>
                        </li>
                        <li class="lvl1 parent dropdown"><a href="#">Ofertas <i class="anm anm-angle-down-l"></i></a>
                          <ul class="dropdown">
                            <li class="lvl-2"><a href="#" class="site-nav lvl-2">Extra Pequeño (XS) <span class="lbl nm_label2">Oferta</span></a></li>
                            <li class="lvl-2"><a href="#" class="site-nav lvl-2">Extra Largo (XL) <span class="lbl nm_label2">Oferta</span></a></li>
                            <li class="lvl-2"><a href="#" class="site-nav lvl-2">Cumpleaños <span class="lbl nm_label2">Oferta</span></a></li>
                            <li class="lvl-2"><a href="#" class="site-nav lvl-2">Blazers <span class="lbl nm_label2">Oferta</span> </a></li>
                          </ul>
                        </li>
                      </ul>
                    </nav>
                    <!--End Desktop Menu-->
                </div>
                <!--Mobile Logo-->
                <div class="col-6 col-sm-6 col-md-6 col-lg-2 d-block d-lg-none mobile-logo">
                	<div class="logo">
                        <a href="index.php">
                            <img style="max-width: 30%;" src="assets/azulogos/Logo_180x92px/Logo_p180x92.png" alt="Azulogo" title="Azu logo" />
                        </a>
                    </div>
                </div>
                <!--Mobile Logo-->
                <div class="col-4 col-sm-3 col-md-3 col-lg-2">
                	<div class="site-cart">
                    	<a href="#;" class="site-header__cart" title="Cart">
                        	<i class="icon anm anm-bag-l"></i>
                            <!--<span id="CartCount" class="site-header__cart-count" data-cart-render="item_count">0</span>-->
                        </a>
                        <!--Minicart Popup-->
                        <div id="header-cart" class="block block-cart" >
                        	<ul class="mini-products-list" id="cartlist">
                            
                            </ul>
                            <div class="total">
                            	<div class="total-in">
                                	<span class="label">Subtotal de Bolsa:</span><span class="product-price"><span id="spanmoney" class="money"></span></span>
                                </div>
                                 <div class="buttonSet text-center">
                                    <a href="bag.php" class="btn btn-secondary btn--small">Ver Bolsa</a>
                                    <a href="buy.php" class="btn btn-secondary btn--small">Pagar Ahora</a>
                                </div>
                            </div>
                        </div>
                        <!--EndMinicart Popup-->
                    </div>
                    <div class="site-header__search">
                    	<button type="button" class="search-trigger"><i class="icon anm anm-search-l"></i></button>
                    </div>
                </div>
        	</div>
        </div>
    </div>
    <!--End Header-->
    <!--Mobile Menu-->
     <div class="mobile-nav-wrapper" role="navigation">
		<div class="closemobileMenu"><i class="icon anm anm-times-l pull-right"></i> Cerrar Azú Menú</div>
        <ul id="MobileNav" class="mobile-nav">
        	<li class="lvl1 parent megamenu"><a href="#">Novedades <i class="anm anm-plus-l" ></i></a>
          <ul>
            <li><a href="search.php?search=Top" class="site-nav">Tops</i></a>
            </li>
            <li><a href="search.php?search=Vestido" class="site-nav">Vestidos</i></a>
              <!--<ul>-->
              <!--  <li><a href="home8-jewellery.html" class="site-nav">Home 8 - Jewellery</a></li>-->
              <!--  <li><a href="home9-parallax.html" class="site-nav">Home 9 - Parallax</a></li>-->
              <!--  <li><a href="home10-minimal.html" class="site-nav">Home 10 - Minimal</a></li>-->
              <!--  <li><a href="home11-grid.html" class="site-nav">Home 11 - Grid</a></li>-->
              <!--  <li><a href="home12-category.html" class="site-nav">Home 12 - Category</a></li>-->
              <!--  <li><a href="home13-auto-parts.html" class="site-nav">Home 13 - Auto Parts</a></li>-->
              <!--  <li><a href="home14-bags.html" class="site-nav last">Home 14 - Bags</a></li>-->
              <!--</ul>-->
            </li>
            <li><a href="search.php?search=Falda" class="site-nav">Faldas</i></a>
            </li>
          </ul>
        </li>
        	<li class="lvl1 parent megamenu"><a href="#">Comprar <i class="anm anm-plus-l"></i></a>
          <ul>
            <li><a href="#" class="site-nav">¡Lo más vendido!<i class="anm anm-plus-l"></i></a>
              <ul>
                <li><a href="search.php?search=Top" class="site-nav">Tops</a></li>
                <li><a href="search.php?search=Vestido" class="site-nav">Vestidos</a></li>
                <li><a href="search.php?search=Falda" class="site-nav">Faldas</a></li>
              </ul>
            </li>
            <li><a href="#" class="site-nav">Relevantes<i class="anm anm-plus-l"></i></a>
              <ul>
                <li><a href="search.php?feature=sol" class="site-nav">Sol </a></li>
                <li><a href="search.php?feature=fiesta" class="site-nav">Fiesta</a></li>
                <li><a href="search.php?feature=noche" class="site-nav">De noche</a></li>
                <li><a href="search.php?feature=amigos" class="site-nav">Amigas</a></li>
                <li><a href="search.php?feature=coctel" class="site-nav">Coctel</a></li>
                <li><a href="search.php?feature=fresco" class="site-nav">Fresco </a></li>
              </ul>
            </li>
          </ul>
        </li>
        	<li class="lvl1 parent megamenu"><a href="product-layout-1.html">Tops <i class="anm anm-plus-l"></i></a>
          <ul>
            <li><a href="product-layout-1.html" class="site-nav">Populares<i class="anm anm-plus-l"></i></a>
              <ul>
                <li><a href="search.php?input_search=blanco+elegante&search=Top" class="site-nav">Blanco Elegante</a></li>
                <li><a href="search.php?input_search=negro+fiesta&search=Top" class="site-nav">Negro de fiesta</a></li>
                <li><a href="search.php?input_search=amigas&search=Top" class="site-nav">Con tus amigas</a></li>
                <li><a href="search.php?input_search=dia+con+tu+pareja&search=Top" class="site-nav">Dia con tu pareja</a></li>
                <li><a href="search.php?input_search=una+manga&search=Top" class="site-nav">Una Manga</a></li>
              </ul>
            </li>
            <li><a href="short-description.html" class="site-nav">Colores<i class="anm anm-plus-l"></i></a>
              <ul>
                <li><a href="search.php?search=Top&color=black" class="site-nav">Negro</a></li>
                <li><a href="search.php?search=Top&color=white" class="site-nav">Blanco</a></li>
                <li><a href="search.php?search=Top&color=blue" class="site-nav">Azul</a></li>
                <li><a href="search.php?search=Top&color=pink" class="site-nav">Rosa</a></li>
                <li><a href="search.php?search=Top&color=red" class="site-nav">Rojo</a></li>
                <li><a href="search.php?search=Top&color=yellow" class="site-nav last">Amarillo </a></li>
              </ul>
            </li>
            <li><a href="#" class="site-nav">Tallas<i class="anm anm-plus-l"></i></a>
              <ul>
                <li><a href="search.php?search=Top&size=xs" class="site-nav">XS</a></li>
                <li><a href="search.php?search=Top&size=s" class="site-nav">S</a></li>
                <li><a href="search.php?search=Top&size=m" class="site-nav">M</a></li>
                <li><a href="search.php?search=Top&size=l" class="site-nav">L</a></li>
                <li><a href="search.php?search=Top&size=xl" class="site-nav">XL</a></li>
              </ul>
            </li>
            <li><a href="#" class="site-nav">Ofertas<i class="anm anm-plus-l"></i></a>
              <ul>
                <li><a href="search.php?search=Top&sku=A123456789" class="site-nav">Amarillo Elegante</a></li>
                <li><a href="search.php?search=Top&sku=A123456789" class="site-nav">De una manga </a></li>
              </ul>
            </li>
          </ul>
        </li>
        	<li class="lvl1 parent megamenu"><a href="about-us.html">Bolsa de Compras <i class="anm anm-plus-l"></i></a>
          <ul>
          	<li><a href="bag.php" class="site-nav">Ver Bolsa</i></a>
            </li>
            <li><a href="buy.php" class="site-nav">¡PAGAR AHORA!</i></a>
            </li>
          </ul>
        </li>
        	<li class="lvl1"><a href="buy.php"><b>Comprar Ahora</b></a>
        </li>
      </ul>
	</div>
	<!--End Mobile Menu-->
	<script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
<script>
$.get('https://api.azu-studio.com/header/setHeader.php?id=AF0xFFFF',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
document.getElementById("phone").innerHTML = '<i class="anm anm-phone-s"></i>'+myObj.information[0];
document.getElementById("mensajedeldia").innerHTML = myObj.information[1];

});
</script>