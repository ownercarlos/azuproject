 <div class="newsletter-section">
            <div class="container">
                <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-7 w-100 d-flex justify-content-start align-items-center">
                            <div class="display-table">
                                <div class="display-table-cell footer-newsletter">
                                    <div class="section-header text-center">
                                        <label class="h2"><span>Registrate para </span>lo más nuevo</label>
                                    </div>
                                  
                                        <div class="input-group">
                                            <input type="email" class="input-group__field newsletter__input" name="action" id="action" value="" placeholder="Correo Electrónico" required="">
                                            <span class="input-group__btn">
                                                <button type="button" class="btn newsletter__submit" onclick="handleEmail()" name="commit" id="Subscribe" style=" background-color: #c11718"><span class="newsletter__submit-text--large" >Subscribirse</span></button>
                                            </span>
                                        </div>
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-5 d-flex justify-content-end align-items-center">
                            <div class="footer-social">
                                <ul class="list--inline site-footer__social-icons social-icons">
                                    <!--<li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Facebook"><i class="icon icon-facebook"></i></a></li>-->
                                    <!--<li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Twitter"><i class="icon icon-twitter"></i> <span class="icon__fallback-text">Twitter</span></a></li>-->
                                    <!--<li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Pinterest"><i class="icon icon-pinterest"></i> <span class="icon__fallback-text">Pinterest</span></a></li>-->
                                    <li><a class="social-icons__link" href="https://www.instagram.com/azu.studio/" target="_blank" title="Azu Instagram"><i class="icon icon-instagram"></i> <span class="icon__fallback-text">Instagram</span></a></li>
                                    <!--<li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Tumblr"><i class="icon icon-tumblr-alt"></i> <span class="icon__fallback-text">Tumblr</span></a></li>-->
                                    <!--<li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on YouTube"><i class="icon icon-youtube"></i> <span class="icon__fallback-text">YouTube</span></a></li>-->
                                    <!--<li><a class="social-icons__link" href="#" target="_blank" title="Belle Multipurpose Bootstrap 4 Template on Vimeo"><i class="icon icon-vimeo-alt"></i> <span class="icon__fallback-text">Vimeo</span></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>    
        </div>
        <div class="site-footer" style=" background-color: #c11718">
        	<div class="container">
     			<!--Footer Links-->
            	<div class="footer-top">
                	<div class="row">
                    	<div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                        	<h4 class="h4" id="comprarapida" onclick="expand('ul_comprarapida')">Compra Rápida</h4>
                            <ul id="ul_comprarapida">
                            	<li><a href="search.php?search=Top">Top</a></li>
                                <li><a href="search.php?search=Falda">Falda</a></li>
                                <li><a href="search.php?search=Vestido">Vestidos</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                        	<h4 class="h4"id="info" onclick="expand('ul_info')">Información</h4>
                            <ul id="ul_info" >
                            	<li><a href="who.php">Acerca de nosotros</a></li>
                                <li><a href="#">Privacy policy</a></li>
                                <li><a href="#">Terms &amp; condition</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-3 footer-links">
                        	<h4 class="h4"id="service"onclick="expand('ul_service')">Servicio al Cliente</h4>
                            <ul id="ul_service">
                                <li><a href="#">Preguntas Frecuentes FAQ</a></li>
                                <li><a href="#">Contáctanos</a></li>
                                <li><a href="#">Mis órdenes y retornos</a></li>
                                <li><a href="#">Chat</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-12 col-md-3 col-lg-3 contact-box">
                        	<h4 class="h4">Contact Us</h4>
                            <ul class="addressFooter">
                            	<li><i class="icon anm anm-map-marker-al"></i><p>Santiago Sacatepequez</p></li>
                                <li class="phone"><i class="icon anm anm-phone-s"></i><p>(+502) 59307820</p></li>
                                <li class="email"><i class="icon anm anm-envelope-l"></i><p>support@azu-studio.com</p></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--End Footer Links-->
                <hr>
                <div class="footer-bottom">
                	<div class="row">
                    	<!--Footer Copyright-->
	                	<div class="col-12 col-sm-12 col-md-6 col-lg-6 order-1 order-md-0 order-lg-0 order-sm-1 copyright text-sm-center text-md-left text-lg-left"><span></span> <a href="templateshub.net">Azú Studio</a></div>
                        <!--End Footer Copyright-->
                        <!--Footer Payment Icon-->
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 order-0 order-md-1 order-lg-1 order-sm-0 payment-icons text-right text-md-center">
                        	<ul class="payment-icons list--inline">
                        		<li><i class="icon fa fa-cc-visa" aria-hidden="true"></i></li>
                                <li><i class="icon fa fa-cc-paypal" aria-hidden="true"></i></li>
                            </ul>
                        </div>
                        <!--End Footer Payment Icon-->
                    </div>
                </div>
            </div>
        </div>
        
        <script>
            function expand(id){
                var ul=document.getElementById(id);
                
                if(ul.style.display=="block"){
                    ul.style.display="none"
                }
                else{
                    ul.style.display="block";
                }
                
            }
        </script>