<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/checkout.html   11 Nov 2019 12:44:33 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">       
<title>Checkout &ndash; Azu</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>

<body class="page-template belle">
<div class="pageWrapper">
 <div  id=insertheader ></div>
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
        <!--End Page Title-->
         <div class="map-section map">
        </div>
          <div class="bredcrumbWrap">
            <div class="container breadcrumbs">
              
            </div>
        </div>
        <div class="container">
        	<div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box returning-customer">
                        <h3><i class="icon anm anm-user-al"></i> Formulario de Envío</h3>
                        <div id="customer-login" class="collapse customer-content">
                        </div>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-3">
                    <div class="customer-box customer-coupon">
                        <h3 class="font-15 xs-font-13"><i class="icon anm anm-gift-l"></i> Orden</h3>
                        <div id="have-coupon" class="collapse coupon-checkout-content">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row billing-fields">
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 sm-margin-30px-bottom">
                    <div class="create-ac-content bg-light-gray padding-20px-all">
                        <form>
                            <fieldset>
                                <h2 class="login-title mb-3">Detalles de Envío</h2>
                                <div class="row">
                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-firstname">Nombre y Apellido <span class="required-f">*</span></label>
                                        <input name="firstname" value="" id="input-names" type="text">
                                    </div>
                                        <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-email">Correo Electronico <span class="required-f">*</span></label>
                                        <input name="email" value="" id="input-email" type="email">
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-telephone">Teléfono <span class="required-f">*</span></label>
                                        <input name="telephone" value="" id="input-telephone" type="tel">
                                    </div>
                                      <div class="form-group col-md-6 col-lg-6 col-xl-6 required">
                                        <label for="input-address-1">Dirección de Entrega <span class="required-f">*</span></label>
                                        <textarea class="form-control resize-both" rows="4" name="address" id="address-text"></textarea>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>

                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                    <div class="your-order-payment">
                        <div class="your-order">
                            <h2 class="order-title mb-4">Detalles de Orden</h2>

                            <div class="table-responsive-sm order-table"> 
                                <table class="bg-white table table-bordered table-hover text-center">
                                    <thead id="table-order">

                                        <tr>
                                            <th class="text-left">Producto </th>
                                            <th>Precio</th>
                                            <th>Talla</th>
                                            <th>Cantidad</th>
                                            <th>Color</th>
                                            <th>Subtotal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                                                                <?php
                                            if(isset($_POST["action"])){
                                                
                                               $data = json_decode($_POST["action"],true);
                                                
                                                
                                                $total_price = 0.00;
                                                $html = '';
                                                $aux = 0;
                                                $total = 0.00;
    
                                                foreach($data as $key => $value) {
                                                    
                                                    $html .= '<tr>';

                                                    foreach($value as $json => $json_val){

                                                             $html .= '<td>'.$json_val.'</td>';
                                                             
                                                             if($json == "subtotal"){
                                                                 $total = $total + $json_val;
                                                             }
                                                             
                                            
                                                            
                                                    }
                                                    $aux++;
                                                  $html .= '</tr>';  
                                                }
                                                echo $html;
                                                $sql = "INSERT INTO `orders`(`id`, `sku_list`, `total`, `email`, `address`, `status`, `name`, `phone`, `mac_address`, `created_at`, `updated_at`) VALUES (NULL,'$_POST[action]','$total','empty','empty','0','empty', '0','$_SERVER[REMOTE_ADDR]',NOW(),NOW()) ";
                                                header('Access-Control-Allow-Origin: *');

                                                header('Access-Control-Allow-Methods: GET, POST');
                                                
                                                header("Access-Control-Allow-Headers: X-Requested-With");
                                                    $servername = "localhost";
                                                    $database = "u931312158_dev_shahidaali";
                                                    $username = "u931312158_shahidaali";
                                                    $password = "dev_fiverrShahidaali1";
                                                
                                                
                                                // Create connection
                                                $conn = mysqli_connect($servername, $username, $password, $database);
                                                // Check connection
                                                if (mysqli_query($conn, $sql)) {
                                                $last_id = mysqli_insert_id($conn);
                                                echo "<script> var last_id = '" . $last_id."';</script>";
                                            } else {
                                                echo "Error: " . $sql . "<br>" . mysqli_error($conn);
                                            }
                                            }
                                             ?>
                                    </tbody>
                                    <tfoot class="font-weight-600" id="table-order-foot">
                                        <tr>
                                            <td colspan="4" class="text-right">Total</td>
                                            <td><?php echo $total; ?></td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        
                        <hr />

                        <div class="your-payment">
                            <h2 class="payment-title mb-3">Método de Pago</h2>
                            <div class="payment-method">
                                <div class="payment-accordion">
                                    <div id="accordion" class="payment-section">
                                        <div class="card mb-2">
                                            <div class="card-header">
                                                <a class="card-link" data-toggle="collapse" href="#collapseOne">Transferencia Bancaria</a>
                                            </div>
                                            <div id="collapseOne" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p class="no-margin font-15">Muy Pronto</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card margin-15px-bottom border-radius-none">
                                            <div class="card-header">
                                                <a class="card-link" data-toggle="collapse" href="#collapseThree" aria-expanded="true"> PayPal </a>
                                            </div>
                                            <div id="collapseThree" class="collapse show" data-parent="#accordion">
                                                <div class="card-body">
                                                    <p class="no-margin font-15">Puedes pagar con tarjeta de débito o crédito si no tienes una cuenta PayPal</p>
<?php
if(isset($_POST["action"])){
    
   // var_dump(json_decode($_POST["action"]));
    
    $data = json_decode($_POST["action"], true);
    $total_price = 0.00;
    foreach($data as $key => $value) {
    
        foreach($value as $json => $json_val){
             if($json == "price"){
                 $total_price = $total_price + $json_val;
             }
        }
    
    }
 ?>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <meta http-equiv="X-UA-Compatible" content="IE=edge" /> <!-- Optimal Internet Explorer compatibility -->
</head>

<body>
  <script
    src="https://www.paypal.com/sdk/js?client-id=AXePfAZWsS6mvHeTiKxShpco3l3uOShQh5ApUon9F5_GtbN_2UBU3Gi_aPaYv554ZS4QT7lXrMzFhjwb">
    // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>

  <div id="paypal-button-container"></div>
<script>
  paypal.Buttons({

    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
        if(document.getElementById("input-names").value != "" &&
            document.getElementById("input-email").value != "" &&
            document.getElementById("input-telephone").value != "" &&
            document.getElementById("address-text").value != "" ){
                    var names = document.getElementById("input-names").value;
                    var email = document.getElementById("input-email").value;
                    var phone = document.getElementById("input-telephone").value;
                    var address = document.getElementById("address-text").value;
                    var action = JSON.stringify({"address":address, "status":'1', "email":email, "name":names,"phone":phone, "id":last_id});
                     var fd = new FormData();
                     fd.append("action", action);
                    //document.getElementById('action').value = JSON.stringify(obj);
                    var formData = document.getElementById('formhidden');
                     xhr = new XMLHttpRequest();
                     xhr.open( 'POST', 'https://api.azu-studio.com/orders/update.php', true);
                     xhr.onreadystatechange = function ( response ) {};
                     xhr.send(fd);

                      return actions.order.create({
                        purchase_units: [{
                          amount: {
                            value: '<?php echo $total_price; ?>'
                          }
                        }]
                      });
            }
            else{
                alert("rellene todos los campos*");
                 window.location.href = "bag.php";
            }

    },
    onApprove: function(data, actions) {
                    var action = JSON.stringify({"status":'2',"id":last_id, "paid":"true"});
                     var fd = new FormData();
                     fd.append("action", action);
                    //document.getElementById('action').value = JSON.stringify(obj);
                    var formData = document.getElementById('formhidden');
                     xhr = new XMLHttpRequest();
                     xhr.open( 'POST', 'https://api.azu-studio.com/orders/update.php', true);
                     xhr.onreadystatechange = function ( response ) {};
                     xhr.send(fd);
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
         window.location.href = "success_payment.php?name="+details.payer.name.given_name;
      });
    },
     onCancel: function (data, actions) {
                    var action = JSON.stringify({"status":'5',"id":last_id, "paid":"true"});
                     var fd = new FormData();
                     fd.append("action", action);
                    //document.getElementById('action').value = JSON.stringify(obj);
                    var formData = document.getElementById('formhidden');
                     xhr = new XMLHttpRequest();
                     xhr.open( 'POST', 'https://api.azu-studio.com/orders/update.php', true);
                     xhr.onreadystatechange = function ( response ) {};
                     xhr.send(fd);
    window.location.href = "bag.php";
  },
  }).render('#paypal-button-container');
  //This function displays Smart Payment Buttons on your web page.
</script>

</body>

<?php

}
?>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <footer id="footer">
        
    </footer>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>
<script>
    $("#insertheader").load("header_component.php", function(){
                         console.log("header component loaded succesfully");
                     });
    $("#footer").load("footer_component.php", function(){
                         console.log("footer component loaded succesfully");
                     });
</script>
<script>

</script>
<script src="js/likes.js"></script>
<!-- belle/checkout.html   11 Nov 2019 12:44:33 GMT -->
</html>