<!DOCTYPE html>
<html class="no-js" lang="en">

<!-- belle/wishlist.html   11 Nov 2019 12:22:27 GMT -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Wishlist &ndash; Belle Multipurpose Bootstrap 4 Template</title>
<meta name="description" content="description">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/images/favicon.png" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">
</head>
<body class="page-template belle">
<div class="pageWrapper">
<div id="insertheader" ></div>
    <!--Body Content-->
    <div id="page-content">
    	<!--Page Title-->
        <!--End Page Title-->
        <div class="map-section map">
        </div>
        
          <div class="bredcrumbWrap">
            <div class="container breadcrumbs">
              
            </div>
        </div>
        <div class="container">
        	<div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 main-col" style="margin-top: 100px;">
                	<form action="#">
                        <div class="wishlist-table table-content table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                    	<th class="product-name text-center alt-font">Eliminar</th>
                                        <th class="product-price text-center alt-font">Imagen</th>
                                        <th class="product-name alt-font">Producto</th>
                                        <th class="product-price text-center alt-font">Precio</th>
                                        <th class="stock-status text-center alt-font">Stock</th>
                                        <th class="product-subtotal text-center alt-font">Color</th>
                                         <th class="product-name text-center alt-font">Cantidad</th>
                                    </tr>
                                </thead>
                                <tbody id="table_bolsa">
 
                                </tbody>
                                
                            </table>
                            <br><br>
                            <label>Precio Total: </label>
                            <b><span class="money" id="price">0.00</span></b>
                            <span class="btn" onclick="comprar()">¡Comprar Ahora!</span>
                        </div>
                    </form>                   
               	</div>
            </div>
        </div>
        
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <footer id="footer"></footer>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
</div>
</body>
<script>
$("#insertheader").load("header_component.php", function(){
                         console.log("header component loaded succesfully");
                     });
    $("#footer").load("footer_component.php", function(){
                         console.log("footer component loaded succesfully");
                     });
</script>

<script>
   window.onload = function(){
       getAllProductsByCookie();
   }
   var total_price = Number("0.00");
   var priced = false;
   
   function getAllProductsByCookie(){
       document.getElementById("table_bolsa").innerHTML = "";
       if(jQuery.cookie('likes') !== undefined){
       
        var obj_sku = JSON.parse(jQuery.cookie('likes_map'));
        for(var ids in obj_sku){
            var str_representation = ids.toString();
            str_representation = str_representation.replace("input", "");
            
        
         var action = JSON.stringify({"producto":{"operation":"select","id":str_representation, "sku":"", "precio":"", "descuento":"", "peso":"", "oferta":"", "stock":"","colores":"", "activo":"1", "marca":"", "titulo":"", "descripcion":"", "features":""}});
         
                 var fd = new FormData();
                 fd.append("action", action);
                //document.getElementById('action').value = JSON.stringify(obj);
                var formData = document.getElementById('formhidden');
                 let xhr2 = new XMLHttpRequest();
                 xhr2.open( 'POST', 'https://api.azu-studio.com/azuapi.php', true );
                 xhr2.send(fd);
                 xhr2.onreadystatechange = function ( response ) {
                      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                          var product_object = JSON.parse(this.response);
                          var product_title = product_object[0].title;
                          var price = product_object[0].price;
                          var sku = product_object[0].sku;
                          var stock = product_object[0].stock;
                          var discount = product_object[0].discount;
                          var image = product_object[0].images;
                          image = image.substring(image.indexOf("[")+1, image.indexOf("]")-1);
                          var src = image.replace('"', '');
                          var pos = src.indexOf(",");
                          var input_product = "input_"+product_object[0].id;
                          if(pos<0){
                              
                          }
                          else{
                              src = src.substring(0, src.indexOf(",") -1);
                          }
                          
                          image = "/dev/shahidaali/azu-studio/storage/"+src;
                          var offer = product_object[0].offer=="1" ? true : false;
                          var old_price;
                          if(offer){
                              
                              price = price - (price * discount);
                              old_price = price;
                              price = "Q"+price;
                          }
                          else{
                              old_price = price;
                              price = "Q"+price;
                          }
                          
                          var table_row = document.createElement("TR");
                          table_row.setAttribute("id","td_"+sku);
                            var table_data = document.createElement("TD")
                            table_data.setAttribute("class", "product-remove text-center");
                            table_data.setAttribute("valign", "middle");
                                var a_1 = document.createElement("A");
                                a_1.setAttribute("href","javascript:unLike('"+sku+"','"+product_object[0].id+"');");
                                    var i_1 = document.createElement("I");
                                    i_1.setAttribute("class", "icon icon anm anm-times-l");
                                    
                                a_1.appendChild(i_1);
                            table_data.appendChild(a_1);
                        table_row.appendChild(table_data);
                            
                             var table_data2 = document.createElement("TD")
                            table_data2.setAttribute("class", "product-thumbnail text-center");
                                var a_2 = document.createElement("A");
                                a_2.setAttribute("href","#");
                                    var img = document.createElement("IMG");
                                    img.src = image;
                                    img.setAttribute("alt", "Imagen del producto");
                                    img.setAttribute("title", "Imagen del producto");
                                    
                                a_2.appendChild(img);
                            table_data2.appendChild(a_2);
                        table_row.appendChild(table_data2);
                        
                            var table_data3 = document.createElement("TD");
                            table_data3.setAttribute("class", "product-name");
                                var head4 = document.createElement("H4");
                                head4.setAttribute("class", "no-margin");
                                    var a_3 = document.createElement("A");
                                    a_3.innerHTML = product_title;
                                head4.appendChild(a_3);
                            table_data3.appendChild(head4);
                        table_row.appendChild(table_data3);
                        
                            var table_data4 = document.createElement("TD");
                            table_data4.setAttribute("class","product-price text-center");
                                var span_price = document.createElement("SPAN");
                                span_price.setAttribute("id","span_"+input_product)
                                span_price.setAttribute("class", "amount");
                                span_price.innerHTML = price;
                            table_data4.appendChild(span_price);
                        table_row.appendChild(table_data4);
                        
                            var table_data5 = document.createElement("TD");
                            table_data5.setAttribute("class", "stock text-center");
                                var select = document.createElement("SELECT");
                                    select.setAttribute("name", "SortBy");
                                    select.setAttribute("id", "select_size"+sku);
                                    select.setAttribute("class", "filters-toolbar__input filters-toolbar__input--sort");
                                    select.setAttribute("style","border: 1px solid black");
                                     
                                     if(product_object[0].stock_xs == 0 &&
                                        product_object[0].stock_s == 0 &&
                                        product_object[0].stock_m == 0 &&
                                        product_object[0].stock_l == 0 &&
                                        product_object[0].stock_xl == 0 )
                                     {
                                         select.innerHTML = "¡NO HAY STOCK!";
                                         table_data5.innerHTML ="¡NO HAY STOCK!" ;
                                     }
                                     else if(product_object[0].stock_xs > 0 ||
                                        product_object[0].stock_s > 0 ||
                                        product_object[0].stock_m  > 0 ||
                                        product_object[0].stock_l  > 0 ||
                                        product_object[0].stock_xl  > 0 ){

                                            total_price = total_price + Number(old_price);
                                            total_price = Math.round((total_price + Number.EPSILON) * 100) / 100;
                                            document.getElementById("price").innerHTML = "Q"+total_price.toString();
                                            var option_default = document.createElement("OPTION");
                                                option_default.setAttribute("value", "title-ascending");
                                                option_default.setAttribute("hidden", "true");
                                                option_default.setAttribute("selected", "true");
                                                option_default.innerHTML = "Seleccionar Talla"; 
                                    select.appendChild(option_default);
                                            if(product_object[0].stock_xs > 0){
                                                var option_xs = document.createElement("OPTION");
                                                option_xs.innerHTML = "XS";
                                    select.appendChild(option_xs);
                                            }
                                            if(product_object[0].stock_s > 0){
                                                var option_s = document.createElement("OPTION");
                                                option_s.innerHTML = "S";
                                    select.appendChild(option_s);
                                            }
                                            if(product_object[0].stock_m > 0){
                                                var option_m = document.createElement("OPTION");
                                                option_m.innerHTML = "M";
                                    select.appendChild(option_m);
                                            }
                                            if(product_object[0].stock_l > 0){
                                                var option_l = document.createElement("OPTION");
                                                option_l.innerHTML = "L";
                                    select.appendChild(option_l);
                                            }
                                            if(product_object[0].stock_xl > 0){
                                                var option_xl = document.createElement("OPTION");
                                                option_xl.innerHTML = "XL";
                                    select.appendChild(option_xl);
                                            }
                                           table_data5.appendChild(select);  
                                        }
                            
                        table_row.appendChild(table_data5);
                        
                            var table_data6 = document.createElement("TD");
                            table_data6.setAttribute("class", "product-subtotal text-center");
                            
                               var select_color = document.createElement("SELECT");
                                    select_color.setAttribute("name", "SortBy");
                                    select_color.setAttribute("id", "select_color_"+sku);
                                    select_color.setAttribute("class", "filters-toolbar__input filters-toolbar__input--sort");
                                    select_color.setAttribute("style","border: 1px solid black");
                                    
                                         var option_default_color = document.createElement("OPTION");
                                                option_default_color.setAttribute("value", "title-ascending");
                                                option_default_color.setAttribute("hidden", "true");
                                                option_default_color.setAttribute("selected", "true");
                                                option_default_color.innerHTML = "Seleccionar Color"; 
                                    select_color.appendChild(option_default_color);
                                    
                        
                        var colors = JSON.parse(product_object[0].colors);
                                            for(var col in colors){
                                                var option_col = document.createElement("OPTION");
                                                option_col.innerHTML = col;
                                    select_color.appendChild(option_col);
                                            }
                            table_data6.appendChild(select_color);
                        table_row.appendChild(table_data6);
                        
                             var table_data7 = document.createElement("TD");
                            table_data7.setAttribute("class", "product-subtotal text-center");
                            
                                var div_wrap = document.createElement("DIV");
                                div_wrap.setAttribute("class","wrapQtyBtn");
                                
                                    var div_qty = document.createElement("DIV")
                                    div_qty.setAttribute("class","qtyField");
                                    div_qty.setAttribute("id", "div_qty"+sku);
                                        
                                        
                                        var a_minus = document.createElement("A");
                                        a_minus.setAttribute("class", "qtyBtn minus");
                                        a_minus.setAttribute("href", "javascript:substractOne('"+input_product+"');");
                                        
                                            var i_minus = document.createElement("I");
                                            i_minus.setAttribute("class", "fa anm anm-minus-r");
                                            i_minus.setAttribute("aria-hidden", "fa anm anm-minus-r");
                                        a_minus.appendChild(i_minus);
                                    div_qty.appendChild(a_minus);
                         
                                     var input = document.createElement("INPUT");
                     input.setAttribute('type', "text");
                     input.setAttribute('min', "1");
                     input.setAttribute('id', input_product);
                        if(jQuery.cookie('likes_map') !== undefined){
                        var jar = JSON.parse(Cookies.get('likes_map'));
                       if(!jQuery.isEmptyObject(jar)){
                            for(var inputs in jar){
                            
                                 if("input"+product_object[0].id == inputs){
                                input.setAttribute('value', jar[inputs]);
                            
                            }
                            else{
                                input.setAttribute('value', "1"); 
                            }
                         
                           
                           
                       
                        }
                        }
                        else{
                            input.setAttribute('value', "1"); 
                        }
                        
                    }
                    else{
                       input.setAttribute('value', "1"); 
                    }
                     input.setAttribute('name', "quantity");
                     
                     input.setAttribute('class', "product-form__input qty");
                     
                     div_qty.appendChild(input);
                     
                      var a_minus = document.createElement("A");
                                        a_minus.setAttribute("class", "qtyBtn plus");
                                        a_minus.setAttribute("href", "javascript:addOne('"+input_product+"');");
                                        
                                            var i_minus = document.createElement("I");
                                            i_minus.setAttribute("class", "fa anm anm-plus-r");
                                            i_minus.setAttribute("aria-hidden", "fa anm anm-minus-r");
                                        a_minus.appendChild(i_minus);
                                    div_qty.appendChild(a_minus);
                                div_wrap.appendChild(div_qty);
                                table_data7.appendChild(div_wrap);
                    table_row.appendChild(table_data7);
                document.getElementById("table_bolsa").appendChild(table_row);
                        
                                    
                                
                        
                        
                            
                      }
                 }
        }
       }
       else{
     
       }
       
   }
        
       
   
   
</script>

<script>
function addLikes(sku){
    if(Cookies.get('likes') != undefined){
        //logic for likes
        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
         const found = jar.find(element => element == sku.id);
         if(found != undefined){
             console.log("Process found.");
         }
         else{
            
            jar.push(sku.id);
         }
        let monster = JSON.stringify(jar);
        jQuery.cookie('likes', monster, { expires: 30 });
        
        //logic for mapping

      
        updateLikes();
    }
    else{
        var arr = new Array();
        arr.push(sku.id)
        arr = JSON.stringify(arr);
        jQuery.cookie('likes', arr, { expires: 30 });
        jQuery.cookie('likes_map', JSON.stringify({}), {expires: 30});
        updateLikes();

    }

    
}



function getLikes(arrStringed){
    
    var obj = JSON.parse(arrStringed);
    var iterator = 0;
    var product_sku ="";
    var finalprice ="0";

             var action = JSON.stringify({"producto":{"operation":"select","id":"", "sku":obj, "precio":"", "descuento":"", "peso":"", "oferta":"", "stock":"","colores":"", "activo":"1", "marca":"", "titulo":"", "descripcion":"", "features":""}});

        product_sku = obj;
         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/likes/getLikes.php', true );
         xhr.send(fd);
         xhr.onreadystatechange = function ( response ) {
             if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
             var respObject = JSON.parse(xhr.response);
             //EMPIEZA LLAMADA A API PARA IMAGEN
             for(var i = 0; i < product_sku.length; i++){
             
             
             var action = JSON.stringify({"producto": {"operation": "select", "sku":respObject[i].sku}});
                 var fd = new FormData();
                 fd.append("action", action);
                //document.getElementById('action').value = JSON.stringify(obj);
                var formData = document.getElementById('formhidden');
                 let xhr2 = new XMLHttpRequest();
                 xhr2.open( 'POST', 'https://api.azu-studio.com/azuapi.php', true );
                 xhr2.send(fd);
                 xhr2.lastobject = respObject[i];
                 xhr2.onreadystatechange = function ( response ) {
                     var respObject2 = JSON.parse(xhr.response);
                     if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    let lastobject = xhr2.lastobject;
                    let sku = lastobject.sku;
                     //var imgArray = JSON.parse(xhr2.response);
                     var source = lastobject.images;
                     var type = "";
                     var color = "";
                     Object.keys(JSON.parse(lastobject.colors)).forEach(function(key) {
                            color = lastobject.colors[key];
                            // ...
                        });
                     
                     var li = document.createElement("LI");
                     li.setAttribute('class', "item");
                     li.setAttribute('id', "li"+sku);
                     var a = document.createElement("A");
                     a.setAttribute('class', "product-image");
                     a.setAttribute('href', "#");
                     var image = document.createElement("IMG");
                     image.alt = "Image";
                     image.src = source;
                     a.appendChild(image);
                     li.appendChild(a);
                     
                     var div = document.createElement("DIV");
                     div.setAttribute('class', "product-details");
                     var a2 = document.createElement("A");
                     a2.setAttribute('class', "remove");
                     a2.setAttribute('href', "javascript:unLike('"+sku+"','"+lastobject.id+"');");
                     
                     var i = document.createElement("I");
                     i.setAttribute('class', "anm anm-times-l");
                     i.setAttribute('aria-hidden', "true");
                     a2.appendChild(i);
                     div.appendChild(a2);
                     var a3 = document.createElement("A");
                     a3.setAttribute('class', "edit-i remove");
                     a3.setAttribute('href', "#");
                     
                     var i2 = document.createElement("I");
                     i2.setAttribute('class', "anm anm-edit");
                     i2.setAttribute('aria-hidden', "true");
                     a3.appendChild(i2);
                     //div.appendChild(a3);
                     var a4 = document.createElement("A");
                     a4.setAttribute('class', "pName");
                     a4.setAttribute('href', "cart.html");
                     a4.innerHTML = lastobject.title;
                     
                     div.appendChild(a4);
                     
                     var div2 = document.createElement("DIV");
                     div2.setAttribute('class', "variant-cart");
                     var color;
                     for (var col in JSON.parse(lastobject.colors)) {
                         color = col
                     }
                     div2.innerHTML = color;
                     
                     div.appendChild(div2);
                     
                     var div3 = document.createElement("DIV");
                     div3.setAttribute('class', "wrapQtyBtn");
                     
                     var div4 = document.createElement("DIV");
                     div4.setAttribute('class', "qtyField");
                     
                     var span = document.createElement("SPAN");
                     span.setAttribute("class", 'label');
                     span.setAttribute('id', "span_q"+"input"+lastobject.id);
                     span.innerHTML = "Cantidad";
                     
                     div4.appendChild(span);
                     var input_product = "input"+lastobject.id;
                     var a5 = document.createElement("A");
                     a5.setAttribute('class', "qtyBtn minus");
                     a5.setAttribute('href', "javascript:substractOne('"+input_product+"');");
                     var i3 = document.createElement("I");
                     i3.setAttribute('class', "fa anm anm-minus-r");
                     i3.setAttribute('aria-hidden', "true");
                     a5.appendChild(i3);
                     div4.appendChild(a5);
                     
                     var input = document.createElement("INPUT");
                     input.setAttribute('type', "text");
                     input.setAttribute('min', "1");
                     input.setAttribute('id', "input"+lastobject.id);
                        if(jQuery.cookie('likes_map') !== undefined){
                        var jar = JSON.parse(Cookies.get('likes_map'));
                       if(!jQuery.isEmptyObject(jar)){
                            for(var inputs in jar){
                            
                                 if("input"+lastobject.id == inputs){
                                input.setAttribute('value', jar[inputs]);
                            
                            }
                            else{
                                input.setAttribute('value', "1"); 
                            }
                         
                           
                           
                       
                        }
                        }
                        else{
                            input.setAttribute('value', "1"); 
                        }
                        
                    }
                    else{
                       input.setAttribute('value', "1"); 
                    }
                     input.setAttribute('name', "quantity");
                     
                     input.setAttribute('class', "product-form__input qty");
                     var a6 = document.createElement("A");
                     a6.setAttribute('class', "qtyBtn plus");
                    
                     a6.setAttribute('href', "javascript:addOne('"+input_product+"');");
                     var i4 = document.createElement("I");
                     i4.setAttribute('class', "fa anm anm-plus-r");
                     i4.setAttribute('aria-hidden', "true");
                     a6.appendChild(i4);
                     div4.appendChild(input);
                     div4.appendChild(a6);
                     div3.appendChild(div4);
                     div.appendChild(div3);
                     
                     var div5 = document.createElement("DIV");
                     div5.setAttribute('class', "priceRow");
                     var div6 = document.createElement("DIV");
                     div6.setAttribute('class', "product-price");
                     var span2 = document.createElement("SPAN");
                     span2.setAttribute('class', "money");
                     span2.setAttribute('id', "span_"+"input"+lastobject.id);
                     var precio = "";
                     
                     if(lastobject.offer == "1"){
                         precio = lastobject.price - (lastobject.price*lastobject.discount);
                     }
                     else{
                         precio = lastobject.price;
                     }
                     finalprice = parseFloat(finalprice, 10) + parseFloat(precio, 10);
                     span2.innerHTML = "Q"+precio;
                     div6.appendChild(span2);
                     
                     div5.appendChild(div6);
                     
                     div.appendChild(div5);
                     
                     li.appendChild(div);
                    
                     document.getElementById('cartlist').appendChild(li);
                      if(jQuery.cookie('likes_map')!== undefined){
                        updatePriceLikes("noelement", "noelement");
            
                      }
                     document.getElementById('spanmoney').innerHTML = "Q"+finalprice;
                    }
                     
                     
                 };
             }
                 
             
             //TERMINA LLAMADA A API PARA IMAGEN
             }
         };
         

         
    

         
}
</script>

<script>
    if(Cookies.get('likes') != undefined){
         var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let monster = JSON.stringify(jar);
        getLikes(monster);
    }
    else{
        
    }
</script>

<script>
    function updateLikes(){
        document.getElementById("cartlist").innerHTML = "";
         if(Cookies.get('likes') != undefined){
         document.getElementById('cartlist').innerHTML = "";
         var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let monster = JSON.stringify(jar);
        getLikes(monster);
         var ul =document.getElementById("cartlist").childNodes
        if(ul.length==0){
            document.getElementById("spanmoney").innerHTML = "Q00.00";
        }

        }
    }
    function updatePriceLikes(inputid, operation, optional){
        if(inputid !== "noelement" && operation !== "noelement"){
            
        var li = document.getElementById('cartlist');
        if(optional){
            var costo = document.getElementById("span_"+inputid).innerHTML;
             var total = document.getElementById("price").innerHTML;
        }
        else{
           var costo = document.getElementById("span_"+inputid).value; 
            var total = document.getElementById("spanmoney").innerHTML;
        }
        
        costo = costo.substr(1, costo.length);
       
        total = total.substr(1, total.length);
        var qty = document.getElementById(inputid).value;
        if(JSON.parse(Cookies.get('likes')).length > 1){
            if(operation == "+"){
                total = parseFloat(total, 10) + (1* parseFloat(costo, 10));
            }
            else{
                total = parseFloat(total, 10) - (1* parseFloat(costo, 10));
            }
        }
        else{
            total = (parseInt(qty,10)* parseFloat(costo, 10));
        }
            if(optional){
                         total = Math.round((total + Number.EPSILON) * 100) / 100;
                        document.getElementById("price").innerHTML = "Q"+total; 
                    }
                    else{
                       
                        document.getElementById("spanmoney").innerHTML = "Q"+total; 
                    }

        }
        else{
            var liked_elements = JSON.parse(jQuery.cookie('likes_map'));
            for(var el in liked_elements){
                    var li = document.getElementById('cartlist');
                    var nodes = li.childNodes[1].childNodes[1].childNodes[4].childNodes[0].childNodes[0].childNodes[0].data;
                    var s = "span_"+String(el);
                    var costo = nodes;
                    costo = costo.substr(1, costo.length);
                    var total = document.getElementById("spanmoney").innerHTML;
                    total = total.substr(1, total.length);
                    var qty = document.getElementById(el).value;
                    if(JSON.parse(Cookies.get('likes')).length > 1){
                            total = parseFloat(total, 10) + (1* parseFloat(costo, 10));
                        
                    }
                    else{
                        total = (parseInt(qty,10)* parseFloat(costo, 10));
                    }
                    if(optional){
                         total = Math.round((total + Number.EPSILON) * 100) / 100;
                        document.getElementById("price").innerHTML = "Q"+total; 
                    }
                    else{
                        document.getElementById("spanmoney").innerHTML = "Q"+total; 
                    }
                       
            
                        }
        }
        
    
        

    }
    function addOne(id){
        var input_element = document.getElementById(id);
        if(!isNaN(parseInt(input_element.value, 10)) &&  parseInt(input_element.value, 10) != 0){
            input_element.value = parseInt(input_element.value, 10) +1;
             var jar = JSON.parse(Cookies.get('likes_map'));
             jar[id] = input_element.value;

            jQuery.cookie('likes_map', JSON.stringify(jar), { expires: 30 });
            updatePriceLikes(id, "+", true);
        }
        else{
            
        }
        
    }
    function substractOne(id){
        var input_element = document.getElementById(id);
        if(!isNaN(parseInt(input_element.value, 10)) &&  parseInt(input_element.value, 10) > 1){
            input_element.value =  parseInt(input_element.value, 10) - 1;
            //guardar cookie que contenga el id del input con value = input_element.value
             var jar = JSON.parse(Cookies.get('likes_map'));
             jar[id] = input_element.value;
            jQuery.cookie('likes_map', JSON.stringify(jar), { expires: 30 });
            updatePriceLikes(id, "-", true);
        }
        else{
            
        }
        
        
        
    }
    function unLike(sku, id){
        let element = document.getElementById('li'+sku);
        element.parentNode.removeChild(element);
        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let resultado = jar.filter(skus => skus != sku);
        let monster = JSON.stringify(resultado);
        jQuery.cookie('likes', monster, { expires: 30 });
        var id = "input"+id;
        var liked_elements = JSON.parse(jQuery.cookie('likes_map'));
        for(var el in liked_elements){
            if(el == id){
                delete liked_elements[el];
            }
        }
        jQuery.cookie('likes_map', JSON.stringify(liked_elements), { expires: 30 });
        
        updateLikes();
        getAllProductsByCookie();
        
    }
    
    var submit_now = false;
    function comprar(){


        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        var total_price = document.getElementById("price");
        var sku_list = [];
        var app_object = {};
        var stock = false;
        var passes = false;
        for(var i = 0; i<jar.length;i++){
            sku_list.push(jar[i]);
            var action = JSON.stringify({"producto":{"operation":"select","id":"","sku":jar[i],"precio":"","descuento":"","peso":"","oferta":"","stock":"","colores":"","activo":"160596","marca":"","titulo":"","descripcion":"","features":""}});

         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/azuapi.php', true );
         xhr.send(fd);
         xhr.sku = jar[i];
         xhr.iterator = i;
         xhr.itlength = jar.length;
         var real_iterator = 0;
         xhr.onreadystatechange = function ( response ) {
             if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            var product_info = JSON.parse(this.response);
            var sku_product = product_info[0].sku;
            var element ="div_qty"+sku_product; 
            var divqty = document.getElementById(element);
            input_val = divqty.childNodes[1].value;
            var size = document.getElementById("select_size"+sku_product).value;
            var color = document.getElementById("select_color_"+sku_product).value;
            var precio = 0.00;
             if(product_info[0].offer == "1"){
                         precio = product_info[0].price - (product_info[0].price*product_info[0].discount);
                     }
                     else{
                         precio = product_info[0].price;
                     }
            precio = parseFloat(precio);
            precio =  Math.round((precio + Number.EPSILON) * 100) / 100;
            precio = precio*input_val;
            
            switch(size){
                case "XS":
                    stock = parseInt(input_val, 10) <= parseInt(product_info[0].stock_xs,10) ? true:false;
                    break;
                case "S":
                    stock =  parseInt(input_val, 10) <=  parseInt(product_info[0].stock_s,10) ? true:false;
                    break;
                case "M":
                    stock =  parseInt(input_val, 10) <=  parseInt(product_info[0].stock_m, 10) ? true:false;
                    break;
                case "L":
                    stock =  parseInt(input_val, 10) <=  parseInt(product_info[0].stock_l, 10) ? true:false;
                    break;
                case "XL":
                    stock =  parseInt(input_val, 10) <=  parseInt(product_info[0].stock_xl, 10) ? true:false;
                    break;
            }
            if(size != "title-ascending" && color != "title-ascending" && stock == true){
                submit_now = true;
                passes !=passes;
                 app_object[sku_product] = {
                "title": product_info[0].title, 
                "price": precio/input_val,
                "size": size,
                "qty": input_val,
                "color": color,
                "subtotal": precio
                
                
                
                
            };
            
            }
            else{
                submit_now = false;
                passes = false;
                if(stock==false){
                    alert("El numero que usted ingreso para la talla sobrepasa el stock! Intente nuevamente");
                }
                else{
                    alert("Seleccione una talla/color");
                }

                
            }
              real_iterator++;
            if(real_iterator == xhr.itlength){
               
                if(submit_now){
                    try_submit(app_object);
                }
                
            }
           
           
             }
            

           }

        }
       
        
        
        //let monster = JSON.stringify(resultado);
        //jQuery.cookie('likes', monster, { expires: 30 });
    }
    
    function try_submit(appobj){
        if(submit_now){
        var action = JSON.stringify(appobj);
         var form = document.createElement('form');
        form.style.visibility = 'hidden';
        form.method = 'POST';
        form.action = "buy.php";
        var input = document.createElement('input');
            input.name = "action";
            input.value = action;
            form.appendChild(input)
        document.body.appendChild(form);
        form.submit();
        }
   
    }
    
</script>

</html>