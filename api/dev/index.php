<!DOCTYPE html>
<html class="no-js" lang="en">


<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>Azú</title>
<meta name="description" content="Compra fancy, elegante y sexy en Azú Guatemala">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" href="assets/imagesazu/azuicon.ico" />
<!-- Plugins CSS -->
<link rel="stylesheet" href="assets/css/plugins.css">
<!-- Bootstap CSS -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<!-- Main Style CSS -->
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/css/responsive.css">

</head>
<body class="template-index belle template-index-belle">
<div id="pre-loader">
    <img src="assets/images/loader.gif" alt="Loading..." />
</div>
<div class="pageWrapper">

    <div id="insertheader">
        
    </div>
    
    <!--Body Content-->
    <div id="page-content">
    	<!--Home slider-->
    	<div class="slideshow slideshow-wrapper pb-section sliderFull" >
        	<div class="home-slideshow" id="homeslider">
            	<div class="slide">
                	<div class="blur-up lazyload bg-size" id="image1">
                	    
                        <img class="blur-up lazyload bg-img" data-src="assets/images/slideshow-banners/belle-banner1.jpg" src="assets/imagesazu/Temporada/azu_studio_capabanner.jpeg" alt="Shop Our New Collection" title="Shop Our New Collection" />
                        
                        
                        <div class="slideshow__text-wrap slideshow__overlay classic bottom">
                            <div class="slideshow__text-content bottom">
                                <div class="wrap-caption center">
                                        <h2 class="h1 mega-title slideshow__title" id="titulo1">Nuestra Nueva Colección</h2>
                                        <span class="mega-subtitle slideshow__subtitle" id="descripcion1">De la simpleza nace una reina</span>
                                        <span class="btn">¡Comprar Ahora!</span>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide">
                	<div class="blur-up lazyload bg-size" id="image2">
                	    
                        <img class="blur-up lazyload bg-img" data-src="assets/imagesazu/portrait.jpg" src="assets/imagesazu/portrait.jpg" alt="Summer Bikini Collection" title="Summer Bikini Collection" />
                        <div class="slideshow__text-wrap slideshow__overlay classic bottom">
                            <div class="slideshow__text-content bottom">
                                <div class="wrap-caption center">
                                    <h2 class="h1 mega-title slideshow__title" id="titulo2">Colección de Fin de año</h2>
                                    <span class="mega-subtitle slideshow__subtitle" id="descripcion2">Ahorra hasta el 50% con nuestras ofertas</span>
                                    <span class="btn">¡Comprar Ahora!</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Home slider-->
        <!--Collection Tab slider-->
        <div class="tab-slider-product section">
        	<div class="container">
            	<div class="row">
                	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="section-header text-center">
                            <h2 class="h2">Nuevos Lanzamientos</h2>
                            <p>Busca en nuestra variedad de productos</p>
                        </div>
                        <div class="tabs-listing">
                            <ul class="tabs clearfix">
                                <li class="active" rel="tab1">Tops</li>
                                <li rel="tab2">Faldas</li>
                                <li rel="tab3">Vestidos</li>
                            </ul>
                            <div class="tab_container">
                                <div id="tab1" class="tab_content grid-products">
                                    <div class="productSlider">
                                        <div class="col-12 item" id="newArrival1">
                                           
                                        </div>
                                        
                                               
                                               
                                        <div class="col-12 item" id="newArrival2">

                                        </div>
                                        <div class="col-12 item" id="newArrival3">

                                        </div>
                                        <div class="col-12 item" id="newArrival4">

                                        </div>
                                        <div class="col-12 item">
                                            <!-- start product image -->
                                            <div class="product-image">
                                                <!-- start product image -->
                                                <a href="short-description.html">
                                                    <!-- image -->
                                                    <img class="primary blur-up lazyload" data-src="assets/images/product-images/product-image5.jpg" src="assets/images/product-images/product-image5.jpg" alt="image" title="product" />
                                                    <!-- End image -->
                                                    <!-- Hover image -->
                                                    <img class="hover blur-up lazyload" data-src="assets/images/product-images/product-image5-1.jpg" src="assets/images/product-images/product-image5-1.jpg" alt="image" title="product" />
                                                    <!-- End hover image -->
                                                    <!-- product label -->
                                                    <div class="product-labels"><span class="lbl on-sale">Sale</span></div>
                                                    <!-- End product label -->
                                                </a>
                                                <!-- end product image -->
        
                                                <!-- Start product button -->
                                                <form class="variants add" action="#" onclick="window.location.href='cart.html'"method="post">
                                                    <button class="btn btn-addto-cart" type="button" tabindex="0">Select Options</button>
                                                </form>
                                                <div class="button-set">
                                                    <a href="javascript:void(0)" title="Quick View" class="quick-view-popup quick-view" data-toggle="modal" data-target="#content_quickview">
                                                        <i class="icon anm anm-search-plus-r"></i>
                                                    </a>
                                                    <div class="wishlist-btn">//
                                                        <a class="wishlist add-to-wishlist" href="wishlist.html">
                                                            <i class="icon anm anm-heart-l"></i>
                                                        </a>
                                                    </div>
                                                    <div class="compare-btn">
                                                        <a class="compare add-to-compare" href="compare.html" title="Add to Compare">
                                                            <i class="icon anm anm-random-r"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <!-- end product button -->
                                            </div>
                                            <!-- end product image -->
        
                                            <!--start product details -->
                                            <!-- End product details -->
                                        </div>
                                    </div>
                                </div>
                                <div id="tab2" class="tab_content grid-products">
                                    <div class="productSlider">
                                        <div class="col-12 item" id="newArrivalFalda1">
                                            
                                        </div>
                                        <div class="col-12 item" id="newArrivalFalda2">
                                            
                                        </div>
                                        <div class="col-12 item" id="newArrivalFalda3">
                                            
                                        </div>
                                        <div class="col-12 item" id="newArrivalFalda4">
                                           
                                        </div>
                                    </div>
                                </div>
                                <div id="tab3" class="tab_content grid-products">
                                    <div class="productSlider">
                                        <div class="col-12 item" id="newArrivalVestido1">
                                           
                                        </div>
                                        <div class="col-12 item"  id="newArrivalVestido2">
                                            
                                        </div>
                                        <div class="col-12 item"  id="newArrivalVestido3">
                                            
                                        </div>
                                        <div class="col-12 item"  id="newArrivalVestido4">
                                            
                                        </div>
                                        <div class="col-12 item">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            	</div>    
            </div>
        </div>
        <!--Collection Tab slider-->
        
        <!--Collection Box slider-->
        <div class="collection-box section">
        	<div class="container-fluid">
				<div class="collection-grid" id="collection-grid">
                        <div class="collection-grid-item">
                            <a href="search.php?search=Top" class="collection-grid-item__link">
                                <img id="topCollection"  src="" alt="Tops" class="blur-up lazyload"/>
                                <div class="collection-grid-item__title-wrapper">
                                    <h3 class="collection-grid-item__title btn btn--secondary no-border">Tops</h3>
                                </div>
                            </a>
                        </div>
                        <div class="collection-grid-item">
                            <a href="search.php?search=Vestido" class="collection-grid-item__link">
                                <img id="vestidosCollection" class="blur-up lazyload" src="" alt="Vestidos"/>
                                <div class="collection-grid-item__title-wrapper">
                                    <h3 class="collection-grid-item__title btn btn--secondary no-border">Vestidos</h3>
                                </div>
                            </a>
                        </div>
                        <div class="collection-grid-item blur-up lazyloaded">
                            <a href="#" class="collection-grid-item__link">
                                <img id="blazerCollection" src="" alt="Blazers" class="blur-up lazyload"/>
                                <div class="collection-grid-item__title-wrapper">
                                    <h3 class="collection-grid-item__title btn btn--secondary no-border">Blazers</h3>
                                </div>
                            </a>
                        </div>
                        <div class="collection-grid-item">
                            <a href="search.php?search=Falda" class="collection-grid-item__link">
                                <img id="faldaCollection"  src="" alt="Basic" class="blur-up lazyload"/>
                                <div class="collection-grid-item__title-wrapper">
                                    <h3 class="collection-grid-item__title btn btn--secondary no-border">Faldas</h3>
                                </div>
                            </a>
                        </div>
                        <div class="collection-grid-item">
                            <a href="collection-page.html" class="collection-grid-item__link">
                                <img id="croptopCollection"  src="" alt="Crop Top" class="blur-up lazyload"/>
                                <div class="collection-grid-item__title-wrapper">
                                    <h3 class="collection-grid-item__title btn btn--secondary no-border">Crop Tops</h3>
                                </div>
                            </a>
                        </div>
                        <div class="collection-grid-item">
                            <a href="collection-page.html" class="collection-grid-item__link">
                                <img id="calcetaCollection" src="" alt="Calcetas" class="blur-up lazyload"/>
                                <div class="collection-grid-item__title-wrapper">
                                    <h3 class="collection-grid-item__title btn btn--secondary no-border">Calcetas</h3>
                                </div>
                            </a>
                        </div>
                    </div>
            </div>
        </div>
        <!--End Collection Box slider-->
        
        <!--Logo Slider-->
        <div class="section logo-section">
        	<div class="container">
            	<div class="row">
                	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
                		<div class="logo-bar">
                    <div class="logo-bar__item">
                        <img src="assets/azulogos/Logo_180x92px/Logo_a180x92.png " alt="" title="" />
                    </div>
                    <div class="logo-bar__item">
                        <img src="assets/azulogos/Logo_180x92px/Logo_c180x92.png" alt="" title="" />
                    </div>
                    <div class="logo-bar__item">
                        <img src="assets/azulogos/Logo_180x92px/Logo_d180x92.png" alt="" title="" />
                    </div>
                    <div class="logo-bar__item">
                        <img src="assets/azulogos/Logo_180x92px/Logo_p180x92.png" alt="" title="" />
                    </div>

                </div>
                	</div>
                </div>
            </div>
        </div>
        <!--End Logo Slider-->
        
        <!--Featured Product-->
        <div class="product-rows section">
        	<div class="container">
            	<div class="row">
                	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
        				<div class="section-header text-center">
                            <h2 class="h2">Prendas Destacadas</h2>
                            <p>Nuestros productos más populares</p>
                        </div>
            		</div>
                </div>
                <div class="grid-products">
	                <div class="row">
                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2" id="popular1">
                        </div>
                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2" id="popular2">
                        </div>
                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2" id="popular3">
                        </div>
                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2" id="popular4">
                        </div>
                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2" id="popular5">
                        </div>
                        <div class="col-6 col-sm-6 col-md-4 col-lg-4 item grid-view-item style2" id="popular6">
                        	
                        </div>
                	</div>
                </div>
           </div>
        </div>	
        <!--End Featured Product-->

        
        <!--Store Feature-->
        <div class="store-feature section">
        	<div class="container">
            	<div class="row">
                	<div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    	<ul class="display-table store-info">
                        	<li class="display-table-cell">
                            	<i class="icon anm anm-truck-l"></i>
                            	<h5>Envío gratis y Devoluciones</h5>
                            	<span class="sub-text">Envíos gratis en toda Guatemala</span>
                            </li>
                          	<li class="display-table-cell">
                            	<i class="icon anm anm-dollar-sign-r"></i>
                                <h5>Garantía</h5>
                                <span class="sub-text">30 días de devolución de tu dinero</span>
                          	</li>
                          	<li class="display-table-cell">
                            	<i class="icon anm anm-comments-l"></i>
                                <h5>Soporte 24/7</h5>
                                <span class="sub-text">Puedes llamarnos o escribirnos a cualquier hora</span>
                            </li>
                          	<li class="display-table-cell">
                            	<i class="icon anm anm-credit-card-front-r"></i>
                                <h5>Pagos Seguros</h5>
                                <span class="sub-text">Todos los pagos son seguros, no almacenamos información de pago.</span>
                        	</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--End Store Feature-->
    </div>
    <!--End Body Content-->
    
    <!--Footer-->
    <footer id="footer">
       
    </footer>
    <!--End Footer-->
    <!--Scoll Top-->
    <span id="site-scroll" style=" background-color: #c11718"><i class="icon anm anm-angle-up-r"></i></span>
    <!--End Scoll Top-->
    
    <!--Quick View popup-->
    <div class="modal fade quick-view-popup" id="content_quickview">
    	<div class="modal-dialog">
        	<div class="modal-content">
            	<div class="modal-body">
                    <div id="ProductSection-product-template" class="product-template__container prstyle1">
                <div class="product-single">
                <!-- Start model close -->
                <a href="javascript:void()" data-dismiss="modal" class="model-close-btn pull-right" title="close"><span class="icon icon anm anm-times-l"></span></a>
                <!-- End model close -->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="product-details-img">
                            <div class="pl-20" id="modal_div_imagen">
                                <img src="assets/images/product-detail-page/camelia-reversible-big1.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="product-single__meta">
                                <h2 id="modal_titulo" class="product-single__title">Product Quick View Popup</h2>
                                <div class="prInfoRow">
                                    <div class="product-stock"> <span class="instock " id="modal_div_stock">In Stock</span> <span class="outstock hide">Unavailable</span> </div>
                                    <div class="product-sku">SKU: <span class="variant-sku" id="modal_span_sku">19115-rdxs</span></div>
                                </div>
                                <p class="product-single__price product-single__price-product-template">
                                    <span class="visually-hidden" >Precio Regular</span>
                                    <s id="ComparePrice-product-template"><span class="money" id="modal_span_oldprice" >$600.00</span></s>
                                    <span class="product-price__price product-price__price-product-template product-price__sale product-price__sale--single">
                                        <span id="ProductPrice-product-template"><span class="money" id="modal_span_price">$500.00</span></span>
                                    </span>
                                </p>
                                <div class="product-single__description rte" id="modal_div_descripcion">
                                    Belle Multipurpose Bootstrap 4 Html Template that will give you and your customers a smooth shopping experience which can be used for various kinds of stores such as fashion,...
                                </div>
                                
                            <form method="post" action="http://annimexweb.com/cart/add" id="product_form_10508262282" accept-charset="UTF-8" class="product-form product-form-product-template hidedropdown" enctype="multipart/form-data">
                                <div class="swatch clearfix swatch-0 option1" data-option-index="0">
                                    <div class="product-form__item">
                                      <label class="header">Color: <span class="slVariant" id="modal_span_color">Red</span></label>
                                      <!--<div data-value="Red" class="swatch-element color red available">-->
                                      <!--  <input class="swatchInput" id="swatch-0-red" type="radio" name="option-0" value="Red">-->
                                      <!--  <label class="swatchLbl color medium rectangle" for="swatch-0-red" style="background-image:url(assets/images/product-detail-page/variant1-1.jpg);" title="Red"></label>-->
                                      <!--</div>-->
                                      <!--<div data-value="Blue" class="swatch-element color blue available">-->
                                      <!--  <input class="swatchInput" id="swatch-0-blue" type="radio" name="option-0" value="Blue">-->
                                      <!--  <label class="swatchLbl color medium rectangle" for="swatch-0-blue" style="background-image:url(assets/images/product-detail-page/variant1-2.jpg);" title="Blue"></label>-->
                                      <!--</div>-->
                                      <!--<div data-value="Green" class="swatch-element color green available">-->
                                      <!--  <input class="swatchInput" id="swatch-0-green" type="radio" name="option-0" value="Green">-->
                                      <!--  <label class="swatchLbl color medium rectangle" for="swatch-0-green" style="background-image:url(assets/images/product-detail-page/variant1-3.jpg);" title="Green"></label>-->
                                      <!--</div>-->
                                      <!--<div data-value="Gray" class="swatch-element color gray available">-->
                                      <!--  <input class="swatchInput" id="swatch-0-gray" type="radio" name="option-0" value="Gray">-->
                                      <!--  <label class="swatchLbl color medium rectangle" for="swatch-0-gray" style="background-image:url(assets/images/product-detail-page/variant1-4.jpg);" title="Gray"></label>-->
                                      <!--</div>-->
                                    </div>
                                </div>
                                <div class="swatch clearfix swatch-1 option2" data-option-index="1">
                                    <div class="product-form__item">
                                      <label class="header">Size: <span class="slVariant">XS</span></label>
                                      <div data-value="XS" class="swatch-element xs available">
                                        <input class="swatchInput" id="swatch-1-xs" type="radio" name="option-1" value="XS">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-xs" title="XS">XS</label>
                                      </div>
                                      <div data-value="S" class="swatch-element s available">
                                        <input class="swatchInput" id="swatch-1-s" type="radio" name="option-1" value="S">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-s" title="S">S</label>
                                      </div>
                                      <div data-value="M" class="swatch-element m available">
                                        <input class="swatchInput" id="swatch-1-m" type="radio" name="option-1" value="M">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-m" title="M">M</label>
                                      </div>
                                      <div data-value="L" class="swatch-element l available">
                                        <input class="swatchInput" id="swatch-1-l" type="radio" name="option-1" value="L">
                                        <label class="swatchLbl medium rectangle" for="swatch-1-l" title="L">L</label>
                                      </div>
                                    </div>
                                </div>
                                <!-- Product Action -->
                                <div class="product-action clearfix">
                                    <div class="product-form__item--quantity">
                                        <div class="wrapQtyBtn">
                                            <div class="qtyField">
                                                <a class="qtyBtn minus" href="javascript:void(0);"><i class="fa anm anm-minus-r" aria-hidden="true"></i></a>
                                                <input type="text" id="Quantity" name="quantity" value="1" class="product-form__input qty">
                                                <a class="qtyBtn plus" href="javascript:void(0);"><i class="fa anm anm-plus-r" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>                                
                                    <div class="product-form__item--submit">
                                        <a id ="modal_a_comprar">
                                        <button id="modal_btn_cart" type="button" name="add" class="btn product-form__cart-submit">
                                            <span>Comprar</span>
                                        </button>
                                        </a>
                                    </div>
                                </div>
                                <!-- End Product Action -->
                            </form>
                            <div class="display-table shareRow">
                                    <div class="display-table-cell">
                                        <div class="wishlist-btn">
                                            <a id="modal_a_like" class="wishlist add-to-wishlist" href="" title="Add to Wishlist"><i class="icon anm anm-heart-l" aria-hidden="true"></i> <span>Like</span></a>
                                        </div>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
                <!--End-product-single-->
                </div>
            </div>
        		</div>
        	</div>
        </div>
    </div>
    <!--End Quick View popup-->
    
    <!-- Newsletter Popup -->
	<div class="newsletter-wrap" id="popup-container">
      <div id="popup-window">
        <a class="btn closepopup"><i class="icon icon anm anm-times-l"></i></a>
        <!-- Modal content-->
        <div class="display-table splash-bg">
          <div class="display-table-cell width40"><img src="assets/images/newsletter-img.jpg" alt="Join Our Mailing List" title="Join Our Mailing List" /> </div>
          <div class="display-table-cell width60 text-center">
            <div class="newsletter-left">
              <h2>Únete a la lista de correos</h2>
              <p>Registra tu correo para ofertas exclusivas a clientes registrados.</p>

                <div class="input-group">
                  <input type="email" class="input-group__field newsletter__input" id="modal_email" name="EMAIL" value="" placeholder="Email address" required="">
                      <span class="input-group__btn">
                      	<button type="button" onclick="handleEmail('true')" class="btn newsletter__submit" name="commit" id="subscribeBtn"> <span class="newsletter__submit-text--large">Subscribe</span> </button>
                      </span>
                  </div>

              <ul class="list--inline site-footer__social-icons social-icons">
                <li><a class="social-icons__link" href="#" title="Facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                <li><a class="social-icons__link" href="#" title="Twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <li><a class="social-icons__link" href="#" title="Pinterest"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                <li><a class="social-icons__link" href="#" title="Instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                <li><a class="social-icons__link" href="#" title="YouTube"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                <li><a class="social-icons__link" href="#" title="Vimeo"><i class="fa fa-vimeo" aria-hidden="true"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
	<!-- End Newsletter Popup -->
    
     <!-- Including Jquery -->
     <script src="assets/js/vendor/jquery-3.3.1.min.js"></script>
     <script src="assets/js/vendor/modernizr-3.6.0.min.js"></script>
     <script src="assets/js/vendor/jquery.cookie.js"></script>
     <script src="assets/js/vendor/wow.min.js"></script>
     <!-- Including Javascript -->
     <script src="assets/js/bootstrap.min.js"></script>
     <script src="assets/js/plugins.js"></script>
     <script src="assets/js/popper.min.js"></script>
     <script src="assets/js/lazysizes.js"></script>
     <script src="assets/js/main.js"></script>
     <!--For Newsletter Popup-->
     <script>
		jQuery(document).ready(function(){  
		  jQuery('.closepopup').on('click', function () {
			  jQuery('#popup-container').fadeOut();
			  jQuery('#modalOverly').fadeOut();
		  });
		  
		  var visits = jQuery.cookie('visits') || 0;
		  visits++;
		  jQuery.cookie('visits', visits, { expires: 1, path: '/' });
		  console.debug(jQuery.cookie('visits')); 
		  if ( jQuery.cookie('visits') > 1 ) {
			jQuery('#modalOverly').hide();
			jQuery('#popup-container').hide();
        } else {
            var pageHeight = jQuery(document).height();
            jQuery('<div id="modalOverly"></div>').insertBefore('body');
            jQuery('#modalOverly').css("height", pageHeight);
            jQuery('#popup-container').show();
        }
        if (jQuery.cookie('noShowWelcome')) { jQuery('#popup-container').hide(); jQuery('#active-popup').hide(); }


    }); 
    
    jQuery(document).mouseup(function(e){
        var container = jQuery('#popup-container');
        if( !container.is(e.target)&& container.has(e.target).length === 0)
        {
        container.fadeOut();
        jQuery('#modalOverly').fadeIn(200);
        jQuery('#modalOverly').hide();
        }
    });
    $("#insertheader").load("header_component.php", function(){
                         console.log("header component loaded succesfully");
                     });
    $("#footer").load("footer_component.php", function(){
                         console.log("footer component loaded succesfully");
                     });
</script>
<!--End For Newsletter Popup-->
<!-- Ultimos lanzamientos -->
<script>
// $.get('https://api.azu-studio.com/showLanzamientos.php',  // url
// function (data, textStatus, jqXHR) {  // success callback
// //document.getElementById("newArrival1").innerHTML = data;
// var myObj = JSON.parse(data);
// //console.log(myObj);
// document.getElementById("newArrival1").innerHTML = myObj.information[0];
// document.getElementById("newArrival2").innerHTML = myObj.information[1];
// document.getElementById("newArrival3").innerHTML = myObj.information[2];
// document.getElementById("newArrival4").innerHTML = myObj.information[3];
// });
</script>
<!-- Fin ultimos lanza -->
<!-- Start script for arrivales -->
<script>

//Get tops
$.get('https://api.azu-studio.com/showArrivals.php?get=Top',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);

document.getElementById("newArrival1").innerHTML = myObj.information[0];
document.getElementById("newArrival2").innerHTML = myObj.information[1];
document.getElementById("newArrival3").innerHTML = myObj.information[2];
document.getElementById("newArrival4").innerHTML = myObj.information[3];
});

//Get Faldas
$.get('https://api.azu-studio.com/showArrivals.php?get=Falda',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);

document.getElementById("newArrivalFalda1").innerHTML = myObj.information[0];
document.getElementById("newArrivalFalda2").innerHTML = myObj.information[1];
document.getElementById("newArrivalFalda3").innerHTML = myObj.information[2];
document.getElementById("newArrivalFalda4").innerHTML = myObj.information[3];
});


$.get('https://api.azu-studio.com/showArrivals.php?get=Vestido',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);

document.getElementById("newArrivalVestido1").innerHTML = myObj.information[0];
document.getElementById("newArrivalVestido2").innerHTML = myObj.information[1];
document.getElementById("newArrivalVestido3").innerHTML = myObj.information[2];
document.getElementById("newArrivalVestido4").innerHTML = myObj.information[3];
});

</script>

<!-- end script for arrvales -->

<!-- Scriot for Portadas -->
<script>
$.get('https://api.azu-studio.com/getPortadas.php?get=Top',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
document.getElementById("topCollection").src=myObj.information[0];

});
$.get('https://api.azu-studio.com/getPortadas.php?get=Vestido',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
document.getElementById("vestidosCollection").src=myObj.information[0];
});
$.get('https://api.azu-studio.com/getPortadas.php?get=Falda',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
document.getElementById("faldaCollection").src=myObj.information[0];
});

$.get('https://api.azu-studio.com/getPortadas.php?get=cropTop',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
if(myObj.information.length >0){
    document.getElementById("croptopCollection").src=myObj.information[0];
}
else{
    document.getElementById("croptopCollection").parentNode.parentNode.remove();
}

});

$.get('https://api.azu-studio.com/getPortadas.php?get=Calcetas',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
if(myObj.information.length >0){
document.getElementById("calcetaCollection").src=myObj.information[0];
}
else{
    document.getElementById("calcetaCollection").parentNode.parentNode.remove();
}
});
$.get('https://api.azu-studio.com/getPortadas.php?get=Blazer',  // url
function (data, textStatus, jqXHR) {  // success callback

var myObj = JSON.parse(data);
if(myObj.information.length >0){
document.getElementById("blazerCollection").src=myObj.information[0];
}
else{
    document.getElementById("blazerCollection").parentNode.parentNode.remove();
}
});
</script>
<!-- Termina script para portadas -->
<!-- Empieza el script para manejar los productos en un click -->
<div>
    <form id="formhidden" type="hidden">
        <input type="hidden" id="action" name="action" value="">
    </form>
</div>
<script>
    function handleProduct(sku, color){
        var timestamp, status;
        var obj = document.getElementById(sku);
        timestamp = new Date().toISOString();
        var url = window.location.pathname;
        var filename = url.substring(url.lastIndexOf('/')+1);
        status = filename;
        var action = JSON.stringify({"visitas":{"operation":"insert", "color":color, "sku":sku, "timestamp":timestamp, "status":status, "email":""}});
                
         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/azuapi.php', true);
         xhr.onreadystatechange = function ( response ) {};
         xhr.send(fd);
        
    }
</script>
<!-- Termina manejo de productos en click -->
<!-- frontend para productos populares -->
<script>
    $.get('https://api.azu-studio.com/showFeatures.php',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("newArrival1").innerHTML = data;
var myObj = JSON.parse(data);
document.getElementById("popular1").innerHTML = myObj.information[0];
document.getElementById("popular2").innerHTML = myObj.information[1];
document.getElementById("popular3").innerHTML = myObj.information[2];
document.getElementById("popular4").innerHTML = myObj.information[3];
document.getElementById("popular5").innerHTML = myObj.information[4];
document.getElementById("popular6").innerHTML = myObj.information[5];
});
</script>
<!-- termina productos populares -->
<!-- empieza administrador de correos -->
<script>
    function handleEmail(opt){
        
        if(opt){
            var correo = document.getElementById("modal_email").value; 
            
        }
        else{
           var correo = document.getElementById("action").value; 
        }
        
        var timestamp, status;
        timestamp = new Date().toISOString();
        var url = window.location.pathname;
        var filename = url.substring(url.lastIndexOf('/')+1);
        status = filename;
        var action = JSON.stringify({"visitas":{"operation":"insert", "color":"", "sku":"", "timestamp":timestamp, "status":status, "email":correo}});
           
           
           //INSERT     
         var fd = new FormData();
         fd.append("action", action);
         console.log(fd);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/azuapi.php' , true);
         xhr.onreadystatechange = function ( response ) {};
         xhr.send(fd);
         
         //SEND EMAIL
         var fd = new FormData();
         fd.append("action", correo);
         console.log(fd);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/emailtest.php', true );
         xhr.onreadystatechange = function ( response ) {};
         xhr.send(fd);
         if(opt){
            jQuery('#modalOverly').hide();
			jQuery('#popup-container').hide();
			alert("correo enviado exitosamente");
         }
         else{
             alert("correo enviado exitosamente");
         }
          
        
    }
</script>

<!-- termina administrador de correos -->
<!-- manejo de banner -->
<script>

    
$.get('https://api.azu-studio.com/banner/getBanner.php?get=0xffsadfe',  // url
function (data, textStatus, jqXHR) {  // success callback
//document.getElementById("homeslider").innerHTML = data;
var myobj = JSON.parse(data);

//banner1 post processor


const orientation = window.screen.orientation
const angle = orientation.angle
const type = orientation.type
    var image1 = new Image();
    var image2 = new Image();


//image1.setAttribute('class', "blur-up lazyload bg-img");
image1.setAttribute('alt', 'pleasework');
image1.setAttribute('title', 'pl5easework');
image1.setAttribute('object-fit', 'contain');
image1.src = myobj.information[0];


image2.setAttribute('class', "blur-up lazyload bg-img");
image2.setAttribute('alt', 'pleasework');
image2.setAttribute('title', 'pleasework');

image2.src = myobj.information[3];
image2.setAttribute('object-fit', 'contain');
//document.getElementById('image1').prepend(image1);
//document.getElementById('image2').prepend(image2);

//document.getElementById('bgBanner1').innerHTML = "";
document.getElementById("titulo1").innerHTML = myobj.information[1];
document.getElementById("descripcion1").innerHTML = myobj.information[2];
// document.getElementById('bgBanner1').src = myobj.information[0] ;
// document.getElementById('bgBanner1').setAttribute('data-src',myobj.information[0] );
// document.getElementById('bgBanner2').src = myobj.information[3] ;
// document.getElementById('bgBanner2').setAttribute('data-src',myobj.information[3] );
document.getElementById("titulo2").innerHTML = myobj.information[4];
document.getElementById("descripcion2").innerHTML = myobj.information[5];




});
function myFunction(){
    alert("stop");
}

  // you can refresh as many images you want just repeat above steps

</script>
<!-- termina manejo de banner -->
<!-- administracion de header -->
<script>


</script>
<!-- termina header -->
<!-- likes means cookies -->
<script>
function addLikes(sku){
    if(Cookies.get('likes') != undefined){
        //logic for likes
        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
         const found = jar.find(element => element == sku.id);
         if(found != undefined){
             console.log("Process found.");
         }
         else{
            
            jar.push(sku.id);
         }
        let monster = JSON.stringify(jar);
        jQuery.cookie('likes', monster, { expires: 30 });
        
        //logic for mapping

      
        updateLikes();
    }
    else{
        var arr = new Array();
        arr.push(sku.id)
        arr = JSON.stringify(arr);
        jQuery.cookie('likes', arr, { expires: 30 });
        jQuery.cookie('likes_map', JSON.stringify({}), {expires: 30});
        updateLikes();

    }

    
}



function getLikes(arrStringed){
    
    var obj = JSON.parse(arrStringed);
    var iterator = 0;
    var product_sku ="";
    var finalprice ="0";

             var action = JSON.stringify({"producto":{"operation":"select","id":"", "sku":obj, "precio":"", "descuento":"", "peso":"", "oferta":"", "stock":"","colores":"", "activo":"1", "marca":"", "titulo":"", "descripcion":"", "features":""}});

        product_sku = obj;
         var fd = new FormData();
         fd.append("action", action);
        //document.getElementById('action').value = JSON.stringify(obj);
        var formData = document.getElementById('formhidden');
         xhr = new XMLHttpRequest();
         xhr.open( 'POST', 'https://api.azu-studio.com/likes/getLikes.php', true );
         xhr.send(fd);
         xhr.onreadystatechange = function ( response ) {
             if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
             var respObject = JSON.parse(xhr.response);
             //EMPIEZA LLAMADA A API PARA IMAGEN
             for(var i = 0; i < product_sku.length; i++){
             
             
             var action = JSON.stringify({"producto": {"operation": "select", "sku":respObject[i].sku}});
                 var fd = new FormData();
                 fd.append("action", action);
                //document.getElementById('action').value = JSON.stringify(obj);
                var formData = document.getElementById('formhidden');
                 let xhr2 = new XMLHttpRequest();
                 xhr2.open( 'POST', 'https://api.azu-studio.com/azuapi.php', true );
                 xhr2.send(fd);
                 xhr2.lastobject = respObject[i];
                 xhr2.onreadystatechange = function ( response ) {
                     var respObject2 = JSON.parse(xhr.response);
                     if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
                    let lastobject = xhr2.lastobject;
                    let sku = lastobject.sku;
                     //var imgArray = JSON.parse(xhr2.response);
                     var source = lastobject.images;
                     var type = "";
                     var color = "";
                     Object.keys(JSON.parse(lastobject.colors)).forEach(function(key) {
                            color = lastobject.colors[key];
                            // ...
                        });
                     
                     var li = document.createElement("LI");
                     li.setAttribute('class', "item");
                     li.setAttribute('id', "li"+sku);
                     var a = document.createElement("A");
                     a.setAttribute('class', "product-image");
                     a.setAttribute('href', "#");
                     var image = document.createElement("IMG");
                     image.alt = "Image";
                     image.src = source;
                     a.appendChild(image);
                     li.appendChild(a);
                     
                     var div = document.createElement("DIV");
                     div.setAttribute('class', "product-details");
                     var a2 = document.createElement("A");
                     a2.setAttribute('class', "remove");
                     a2.setAttribute('href', "javascript:unLike('"+sku+"','"+lastobject.id+"');");
                     
                     var i = document.createElement("I");
                     i.setAttribute('class', "anm anm-times-l");
                     i.setAttribute('aria-hidden', "true");
                     a2.appendChild(i);
                     div.appendChild(a2);
                     var a3 = document.createElement("A");
                     a3.setAttribute('class', "edit-i remove");
                     a3.setAttribute('href', "#");
                     
                     var i2 = document.createElement("I");
                     i2.setAttribute('class', "anm anm-edit");
                     i2.setAttribute('aria-hidden', "true");
                     a3.appendChild(i2);
                     //div.appendChild(a3);
                     var a4 = document.createElement("A");
                     a4.setAttribute('class', "pName");
                     a4.setAttribute('href', "cart.html");
                     a4.innerHTML = lastobject.title;
                     
                     div.appendChild(a4);
                     
                     var div2 = document.createElement("DIV");
                     div2.setAttribute('class', "variant-cart");
                     var color;
                     for (var col in JSON.parse(lastobject.colors)) {
                         color = col
                     }
                     div2.innerHTML = color;
                     
                     div.appendChild(div2);
                     
                     var div3 = document.createElement("DIV");
                     div3.setAttribute('class', "wrapQtyBtn");
                     
                     var div4 = document.createElement("DIV");
                     div4.setAttribute('class', "qtyField");
                     
                     var span = document.createElement("SPAN");
                     span.setAttribute("class", 'label');
                     span.setAttribute('id', "span_q"+"input"+lastobject.id);
                     span.innerHTML = "Cantidad";
                     
                     div4.appendChild(span);
                     var input_product = "input"+lastobject.id;
                     var a5 = document.createElement("A");
                     a5.setAttribute('class', "qtyBtn minus");
                     a5.setAttribute('href', "javascript:substractOne('"+input_product+"');");
                     var i3 = document.createElement("I");
                     i3.setAttribute('class', "fa anm anm-minus-r");
                     i3.setAttribute('aria-hidden', "true");
                     a5.appendChild(i3);
                     div4.appendChild(a5);
                     
                     var input = document.createElement("INPUT");
                     input.setAttribute('type', "text");
                     input.setAttribute('min', "1");
                     input.setAttribute('id', "input"+lastobject.id);
                        if(jQuery.cookie('likes_map') !== undefined){
                        var jar = JSON.parse(Cookies.get('likes_map'));
                       if(!jQuery.isEmptyObject(jar)){
                            for(var inputs in jar){
                            
                                 if("input"+lastobject.id == inputs){
                                input.setAttribute('value', jar[inputs]);
                            
                            }
                            else{
                                input.setAttribute('value', "1"); 
                            }
                         
                           
                           
                       
                        }
                        }
                        else{
                            input.setAttribute('value', "1"); 
                        }
                        
                    }
                    else{
                       input.setAttribute('value', "1"); 
                    }
                     input.setAttribute('name', "quantity");
                     
                     input.setAttribute('class', "product-form__input qty");
                     var a6 = document.createElement("A");
                     a6.setAttribute('class', "qtyBtn plus");
                    
                     a6.setAttribute('href', "javascript:addOne('"+input_product+"');");
                     var i4 = document.createElement("I");
                     i4.setAttribute('class', "fa anm anm-plus-r");
                     i4.setAttribute('aria-hidden', "true");
                     a6.appendChild(i4);
                     div4.appendChild(input);
                     div4.appendChild(a6);
                     div3.appendChild(div4);
                     div.appendChild(div3);
                     
                     var div5 = document.createElement("DIV");
                     div5.setAttribute('class', "priceRow");
                     var div6 = document.createElement("DIV");
                     div6.setAttribute('class', "product-price");
                     var span2 = document.createElement("SPAN");
                     span2.setAttribute('class', "money");
                     span2.setAttribute('id', "span_"+"input"+lastobject.id);
                     var precio = "";
                     
                     if(lastobject.active == "1"){
                         precio = lastobject.price - (lastobject.price*lastobject.discount);
                     }
                     else{
                         precio = lastobject.price;
                     }
                     finalprice = parseFloat(finalprice, 10) + parseFloat(precio, 10);
                     span2.innerHTML = "Q"+precio;
                     div6.appendChild(span2);
                     
                     div5.appendChild(div6);
                     
                     div.appendChild(div5);
                     
                     li.appendChild(div);
                    
                     document.getElementById('cartlist').appendChild(li);
                      if(jQuery.cookie('likes_map')!== undefined){
                        updatePriceLikes("noelement", "noelement");
            
                      }
                     document.getElementById('spanmoney').innerHTML = "Q"+finalprice;
                    }
                     
                     
                 };
             }
                 
             
             //TERMINA LLAMADA A API PARA IMAGEN
             }
         };
         

         
    

         
}
</script>
<script>
    if(Cookies.get('likes') != undefined){
         var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let monster = JSON.stringify(jar);
        getLikes(monster);
    }
    else{
        
    }
</script>
<!-- termina likes -->
<script>
    function updateLikes(){
        document.getElementById("cartlist").innerHTML = "";
         if(Cookies.get('likes') != undefined){
         document.getElementById('cartlist').innerHTML = "";
         var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let monster = JSON.stringify(jar);
        getLikes(monster);
         var ul =document.getElementById("cartlist").childNodes
        if(ul.length==0){
            document.getElementById("spanmoney").innerHTML = "Q00.00";
        }

        }
    }
    function updatePriceLikes(inputid, operation){
        if(inputid !== "noelement" && operation !== "noelement"){
            
       
        var li = document.getElementById('cartlist');
        var costo = document.getElementById("span_"+inputid).innerHTML;
        costo = costo.substr(1, costo.length);
        var total = document.getElementById("spanmoney").innerHTML;
        total = total.substr(1, total.length);
        var qty = document.getElementById(inputid).value;
        if(JSON.parse(Cookies.get('likes')).length > 1){
            if(operation == "+"){
                total = parseFloat(total, 10) + (1* parseFloat(costo, 10));
            }
            else{
                total = parseFloat(total, 10) - (1* parseFloat(costo, 10));
            }
        }
        else{
            total = (parseInt(qty,10)* parseFloat(costo, 10));
        }
           document.getElementById("spanmoney").innerHTML = "Q"+total; 

        }
        else{
            var liked_elements = JSON.parse(jQuery.cookie('likes_map'));
            for(var el in liked_elements){
                    var li = document.getElementById('cartlist');
                    var nodes = li.childNodes[1].childNodes[1].childNodes[4].childNodes[0].childNodes[0].childNodes[0].data;
                    var s = "span_"+String(el);
                    var costo = nodes;
                    costo = costo.substr(1, costo.length);
                    var total = document.getElementById("spanmoney").innerHTML;
                    total = total.substr(1, total.length);
                    var qty = document.getElementById(el).value;
                    if(JSON.parse(Cookies.get('likes')).length > 1){
                            total = parseFloat(total, 10) + (1* parseFloat(costo, 10));
                        
                    }
                    else{
                        total = (parseInt(qty,10)* parseFloat(costo, 10));
                    }
                       document.getElementById("spanmoney").innerHTML = "Q"+total; 
            
                        }
        }
        
    
        

    }
    function addOne(id){
        var input_element = document.getElementById(id);
        if(!isNaN(parseInt(input_element.value, 10)) &&  parseInt(input_element.value, 10) != 0){
            input_element.value = parseInt(input_element.value, 10) +1;
             var jar = JSON.parse(Cookies.get('likes_map'));
             jar[id] = input_element.value;

            jQuery.cookie('likes_map', JSON.stringify(jar), { expires: 30 });
            updatePriceLikes(id, "+");
        }
        else{
            
        }
        
    }
    function substractOne(id){
        var input_element = document.getElementById(id);
        if(!isNaN(parseInt(input_element.value, 10)) &&  parseInt(input_element.value, 10) > 1){
            input_element.value =  parseInt(input_element.value, 10) - 1;
            //guardar cookie que contenga el id del input con value = input_element.value
             var jar = JSON.parse(Cookies.get('likes_map'));
             jar[id] = input_element.value;
            jQuery.cookie('likes_map', JSON.stringify(jar), { expires: 30 });
            updatePriceLikes(id, "-");
        }
        else{
            
        }
        
        
        
    }
    function unLike(sku, id){
        let element = document.getElementById('li'+sku);
        element.parentNode.removeChild(element);
        var jar = Cookies.get('likes');
        jar = JSON.parse(jar);
        let resultado = jar.filter(skus => skus != sku);
        let monster = JSON.stringify(resultado);
        jQuery.cookie('likes', monster, { expires: 30 });
        var id = "input"+id;
        var liked_elements = JSON.parse(jQuery.cookie('likes_map'));
        for(var el in liked_elements){
            if(el == id){
                delete liked_elements[el];
            }
        }
        jQuery.cookie('likes_map', JSON.stringify(liked_elements), { expires: 30 });
        
        updateLikes();
        
    }
    
</script>
<script>

function openPopup(titulo, stock, sku, oldprice, price, descripcion, color, tallas, imagenbase64 ){
    document.getElementById("modal_titulo").innerHTML = titulo;
    document.getElementById("modal_div_stock").innerHTML = "En Stock: "+stock;
    document.getElementById("modal_span_sku").innerHTML = sku;
    document.getElementById("modal_span_oldprice").innerHTML = oldprice;
    document.getElementById("modal_span_price").innerHTML = price;
    document.getElementById("modal_div_descripcion").innerHTML = descripcion;
    document.getElementById("modal_span_color").innerHTML = color;
    //<img class="grid-view-item__image primary blur-up ls-is-cached lazyloaded" data-src="
    var imagen = document.createElement("IMG");
    imagen.setAttribute('src', imagenbase64);
    document.getElementById("modal_div_imagen").innerHTML = "";
    document.getElementById("modal_div_imagen").appendChild(imagen);
     document.getElementById("modal_a_comprar").href = "javascript:comprar('"+titulo+"', '"+stock+"', '"+sku+"', '"+oldprice+"', '"+price+"', '"+descripcion+"', '"+color+"', '"+tallas+"', '"+imagenbase64+"')";
    document.getElementById("modal_a_like").href = "javascript:addLikes("+sku+")";
     $("#content_quickview").modal();
}
</script>
<script>
    function comprar(titulo, stock, sku, oldprice, price, descripcion, color, tallas, imagenbase64){
        handleProduct(sku, color);
        var action = JSON.stringify({"titulo":titulo, "stock":stock, "sku": sku, "oldprice": oldprice, "price":price, "descripcion":descripcion, "color": color,"tallas":tallas, "imagenbase64":imagenbase64});
         var form = document.createElement('form');
        form.style.visibility = 'hidden';
        form.method = 'POST';
        form.action = "product-accordion.php";
        var input = document.createElement('input');
            input.name = "action";
            input.value = action;
            form.appendChild(input)
        document.body.appendChild(form);
        form.submit();
    }
    

    

</script>
</div>
</body>


</html>