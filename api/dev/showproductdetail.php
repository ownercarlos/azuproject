        <?php
        if(isset($_GET['sku'])){
        $sku = $_GET['sku'];
        $sql = "SELECT * FROM `Producto` WHERE `sku` LIKE '$sku'";
        $database = "u931312158_Dev_azustudio";
        $username = "u931312158_dev";
        $password = "aqp12345W_";
        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $database);
        // Check connection
        if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
        }
        
        $result = mysqli_query($conn, $sql) or die ("Connection failed: " . mysqli_error($conn));
        $html = "";
        $arr = array();
        $obj;
        while($row = $result->fetch_assoc()){
        $html = $html . '<h1 class="product-single__title">'.$row['titulo'].'</h1>
        <div class="product-nav clearfix">					
        <a href="#" class="next" title="Next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
        </div>
        <div class="prInfoRow">
        <div class="product-stock"> <span class="instock ">Existencias: '.$row['stock'].'</span> <span class="outstock hide">Unavailable</span> </div>
        <div class="product-sku">SKU: <span class="variant-sku">'.$row['sku'].'</span></div>
        <div class="product-review"><a class="reviewLink" href="#tab2"><i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star"></i><i class="font-13 fa fa-star-o"></i><i class="font-13 fa fa-star-o"></i><span class="spr-badge-caption">6 reviews</span></a></div>
        </div>
        <p class="product-single__price product-single__price-product-template">
        <span class="visually-hidden">Regular price</span>';
        if($row["oferta"]=="1"){
             $old_price = "Q". $row["precio"];
             $product_price = $row["precio"]  - ($row["precio"] * $row["descuento"]) ;
             $product_price = "Q".$product_price;
             $sale = substr($row["descuento"], -2);
             if(substr($sale, 0,1) == 0){
                 $sale = str_replace("0", "", $sale);
             }
             $ahorro = $row["precio"] - ($row["precio"]  - ($row["precio"] * $row["descuento"]));
             $ahorro = "Q". $ahorro;
             $html = $html .'<s id="ComparePrice-product-template"><span class="money">'.$old_price.'</span></s>
        <span class="product-price__price product-price__price-product-template product-price__sale product-price__sale--single">
        <span id="ProductPrice-product-template"><span class="money">'.$product_price.'</span></span>
        </span>
        <span class="discount-badge"> <span class="devider">|</span>&nbsp;
        <span>You Save</span>
        <span id="SaveAmount-product-template" class="product-single__save-amount">
        <span class="money">'.$ahorro.'</span>
        </span>
        <span class="off">(<span>'.$sale.'</span>%)</span>
        </span>  
        </p>';
             
         }
         else{
             
             $product_price = "Q".$row["precio"];
             
             
         }
        
        $html = $html .'<div class="product-single__description rte">
        <p>'.$row['descripcion'].'</p>
        </div>
        <form method="post" action="http://annimexweb.com/cart/add" id="product_form_10508262282" accept-charset="UTF-8" class="product-form product-form-product-template hidedropdown" enctype="multipart/form-data">
        <div class="swatch clearfix swatch-0 option1" data-option-index="0">
        <div class="product-form__item">
        <div data-value="Black" class="swatch-element color black available">
        <input class="swatchInput" id="swatch-0-black" type="radio" name="option-0" value="Black"><label class="swatchLbl color small" for="swatch-0-black" style="background-color:black;" title="Black"></label>
        </div>
        <div data-value="Maroon" class="swatch-element color maroon available">
        <input class="swatchInput" id="swatch-0-maroon" type="radio" name="option-0" value="Maroon"><label class="swatchLbl color small" for="swatch-0-maroon" style="background-color:maroon;" title="Maroon"></label>
        </div>
        <div data-value="Blue" class="swatch-element color blue available">
        <input class="swatchInput" id="swatch-0-blue" type="radio" name="option-0" value="Blue"><label class="swatchLbl color small" for="swatch-0-blue" style="background-color:blue;" title="Blue"></label>
        </div>
        <div data-value="Dark Green" class="swatch-element color dark-green available">
        <input class="swatchInput" id="swatch-0-dark-green" type="radio" name="option-0" value="Dark Green"><label class="swatchLbl color small" for="swatch-0-dark-green" style="background-color:darkgreen;" title="Dark Green"></label>
        </div>
        </div>
        </div>
        <div class="swatch clearfix swatch-1 option2" data-option-index="1">
        <div class="product-form__item">
        <div data-value="XS" class="swatch-element xs available">
        <input class="swatchInput" id="swatch-1-xs" type="radio" name="option-1" value="XS">
        <label class="swatchLbl small" for="swatch-1-xs" title="XS">XS</label>
        </div>
        <div data-value="S" class="swatch-element s available">
        <input class="swatchInput" id="swatch-1-s" type="radio" name="option-1" value="S">
        <label class="swatchLbl small" for="swatch-1-s" title="S">S</label>
        </div>
        <div data-value="M" class="swatch-element m available">
        <input class="swatchInput" id="swatch-1-m" type="radio" name="option-1" value="M">
        <label class="swatchLbl small" for="swatch-1-m" title="M">M</label>
        </div>
        </div>
        </div>
        <p class="infolinks"><a href="#sizechart" class="sizelink btn"> Guia de tamaños</a> <a href="#productInquiry" class="emaillink btn"> Preguntar sobre el Producto</a></p>
        <!-- Product Action -->
        <div class="product-action clearfix">
        <div class="product-form__item--quantity">
        <div class="wrapQtyBtn">
        <div class="qtyField">
        <a class="qtyBtn minus" href="javascript:void(0);"><i class="fa anm anm-minus-r" aria-hidden="true"></i></a>
        <input type="text" id="Quantity" name="quantity" value="1" class="product-form__input qty">
        <a class="qtyBtn plus" href="javascript:void(0);"><i class="fa anm anm-plus-r" aria-hidden="true"></i></a>
        </div>
        </div>
        </div>                                
        <div class="product-form__item--submit">
        <button type="button" name="add" class="btn product-form__cart-submit" style="background-color:#b20000;">
        <span>Añadir al Carrito</span>
        </button>
        </div>
        </div>
        <!-- End Product Action -->
        </form>
        <div class="display-table shareRow">
        <div class="display-table-cell medium-up--one-third">
        <div class="wishlist-btn">
        <a class="wishlist add-to-wishlist" href="#" title="Add to Wishlist"><i class="icon anm anm-heart-l" aria-hidden="true"></i> <span>Añadir a mis deseos</span></a>
        </div>
        </div>
        <div class="display-table-cell text-right">
        <div class="social-sharing">
        <a target="_blank" href="#" class="btn btn--small btn--secondary btn--share share-facebook" title="Share on Facebook">
        <i class="fa fa-facebook-square" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Compartir</span>
        </a>
        <a target="_blank" href="#" class="btn btn--small btn--secondary btn--share share-twitter" title="Tweet on Twitter">
        <i class="fa fa-twitter" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Tweet</span>
        </a>
        <a href="#" title="Share on google+" class="btn btn--small btn--secondary btn--share" >
        <i class="fa fa-google-plus" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Google+</span>
        </a>
        <a target="_blank" href="#" class="btn btn--small btn--secondary btn--share share-pinterest" title="Pin on Pinterest">
        <i class="fa fa-pinterest" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Pin it</span>
        </a>
        <a href="#" class="btn btn--small btn--secondary btn--share share-pinterest" title="Share by Email" target="_blank">
        <i class="fa fa-envelope" aria-hidden="true"></i> <span class="share-title" aria-hidden="true">Correo</span>
        </a>
        </div>
        </div>
        </div>';
        array_push($arr, $html);
        
        }
        $obj->information = $arr;
        echo json_encode($obj);
        
        }
        
        ?>