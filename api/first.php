<?php
//step 1
require __DIR__  . '/PayPal-PHP-SDK/PayPal-PHP-SDK/autoload.php';

//step 2

// After Step 1
$apiContext = new \PayPal\Rest\ApiContext(
        new \PayPal\Auth\OAuthTokenCredential(
            'AdFV1_gkfkF8f-GTLRAxzG5lQ2Vu05pTVmA2o1ZB_K8FVpxLjiUaYaVeyz-TW0lafFDoiV0RAecGXmoe',     // ClientID
            'EGQOJaAPnPVGqsrPjMgoABYO3YP2APJe-pcMJI5Nx9qMhHs6FU96EaB17JJ2JjtZhoTGnI3QmPKQ3iga'      // ClientSecret
        )
);

//create new payment using the api

// After Step 2
$payer = new \PayPal\Api\Payer();
$payer->setPaymentMethod('paypal');

$amount = new \PayPal\Api\Amount();
$amount->setTotal('1.00');
$amount->setCurrency('USD');

$transaction = new \PayPal\Api\Transaction();
$transaction->setAmount($amount);

$redirectUrls = new \PayPal\Api\RedirectUrls();
$redirectUrls->setReturnUrl("https://api.azu-studio.com/dev/")
    ->setCancelUrl("https://www.azu-studio.com");

$payment = new \PayPal\Api\Payment();
$payment->setIntent('sale')
    ->setPayer($payer)
    ->setTransactions(array($transaction))
    ->setRedirectUrls($redirectUrls);




?>